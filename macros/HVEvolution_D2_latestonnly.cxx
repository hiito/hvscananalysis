/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_D2_latestonnly.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "atlasstyle-00-04-02/AtlasStyle.h"
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

int main(int argc, char** argv)
{ 
  SetAtlasStyle();
  
  static const unsigned int nRun = 1; int mRun = 1;
  int Runnumber[nRun]    = { 461655};
  std::string TSBD[nRun] = {   "d2"};
  int profile[nRun]      = {      1};
  bool BCID[nRun]        = {   true};
  double Fitxmax[nRun]   = {    200};
  double Fitxmin[nRun]   = {     10};
  
  static const unsigned int nSide = 3; int mSide = 3;
  static const unsigned int nEta = 3; int mEta = 3;
  static const unsigned int nisEA = 2; int misEA = 2;
  TGraphErrors* g_HV[nRun][nSide][nEta][nisEA];
  
  TLegend * l1 = new TLegend(0.5,0.34,0.80,0.64); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  // TString TSLegend[nRun] = {"September 2018", "21 October 2022", "21 November 2022", "22 Apr 2023", "11 July 2023", "September 2023 HeavyIon"};
  TString TSLegend[nRun] = { "September 2023 HeavyIon"};
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");                          tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.05); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.275,0.90,"SCT Internal");                  tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  // TLatex * tex2 = new TLatex(0.275,0.90,"SCT Preliminary"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.15,0.835,"#sqrt{s} = 13 TeV until 2018");  tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.05); tex3->SetLineWidth(2);
  TLatex * tex4 = new TLatex(0.50,0.835,"#sqrt{s} = 13.6 TeV from 2022");  tex4->SetNDC(); tex4->SetTextFont(42); tex4->SetTextSize(0.05); tex4->SetLineWidth(2);
  TLatex * tex5 = new TLatex(0.15,0.77,"Disk 2");                          tex5->SetNDC(); tex5->SetTextFont(42); tex5->SetTextSize(0.05); tex5->SetLineWidth(2);
  TString TSSide[nSide] = {"Side0", "Side1", "Average of Side"};
  TLatex * texSide[nSide];
  for(int ii_Side = 0; ii_Side < mSide; ii_Side++){
    texSide[ii_Side] = new TLatex(0.54,0.77,TSSide[ii_Side]); texSide[ii_Side]->SetNDC(); texSide[ii_Side]->SetTextFont(42); texSide[ii_Side]->SetTextSize(0.05); texSide[ii_Side]->SetLineWidth(2);
  }
  TLatex * texEta[nEta];
  TString TSEta[nEta] = {"Outer", "Middle", "Inner"};
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    texEta[ii_Eta] = new TLatex(0.44,0.77,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.05); texEta[ii_Eta]->SetLineWidth(2);
  }
  TLatex * texisEA[nisEA];
  TString TSisEA[nisEA] = {"EndCap-A", "EndCap-C"};
  for(int ii_isEA = 0; ii_isEA < misEA; ii_isEA++){
    texisEA[ii_isEA] = new TLatex(0.26,0.77,TSisEA[ii_isEA]); texisEA[ii_isEA]->SetNDC(); texisEA[ii_isEA]->SetTextFont(42); texisEA[ii_isEA]->SetTextSize(0.05); texisEA[ii_isEA]->SetLineWidth(2);
  }
  
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_D2_latest";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");
    
  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  // std::vector<TProfile*> profs(0);
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;

  TF1 * fitfunc[nRun][nSide][nEta][nisEA];
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    int eta = ii_Eta + 1;
    // for(int ii_Eta = 0; ii_Eta < 1; ii_Eta++){
    // int eta = ii_Eta + 3;
    for(int ii_Side = 0; ii_Side < mSide; ii_Side++){
      for(int ii_isEA = 0; ii_isEA < misEA; ii_isEA++){
	bool bisEA = true;
	if(ii_isEA==1) bisEA = false;
	TCanvas * c1 = new TCanvas("c1", "", 800, 600);
	c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
	TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 270, 1.4);
	frame1 -> SetTitle(";HV[V];Hit Efficiency");
	frame1 -> GetXaxis() -> SetTitleSize(0.05);
	frame1 -> GetXaxis() -> SetLabelSize(0.05);
	frame1 -> GetXaxis() -> SetTitleOffset(0.9);
	frame1 -> GetYaxis() -> SetTitleSize(0.05);
	frame1 -> GetYaxis() -> SetTitleOffset(0.9);
	frame1 -> GetYaxis() -> SetLabelSize(0.05);
	
	for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
	  Config conf(Runnumber[ii_Run], false, bisEA, ii_Side, TSBD[ii_Run], profile[ii_Run], true, false, BCID[ii_Run], 2);
	  std::vector<std::string> sp = conf.GetScanPoints();

	  double Eff50HV = 0;
	  double beforeEff = 0;
	  g_HV[ii_Run][ii_Side][eta][ii_isEA] = new TGraphErrors(sp.size());
	  for(std::string sp : sp){
	    std::cout << "------------------------------------------------------" << std::endl; 
	    int SP = stoi(sp);
	    std::vector<TProfile*> profs(0);
	    profs = getProfileX(conf, getEffProf2D(conf, sp));
	    TProfile* p = nullptr;
	    p = profs.at(eta-1);
	    value = p->GetBinContent(p->FindBin(eta-1));//because eta indices for the endcaps starts from 0 and ends at 2
	    error = p->GetBinError(p->FindBin(eta-1));
	    g_HV[ii_Run][ii_Side][eta][ii_isEA]->SetPoint(counter, SP, value);
	    g_HV[ii_Run][ii_Side][eta][ii_isEA]->SetPointError(counter, 0, error);
	    std::cout << "Efficiency for run "<<Runnumber[ii_Run]<<" and eta = " << eta<<"  and HV "<<SP<<"  isEA "<<bisEA<<"    Side "<<ii_Side << " is " << value << " +/- " << error << std::endl;
	    if(beforeEff<0.5 && 0.5<value) Eff50HV = SP;
	    beforeEff = value;
	    ++counter;
	  }
	  counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;
	  
	  gROOT->GetColor(3)->SetRGB(0., 0.7, 0.);
	  g_HV[ii_Run][ii_Side][eta][ii_isEA] -> SetLineColor(mycolor(4));
	  g_HV[ii_Run][ii_Side][eta][ii_isEA] -> SetMarkerColor(mycolor(4));
	  g_HV[ii_Run][ii_Side][eta][ii_isEA] -> SetMarkerStyle(20+ii_Run);
	  g_HV[ii_Run][ii_Side][eta][ii_isEA] -> Draw("PLE sames text");
	  // g_HV[ii_Run][ii_Side][eta][ii_isEA] -> Draw("PE sames");
	  
	  // fitfunc[ii_Run][ii_Side][eta][ii_isEA]  = new TF1("fitfunc", "[2]*((ROOT::Math::gaussian_cdf(x, [0], [1]))+[3])");
	  // fitfunc[ii_Run][ii_Side][eta][ii_isEA] -> SetParameters(15, Eff50HV);
	  // fitfunc[ii_Run][ii_Side][eta][ii_isEA] -> SetLineColor(mycolor(ii_Run));
	  // fitfunc[ii_Run][ii_Side][eta][ii_isEA] -> SetLineWidth(2);
	  // g_HV[ii_Run][ii_Side][eta][ii_isEA]    -> Fit("fitfunc","Q+","", Fitxmin[ii_Run], Fitxmax[ii_Run]);
	  if(ii_Side==0 && eta==1 && ii_isEA==0) l1 -> AddEntry(g_HV[ii_Run][ii_Side][eta][ii_isEA], TSLegend[ii_Run], "PEL");
	}
	
	l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw(); tex5->Draw();
	texSide[ii_Side]->Draw(); texisEA[ii_isEA]->Draw();
	// texEta[2]->Draw();
	texEta[ii_Eta]->Draw();
	c1 -> Print(pdfname1+".pdf","pdf");
	delete c1;
      }
    }
  }
    
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
