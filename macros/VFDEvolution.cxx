/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/VFDEvolution.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

int main(int argc, char** argv)
{ 
// #ifdef __CINT__
//   gROOT->LoadMacro("./atlasstyle-00-04-02/AtlasLabels.C");
//   gROOT->LoadMacro("./atlasstyle-00-04-02/AtlasUtils.C");
// #endif

  // SetAtlasStyle();
  
  // std::cout << "argv[1] = " << argv[1] << std::endl; 
  std::cout << "Configuration for run 361689:" << std::endl;
  Config conf_361689(361689, true, false, 2, "", 0, true, false, true);
  conf_361689.PrintConfiguration();
  std::cout << "Configuration for run 348251:" << std::endl;
  Config conf_348251(348251, true, false, 2, "b3d9", 0, true, false, true);
  conf_348251.PrintConfiguration();
  Config conf_324502(324502, true, false, 2, "", 1, true, false, true);
  conf_324502.PrintConfiguration();
  Config conf_309375(309375, true, false, 2, "", 1, true, false, true);
  conf_309375.PrintConfiguration();
  Config conf_340072(340072, true, false, 2, "", 1, true, false, true);
  conf_340072.PrintConfiguration();
  Config conf_284484(284484, true, false, 2, "", 1, true, false, true);
  conf_284484.PrintConfiguration();
  Config conf_427514(427514, true, false, 2, "b0d2", 1, true, false, true);
  
  TColor* color3 = gROOT->GetColor(3);
  color3->SetRGB(0, 0.7, 0);
  TColor* color5 = gROOT->GetColor(5);
  color5->SetRGB(0.7, 0.7, 0);
  
  TProfile* p_361689 = nullptr;
  TProfile* p_348251 = nullptr;
  TProfile* p_324502 = nullptr;
  TProfile* p_309375 = nullptr;
  TProfile* p_340072 = nullptr;
  TProfile* p_284484 = nullptr;
  TProfile* p_427514 = nullptr;
  
  std::vector<TProfile*> profs;
  std::vector<std::string> sp_361689 = conf_361689.GetScanPoints();
  std::vector<std::string> sp_348251 = conf_348251.GetScanPoints();
  std::vector<std::string> sp_324502 = conf_324502.GetScanPoints();
  std::vector<std::string> sp_309375 = conf_309375.GetScanPoints();
  std::vector<std::string> sp_340072 = conf_340072.GetScanPoints();
  std::vector<std::string> sp_284484 = conf_284484.GetScanPoints();
  std::vector<std::string> sp_427514 = conf_427514.GetScanPoints();
  
  std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
    {3, 1, -6, 14},
    {3, 1, -6, 21},
    {3, 1, -5, 14},
    {3, 1, -5, 21},
    {3, 1, -4, 20},
    {3, 1, -3, 20},
    {3, 1, -3, 21},
    {3, 1, -2, 20},
    {3, 1, -1, 20},
    {3, 1, 1, 20},
    {3, 1, 2, 22},
    {3, 1, 3, 22},
    {3, 1, 4, 5},
    {3, 1, 4, 20},
    {3, 1, 6, 31}
  };
  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  static const unsigned int nRun = 7; int mRun = 7;
  static const unsigned int nEta = 6; int mEta = 6;
  TGraphErrors* g_HV[nRun][nEta];
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;
  
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    int eta = ii_Eta + 1;
    
    g_HV[0][ii_Eta] = new TGraphErrors(sp_361689.size());
    for(std::string sp : sp_361689){
      std::cout << "------------------------------------------------------" << std::endl; 
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_361689, getEffProf2D(conf_361689, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[0][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[0][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 361689 and eta = " << eta << " is " << value << " +/- " << error << std::endl; 
      ++counter;
    }
    
    counter = 0;
    value = 0;
    error = 0;
    error1 = 0;
    error2 = 0;
    
    ipos=-1;
    ineg=-1;
    
    g_HV[1][ii_Eta] = new TGraphErrors(sp_348251.size());
    for(std::string sp : sp_348251){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_348251, getEffProf2D(conf_348251, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[1][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[1][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 348251 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
    counter = 0;
    value = 0;
    error = 0;
    error1 = 0;
    error2 = 0;
    
    ipos=-1;
    ineg=-1;
    
    g_HV[2][ii_Eta] = new TGraphErrors(sp_324502.size());
    for(std::string sp : sp_324502){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_324502, getEffProf2D(conf_324502, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[2][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[2][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 324502 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
    counter = 0;
    value = 0;
    error = 0;
    error1 = 0;
    error2 = 0;
    
    ipos=-1;
    ineg=-1;
    
    g_HV[3][ii_Eta] = new TGraphErrors(sp_309375.size());
    for(std::string sp : sp_309375){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_309375, getEffProf2D(conf_309375, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[3][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[3][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 309375 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
    counter = 0;
    value = 0;
    error = 0;
    error1 = 0;
    error2 = 0;
    
    ipos=-1;
    ineg=-1;
    
    g_HV[4][ii_Eta] = new TGraphErrors(sp_340072.size());
    for(std::string sp : sp_340072){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_340072, getEffProf2D(conf_340072, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[4][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[4][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 340072 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
    counter = 0;
    value = 0;
    error = 0;
    error1 = 0;
    error2 = 0;
    
    ipos=-1;
    ineg=-1;
    
    g_HV[5][ii_Eta] = new TGraphErrors(sp_284484.size());
    for(std::string sp : sp_284484){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_284484, getEffProf2D(conf_284484, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[5][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[5][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 284484 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
    g_HV[6][ii_Eta] = new TGraphErrors(sp_427514.size());
    for(std::string sp : sp_427514){
      std::cout << "------------------------------------------------------" << std::endl;
      int SP = stoi(sp);
      profs = getProfileXExclMod(conf_427514, getEffProf2D(conf_427514, sp), modB3_CO100);
      if (eta == 1){ineg = 5; ipos = 6;}
      else if (eta == 2){ineg = 4; ipos = 7;}
      else if (eta == 3){ineg = 3; ipos = 8;}
      else if (eta == 4){ineg = 2; ipos = 9;}
      else if (eta == 5){ineg = 1; ipos = 10;}
      else if (eta == 6){ineg = 0; ipos = 11;}
      ppos = profs.at(ipos);
      pneg = profs.at(ineg);
      value = ppos->GetBinContent(ppos->FindBin(eta));
      value += pneg->GetBinContent(pneg->FindBin(-1*eta));
      value = value/2;
      error1 = ppos->GetBinError(ppos->FindBin(eta));
      error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
      error = TMath::Sqrt(error1*error1 + error2*error2);
      g_HV[6][ii_Eta]->SetPoint(counter, SP, value);
      g_HV[6][ii_Eta]->SetPointError(counter, 0, error);
      std::cout << "Efficiency for run 427514 and eta = " << eta << " is " << value << " +/- " << error << std::endl;
      ++counter;
    }
    
  }
  
  TLegend * l1 = new TLegend(0.5,0.32,0.80,0.64); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  TString TSLegend[nRun] = {"September 2018", "April 2018", "May 2017", "September 2016", "November 2017","November 2015", "July 2022"};
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS"); tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.038); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.25,0.90,"SCT Internal"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.038); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.45,0.90,"Barrel 3"); tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.038); tex3->SetLineWidth(2);
  TString TSEta[nEta] = {"|#eta_{index}|=1", "|#eta_{index}|=2", "|#eta_{index}|=3", "|#eta_{index}|=4", "|#eta_{index}|=5", "|#eta_{index}|=6"};
  TLatex * texEta[nEta];
  int SortRun[nRun] = {6, 0, 1, 4, 2, 3, 5};
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_B3";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");
  
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    TCanvas * c1 = new TCanvas("c1", "", 800, 600);
    c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 270, 1.2);
    frame1 -> SetTitle(";HV[V];Hit Efficiency");
    frame1 -> GetXaxis() -> SetTitleSize(0.05);
    frame1 -> GetXaxis() -> SetLabelSize(0.05);
    frame1 -> GetYaxis() -> SetTitleSize(0.05);
    frame1 -> GetYaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetLabelSize(0.05);
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      g_HV[SortRun[ii_Run]][ii_Eta] -> SetLineColor(mycolor(SortRun[ii_Run]));
      g_HV[SortRun[ii_Run]][ii_Eta] -> SetMarkerColor(mycolor(SortRun[ii_Run]));
      g_HV[SortRun[ii_Run]][ii_Eta] -> SetMarkerStyle(20+SortRun[ii_Run]);
      g_HV[SortRun[ii_Run]][ii_Eta] -> Draw("PLE sames");
      if(ii_Eta==0) l1 -> AddEntry(g_HV[SortRun[ii_Run]][ii_Eta], TSLegend[SortRun[ii_Run]], "PEL");
    }  
    l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw();
    texEta[ii_Eta] = new TLatex(0.60,0.90,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.038); texEta[ii_Eta]->SetLineWidth(2); texEta[ii_Eta]->Draw();
    c1 -> Print(pdfname1+".pdf","pdf");
    delete c1;
  }
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  
  return 0;
}
