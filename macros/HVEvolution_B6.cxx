/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_B6.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"
#include "include/header.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "atlasstyle-00-04-02/AtlasStyle.h"
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

int main(int argc, char** argv)
{ 
  SetAtlasStyle();
  
  static const unsigned int nRun = 9; int mRun = 9;
  int Runnumber[nRun]    = { 348251, 427514, 437692, 440199, 450271, 456316, 461899, 472553, 479374};
  std::string TSBD[nRun] = {   "b3", "b3d9",   "b3",   "b3",   "b3",   "b3",   "b3",   "b3",   "b3"};
  int profile[nRun]      = {      1,      1,      1,      1,      1,      2,      1,      1,      1};
  double Fitxmax[nRun]   = {    150,    150,    150,     150,   200,    200,    200,    200,    200};
  double Fitxmin[nRun]   = {     10,      5,     10,     10,     10,     10,     10,     10,     10};
    
  static const unsigned int nEta = 6; int mEta = 6;
  static const unsigned int nSide = 3; int mSide = 3;
  TGraphErrors* g_HV[nRun][nEta][nSide];
  
  TLegend * l1 = new TLegend(0.5,0.24,0.80,0.64); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  // TString TSLegend[nRun] = {"July 2022", "September 2018", "April 2018"};
  TString TSLegend[nRun] = {"April 2018", "July 2022", "21 October 2022", "21 November 2022", "22 April 2023", "11 July 2023", "1 October 2023 Pb-Pb #sqrt{s} = 5.36 TeV", "5 April 2024", "2 July 2024"};
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");                             tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.05); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.275,0.90,"SCT Internal");                     tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  // TLatex * tex2 = new TLatex(0.275,0.90,"SCT Preliminary"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.15,0.835,"#sqrt{s} = 13 TeV until 2018");     tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.05); tex3->SetLineWidth(2);
  TLatex * tex4 = new TLatex(0.50,0.835,"#sqrt{s} = 13.6 TeV from 2022");    tex4->SetNDC(); tex4->SetTextFont(42); tex4->SetTextSize(0.05); tex4->SetLineWidth(2);
  TLatex * texEta[nEta];
  TString TSEta[nEta] = {"Barrel 6, |#eta_{index}| = 1", "Barrel 6, |#eta_{index}| = 2", "Barrel 6, |#eta_{index}| = 3", "Barrel 6, |#eta_{index}| = 4", "Barrel 6, |#eta_{index}| = 5", "Barrel 6, |#eta_{index}| = 6"};
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    texEta[ii_Eta] = new TLatex(0.15,0.77,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.05); texEta[ii_Eta]->SetLineWidth(2);
  }
  TString TSSide[nSide] = {"Side0", "Side1", "Average of Side"};
  TLatex * texSide[nSide];
  for(int ii_Side = 0; ii_Side < mSide; ii_Side++){
    texSide[ii_Side] = new TLatex(0.47, 0.77,TSSide[ii_Side]); texSide[ii_Side]->SetNDC(); texSide[ii_Side]->SetTextFont(42); texSide[ii_Side]->SetTextSize(0.05); texSide[ii_Side]->SetLineWidth(2); 
  }
  
  static const unsigned int nyear = 5; int myear = 5;
  double YearLumi[nyear] = {4.20134, 42.6356, 92.8927, 156.185, 194.5};
  TLine * tlineYear[nyear];
  for(int ii_year=0; ii_year<myear; ii_year++){ tlineYear[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 180); tlineYear[ii_year]->SetLineStyle(1); tlineYear[ii_year]->SetLineWidth(1); }
  TLine * tlineSigma[nyear];
  for(int ii_year=0; ii_year<myear; ii_year++){ tlineSigma[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 40);  tlineSigma[ii_year]->SetLineStyle(1); tlineSigma[ii_year]->SetLineWidth(1); }
  TLatex * texYear[5]; TString TSYear[5] = {"2016", "2017", "2018", "2022", "2023"};
  double StartLumi[6] = { 4.2, 42.6, 92.9, 156.2, 194, 224};
  double AxisYear[5]; // = {15.7, 57.7, 111.9, 167.5, 205};
  for(int ii_year=0; ii_year<5; ii_year++){
    //  texYear[ii_year] = new TLatex( AxisYear[ii_year], 0.17, TSYear[ii_year]); texYear[ii_year]->SetNDC(); texYear[ii_year]->SetTextFont(42); texYear[ii_year]->SetTextSize(0.04); texYear[ii_year]->SetLineWidth(2);
    AxisYear[ii_year] = 0.35*(StartLumi[ii_year+1]-StartLumi[ii_year]) + StartLumi[ii_year];
    texYear[ii_year] = new TLatex( AxisYear[ii_year], 10, TSYear[ii_year]); texYear[ii_year]->SetTextFont(42); texYear[ii_year]->SetTextSize(0.04); texYear[ii_year]->SetLineWidth(2);
  }
  
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_B6";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");
  
  
  std::vector<TProfile*> profs;
  // std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
  //   {6, 0, -2, 6},
  //   {6, 0,  1, 8},
  //   {6, 0,  1, 38},
  //   {6, 1, -2, 6},
  //   {6, 1,  1, 8},
  //   {6, 1,  1, 38},
  //   {6, 2, -2, 6},
  //   {6, 2,  1, 8},
  //   {6, 2,  1, 38},
  // };
  
  std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
    {3, 1, -6, 14},
    {3, 1, -6, 21},
    {3, 1, -5, 14},
    {3, 1, -5, 21},
    {3, 1, -4, 20},
    {3, 1, -3, 20},
    {3, 1, -3, 21},
    {3, 1, -2, 20},
    {3, 1, -1, 20},
    {3, 1, 1, 20},
    {3, 1, 2, 22},
    {3, 1, 3, 22},
    {3, 1, 4, 5},
    {3, 1, 4, 20},
    {3, 1, 6, 31}
  };

  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;
  
  TF1 * fitfunc[nRun][nEta][nSide];
  for(int ii_Side = 0; ii_Side < mSide; ii_Side++){
    for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
      int eta = ii_Eta + 1;
      
      TCanvas * c1 = new TCanvas("c1", "", 800, 600);
      c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
      TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 220, 1.4);
      frame1 -> SetTitle(";HV[V];Hit Efficiency");
      frame1 -> GetXaxis() -> SetTitleSize(0.05);
      frame1 -> GetXaxis() -> SetLabelSize(0.05);
      frame1 -> GetXaxis() -> SetTitleOffset(0.9);
      frame1 -> GetYaxis() -> SetTitleSize(0.05);
      frame1 -> GetYaxis() -> SetTitleOffset(0.9);
      frame1 -> GetYaxis() -> SetLabelSize(0.05);
      
      for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
	std::cout<<Runnumber[ii_Run]<<std::endl;
	Config conf(Runnumber[ii_Run], true, false, ii_Side, TSBD[ii_Run], profile[ii_Run], true, false, false, 3);
	std::vector<std::string> sp = conf.GetScanPoints();
	
	double Eff50HV = 0;
	double beforeEff = 0;
	g_HV[ii_Run][ii_Eta][ii_Side] = new TGraphErrors(sp.size());
	for(std::string sp : sp){
	  std::cout << "------------------------------------------------------" << std::endl; 
	  int SP = stoi(sp);
	  profs = getProfileXExclMod(conf, getEffProf2D(conf, sp), modB3_CO100);
	  if      (eta == 1){ineg = 5; ipos = 6;}
	  else if (eta == 2){ineg = 4; ipos = 7;}
	  else if (eta == 3){ineg = 3; ipos = 8;}
	  else if (eta == 4){ineg = 2; ipos = 9;}
	  else if (eta == 5){ineg = 1; ipos = 10;}
	  else if (eta == 6){ineg = 0; ipos = 11;}
	  ppos = profs.at(ipos);
	  pneg = profs.at(ineg);
	  value = ppos->GetBinContent(ppos->FindBin(eta));
	  value += pneg->GetBinContent(pneg->FindBin(-1*eta));
	  value = value/2;
	  error1 = ppos->GetBinError(ppos->FindBin(eta));
	  error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
	  error = TMath::Sqrt(error1*error1 + error2*error2);
	  g_HV[ii_Run][ii_Eta][ii_Side]->SetPoint(counter, SP, value);
	  g_HV[ii_Run][ii_Eta][ii_Side]->SetPointError(counter, 0, error);
	  std::cout << "Efficiency for run "<<Runnumber[ii_Run]<<" and eta = " << eta << " is " << value << " +/- " << error << std::endl; 
	  if(beforeEff<0.5 && 0.5<value) Eff50HV = SP;
	  beforeEff = value;
	  ++counter;
	}
	counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;

	gROOT->GetColor(3)->SetRGB(0., 0.7, 0.);
	g_HV[ii_Run][ii_Eta][ii_Side] -> SetLineColor(mycolor(Runnumber[ii_Run]));
	g_HV[ii_Run][ii_Eta][ii_Side] -> SetMarkerColor(mycolor(Runnumber[ii_Run]));
	g_HV[ii_Run][ii_Eta][ii_Side] -> SetMarkerStyle(mystyle(Runnumber[ii_Run]));
	// g_HV[ii_Run][ii_Eta][ii_Side] -> Draw("PLE sames");
	g_HV[ii_Run][ii_Eta][ii_Side] -> Draw("PE sames");
	
	fitfunc[ii_Run][ii_Eta][ii_Side]  = new TF1("fitfunc", "[2]*((ROOT::Math::gaussian_cdf(x, [0], [1]))+[3])");
	fitfunc[ii_Run][ii_Eta][ii_Side] -> SetParameters(15, Eff50HV);
	fitfunc[ii_Run][ii_Eta][ii_Side] -> SetLineColor(mycolor(Runnumber[ii_Run]));
	fitfunc[ii_Run][ii_Eta][ii_Side] -> SetLineWidth(2);
	g_HV[ii_Run][ii_Eta][ii_Side]    -> Fit("fitfunc","Q+","", Fitxmin[ii_Run], Fitxmax[ii_Run]);
	// g_HV[ii_Run][ii_Eta][ii_Side]    -> Fit("fitfunc","","", 0, 250);
	// Eff95HV[ii_Run] = ErrorFunc(fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(0), fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(1), fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(2), fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(3));
	// sigma[ii_Run]   = fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(0);
	// std::cout<<fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(0)<<"     "<<fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(1)<<"     "<<fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(2)<<"     "<<fitfunc[ii_Run][ii_Eta][ii_Side]->GetParameter(3)<<"    "<<Eff95HV[ii_Run]<<std::endl;
	if(ii_Eta==0 && ii_Side==0) l1 -> AddEntry(g_HV[ii_Run][ii_Eta][ii_Side], RunDate(Runnumber[ii_Run]), "PEL");
      }
      
      l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
      texEta[ii_Eta]->Draw(); texSide[ii_Side]->Draw();
      c1 -> Print(pdfname1+".pdf","pdf");
      delete c1;
    }
  }
  
  
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
