/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/Eff_LB.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "atlasstyle-00-04-02/AtlasStyle.h"
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

int main(int argc, char** argv)
{ 
  SetAtlasStyle();

  static const unsigned int nEta = 6; int mEta = 6;
  int ii_Eta = 0;
  
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");        tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.05); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.275,0.90,"SCT Internal"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  // TLatex * tex2 = new TLatex(0.275,0.90,"SCT Preliminary"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex4 = new TLatex(0.15,0.835,"#sqrt{s} = 13.6 TeV");   tex4->SetNDC(); tex4->SetTextFont(42); tex4->SetTextSize(0.05); tex4->SetLineWidth(2);
  TLatex * texEta[nEta];
  TString TSEta[nEta] = {"Barrel 3, |#eta_{index}| = 1", "Barrel 3, |#eta_{index}| = 2", "Barrel 3, |#eta_{index}| = 3", "Barrel 3, |#eta_{index}| = 4", "Barrel 3, |#eta_{index}| = 5", "Barrel 3, |#eta_{index}| = 6"};
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    texEta[ii_Eta] = new TLatex(0.15,0.77,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.05); texEta[ii_Eta]->SetLineWidth(2);
  }
  TString pdfname1 = "plots/Efficiency_LumiBlock_B3";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");

  std::vector<TProfile*> profs;
  std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
    {3, 1, -6, 14},
    {3, 1, -6, 21},
    {3, 1, -5, 14},
    {3, 1, -5, 21},
    {3, 1, -4, 20},
    {3, 1, -3, 20},
    {3, 1, -3, 21},
    {3, 1, -2, 20},
    {3, 1, -1, 20},
    {3, 1, 1, 20},
    {3, 1, 2, 22},
    {3, 1, 3, 22},
    {3, 1, 4, 5},
    {3, 1, 4, 20},
    {3, 1, 6, 31}
  };
  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;
  int eta = ii_Eta + 1;
  
  TCanvas * c1 = new TCanvas("c1", "", 800, 600);
  c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
  TH1F *frame1 = (TH1F*)c1->DrawFrame(185, 0, 480, 1.4);
  frame1 -> SetTitle(";LumiBlock;Hit efficiency");
  frame1 -> GetXaxis() -> SetTitleSize(0.05);
  frame1 -> GetXaxis() -> SetLabelSize(0.05);
  frame1 -> GetXaxis() -> SetTitleOffset(0.9);
  frame1 -> GetYaxis() -> SetTitleSize(0.05);
  frame1 -> GetYaxis() -> SetTitleOffset(0.9);
  frame1 -> GetYaxis() -> SetLabelSize(0.05);
  
  Config conf(437692, true, false, 2, "b0LB", 2, true, false, true);
  std::vector<std::string> sp = conf.GetScanPoints();
  TGraphErrors* g_HV = new TGraphErrors(sp.size());
  for(std::string sp : sp){
    TProfile* ppos = nullptr;
    TProfile* pneg = nullptr;
    std::cout << "------------------------------------------------------" << std::endl; 
    int SP = stoi(sp);
    profs = getProfileXExclMod(conf, getEffProf2D(conf, sp), modB3_CO100);
    if      (eta == 1){ineg = 5; ipos = 6;}
    else if (eta == 2){ineg = 4; ipos = 7;}
    else if (eta == 3){ineg = 3; ipos = 8;}
    else if (eta == 4){ineg = 2; ipos = 9;}
    else if (eta == 5){ineg = 1; ipos = 10;}
    else if (eta == 6){ineg = 0; ipos = 11;}
    ppos = profs.at(ipos);
    pneg = profs.at(ineg);
    value = ppos->GetBinContent(ppos->FindBin(eta));
    value += pneg->GetBinContent(pneg->FindBin(-1*eta));
    value = value/2;
    error1 = ppos->GetBinError(ppos->FindBin(eta));
    error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
    error = TMath::Sqrt(error1*error1 + error2*error2);
    g_HV->SetPoint(counter, SP, value);
    g_HV->SetPointError(counter, 0, error);
    // std::cout << "Efficiency for run "<<Runnumber<<" and eta = " << eta << " is " << value << " +/- " << error << std::endl; 
    ++counter;
    delete ppos; delete pneg;
  }
  counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;
  
  g_HV -> SetMarkerStyle(20);
  g_HV -> SetMarkerSize(0.7);
  g_HV -> Draw("PE sames");
  tex1->Draw(); tex2->Draw(); tex4->Draw(); texEta[ii_Eta]->Draw();
  static const unsigned int nHV = 12; int mHV = 12;
  int startLB[nHV]    = {  191,   206,   216,   222,   235,   248,   261,   274,   289,   309,   324,   339};
  int endLB[nHV]      = {  198,   211,   218,   229,   241,   252,   264,   280,   297,   312,   329,   341};
  TLine * tline[24];
  for(int ii_HV = 0; ii_HV < mHV; ii_HV++){
    tline[ii_HV*2]   = new TLine(startLB[ii_HV], 0, startLB[ii_HV], 1.0);  tline[ii_HV*2]->SetLineColor(2);   tline[ii_HV*2]->Draw();
    tline[ii_HV*2+1] = new TLine(endLB[ii_HV], 0, endLB[ii_HV], 1.0);  tline[ii_HV*2+1]->SetLineColor(4); tline[ii_HV*2+1]->Draw();
  }
  
  c1 -> Print(pdfname1+".pdf","pdf");
  c1 -> SaveAs("plots/tmp.C");
  delete c1;

  
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
