/***********************************************************************/
/** g++ -std=c++14 -o prediction macros/EffPrediction.cxx `root-config --cflags --glibs` **/
/***********************************************************************/

#include <iostream>

#include "TGraph.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h" 
#include "TDatime.h"
#include "TAxis.h"

#include <vector>

int main ()
{
	TCanvas c;
	TDatime da1(2018,04,18,00,00,00);
	TDatime da2(2018,9,23,00,00,00);

	float x[2], eta1[2], eta2[2], eta3[2], eta4[2], eta5[2], eta6[2];
	
	int HV[2][6] = {75, 75, 75, 75, 75, 75, 100, 100, 100, 80, 80, 80};
	
	//~ std::vector<int> HV_da1 = {75, 75, 75, 75, 75, 75};
	//~ std::vector<int> HV_da2 = {100, 100, 100, 80, 80, 80};

	std::vector<int> graphColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> markerStyle = {20,21,22,23,33,29};

	x[0] = da1.Convert();
	x[1] = da2.Convert();

  TGraph* mgr = nullptr;

	for(int i=0; i<6; ++i){
		mgr = new TGraph(2);
		for (int j=0; j<2; ++j){
			mgr->SetPoint(j, x[j], HV[j][i]);
		}
		mgr->SetMarkerStyle(markerStyle.at(i));
		mgr->SetMarkerColor(graphColor.at(i));
		mgr->SetLineColor(graphColor.at(i));
		if (i == 0) mgr->Draw("APL");
		if (i > 0) mgr->Draw("PL SAME");
		mgr->GetXaxis()->SetTimeDisplay(1);
		//~ mgr->GetXaxis()->SetNdivisions(503);
		//~ mgr.GetXaxis()->SetTimeFormat("%Y-%m-%d");
		mgr->GetXaxis()->SetTimeFormat("%d/%m/%Y");
		mgr->GetXaxis()->SetTimeOffset(0,"gmt");
	}
	//~ eta1[0] = 75;
	//~ eta1[1] = 100;
	
	//~ mgr.SetMarkerStyle(20);

	//~ mgr.Draw("ap");
	

	c.SaveAs("plots/prediction_Barrel_3.pdf");
	
	return 0;
}
