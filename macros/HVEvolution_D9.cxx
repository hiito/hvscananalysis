/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_D9.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

int main(int argc, char** argv)
{ 
  static const unsigned int nRun = 2; int mRun = 2;
  int Runnumber[nRun]    = {427514, 348251};
  std::string TSBD[nRun] = {"b3d9", "b3d9"};
  int profile[nRun]      = {     1,      0};
  
  static const unsigned int nSide = 3; int mSide = 3;
  TGraphErrors* g_HV[nRun][nSide];
  
  TLegend * l1 = new TLegend(0.5,0.52,0.80,0.64); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  TString TSLegend[nRun] = {"July 2022", "April 2018"};
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");        tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.038); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.25,0.90,"SCT Internal"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.038); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.45,0.90,"Disk 9");       tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.038); tex3->SetLineWidth(2);
  TString TSSide[nSide] = {"Side0", "Side1", "Average of Side"};
  TLatex * texSide[nSide];
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_D9";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");
    
  std::vector<TProfile*> profs;    
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;
  
  for(int ii_Side = 0; ii_Side < mSide; ii_Side++){
    int eta = 1;
    
    TCanvas * c1 = new TCanvas("c1", "", 800, 600);
    c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 270, 1.2);
    frame1 -> SetTitle(";HV[V];Hit Efficiency");
    frame1 -> GetXaxis() -> SetTitleSize(0.05);
    frame1 -> GetXaxis() -> SetLabelSize(0.05);
    frame1 -> GetYaxis() -> SetTitleSize(0.05);
    frame1 -> GetYaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetLabelSize(0.05);
    
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      Config conf(Runnumber[ii_Run], false, true, ii_Side, TSBD[ii_Run], profile[ii_Run], true, false, true);
      std::vector<std::string> sp = conf.GetScanPoints();
      
      g_HV[ii_Run][ii_Side] = new TGraphErrors(sp.size());
      for(std::string sp : sp){
	std::cout << "------------------------------------------------------" << std::endl; 
	int SP = stoi(sp);
	profs = getProfileX(conf, getEffProf2D(conf, sp));
	p = profs.at(eta-1);
	value = p->GetBinContent(p->FindBin(eta-1));//because eta indices for the endcaps starts from 0 and ends at 2
	error = p->GetBinError(p->FindBin(eta-1));
	g_HV[ii_Run][ii_Side]->SetPoint(counter, SP, value);
	g_HV[ii_Run][ii_Side]->SetPointError(counter, 0, error);
	std::cout << "Efficiency for run "<<Runnumber[ii_Run]<<" and eta = " << eta << " is " << value << " +/- " << error << std::endl; 
	++counter;
      }
      counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;
      
      g_HV[ii_Run][ii_Side] -> SetLineColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Side] -> SetMarkerColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Side] -> SetMarkerStyle(20+ii_Run);
      g_HV[ii_Run][ii_Side] -> Draw("PLE sames");
      if(ii_Side==0) l1 -> AddEntry(g_HV[ii_Run][ii_Side], TSLegend[ii_Run], "PEL");
    }
    
    l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw();
    texSide[ii_Side] = new TLatex(0.60,0.90,TSSide[ii_Side]); texSide[ii_Side]->SetNDC(); texSide[ii_Side]->SetTextFont(42); texSide[ii_Side]->SetTextSize(0.038); texSide[ii_Side]->SetLineWidth(2); texSide[ii_Side]->Draw();
    c1 -> Print(pdfname1+".pdf","pdf");
    delete c1;
  }
  
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
