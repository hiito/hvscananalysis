/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_R21_R22.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"
#include "Math/ProbFunc.h"

#include "atlasstyle-00-04-02/AtlasStyle.h"
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

double ErrorFunc(double p0, double p1, double p2, double p3){
  double y = 0;
  for(int ii = 0; ii < 25000; ii++){
    double x = (double) ii / 100;
    // if(ROOT::Math::gaussian_cdf(x, p0, p1) > 0.95){
    if(p2*((ROOT::Math::gaussian_cdf(x, p0, p1))+p3) > 0.95){
      y = x;
      break;
    }
  }
  return y;
}


int main(int argc, char** argv)
{ 
  SetAtlasStyle();
  
  static const unsigned int nRun = 2; int mRun = 2;
  int Runnumber[nRun]    = { 361689, 361689};
  std::string TSBD[nRun] = {     "",   "b0"};
  int profile[nRun]      = {      0,      3};
  double IntLumi[nRun]   = {  144.2,  144.2};
  
  static const unsigned int nEta = 6; int mEta = 6;
  TGraphErrors* g_HV[nRun][nEta];
  double Eff95HV[nRun];
  TGraph* g_Eff95HV[nEta];
  double sigma[nRun];
  TGraph* g_sigma[nEta];
  
  TLegend * l1 = new TLegend(0.5,0.16,0.80,0.56); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  TString TSLegend[nRun] = {"September 2018 R21", "September 2018 R22"};
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");        tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.05); tex1->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.275,0.90,"SCT Internal"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  // TLatex * tex2 = new TLatex(0.275,0.90,"SCT Preliminary"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.15,0.835,"#sqrt{s} = 13 TeV until 2018");     tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.05); tex3->SetLineWidth(2);
  TLatex * tex4 = new TLatex(0.50,0.835,"#sqrt{s} = 13.6 TeV for 2022");   tex4->SetNDC(); tex4->SetTextFont(42); tex4->SetTextSize(0.05); tex4->SetLineWidth(2);
  TLatex * tex5 = new TLatex(0.45,0.77,"FirstBCOnly");   tex5->SetNDC(); tex5->SetTextFont(42); tex5->SetTextSize(0.05); tex5->SetLineWidth(2);
  TLatex * texEta[nEta];
  TString TSEta[nEta] = {"Barrel 3, |#eta_{index}| = 1", "Barrel 3, |#eta_{index}| = 2", "Barrel 3, |#eta_{index}| = 3", "Barrel 3, |#eta_{index}| = 4", "Barrel 3, |#eta_{index}| = 5", "Barrel 3, |#eta_{index}| = 6"};
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    texEta[ii_Eta] = new TLatex(0.15,0.77,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.05); texEta[ii_Eta]->SetLineWidth(2);
  }
  
  double YearLumi[4] = {4.20134, 42.6356, 92.8927, 156.185};
  TLine * tlineYear[4];
  for(int ii_year=0; ii_year<4; ii_year++){ tlineYear[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 180); tlineYear[ii_year]->SetLineStyle(1); tlineYear[ii_year]->SetLineWidth(1); }
  TLine * tlineSigma[4];
  for(int ii_year=0; ii_year<4; ii_year++){ tlineSigma[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 40);  tlineSigma[ii_year]->SetLineStyle(1); tlineSigma[ii_year]->SetLineWidth(1); }
  TLine * tline = new TLine(0, 180, 200, 180); tline->SetLineStyle(1); tline->SetLineWidth(1);
  
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_R21_R22";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");
  
  
  std::vector<TProfile*> profs;
  std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
    {3, 1, -6, 14},
    {3, 1, -6, 21},
    {3, 1, -5, 14},
    {3, 1, -5, 21},
    {3, 1, -4, 20},
    {3, 1, -3, 20},
    {3, 1, -3, 21},
    {3, 1, -2, 20},
    {3, 1, -1, 20},
    {3, 1, 1, 20},
    {3, 1, 2, 22},
    {3, 1, 3, 22},
    {3, 1, 4, 5},
    {3, 1, 4, 20},
    {3, 1, 6, 31}
  };
  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;

  TF1 * fitfunc[nRun][nEta];
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    int eta = ii_Eta + 1;
    
    TCanvas * c1 = new TCanvas("c1", "", 800, 600);
    c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 270, 1.4);
    frame1 -> SetTitle(";HV[V];Hit efficiency");
    frame1 -> GetXaxis() -> SetTitleSize(0.05);
    frame1 -> GetXaxis() -> SetLabelSize(0.05);
    frame1 -> GetXaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetTitleSize(0.05);
    frame1 -> GetYaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetLabelSize(0.05);
    
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      Config conf(Runnumber[ii_Run], true, false, 2, TSBD[ii_Run], profile[ii_Run], true, false, true);
      // Config conf(Runnumber[ii_Run], true, false, 2, TSBD[ii_Run], profile[ii_Run], true, false, false);
      std::vector<std::string> sp = conf.GetScanPoints();

      double Eff50HV = 0;
      double beforeEff = 0;
      g_HV[ii_Run][ii_Eta] = new TGraphErrors(sp.size());
      for(std::string sp : sp){
	std::cout << "------------------------------------------------------" << std::endl; 
	int SP = stoi(sp);
	profs = getProfileXExclMod(conf, getEffProf2D(conf, sp), modB3_CO100);
	if      (eta == 1){ineg = 5; ipos = 6;}
	else if (eta == 2){ineg = 4; ipos = 7;}
	else if (eta == 3){ineg = 3; ipos = 8;}
	else if (eta == 4){ineg = 2; ipos = 9;}
	else if (eta == 5){ineg = 1; ipos = 10;}
	else if (eta == 6){ineg = 0; ipos = 11;}
	ppos = profs.at(ipos);
	pneg = profs.at(ineg);
	value = ppos->GetBinContent(ppos->FindBin(eta));
	value += pneg->GetBinContent(pneg->FindBin(-1*eta));
	value = value/2;
	error1 = ppos->GetBinError(ppos->FindBin(eta));
	error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
	error = TMath::Sqrt(error1*error1 + error2*error2);
	g_HV[ii_Run][ii_Eta]->SetPoint(counter, SP, value);
	g_HV[ii_Run][ii_Eta]->SetPointError(counter, 0, error);
	std::cout << "Efficiency for run "<<Runnumber[ii_Run]<<" and eta = " << eta << " is " << value << " +/- " << error << std::endl;
	if(beforeEff<0.5 && 0.5<value) Eff50HV = SP;
	beforeEff = value;
	++counter;
      }
      counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;
      
      gROOT->GetColor(3)->SetRGB(0., 0.7, 0.);
      g_HV[ii_Run][ii_Eta] -> SetLineColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Eta] -> SetMarkerColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Eta] -> SetMarkerStyle(20+ii_Run);
      g_HV[ii_Run][ii_Eta] -> Draw("PLE sames");
      
      fitfunc[ii_Run][ii_Eta]  = new TF1("fitfunc", "[2]*((ROOT::Math::gaussian_cdf(x, [0], [1]))+[3])");
      fitfunc[ii_Run][ii_Eta] -> SetParameters(15, Eff50HV);
      fitfunc[ii_Run][ii_Eta] -> SetLineColor(mycolor(ii_Run));
      fitfunc[ii_Run][ii_Eta] -> SetLineWidth(2);
      g_HV[ii_Run][ii_Eta]    -> Fit("fitfunc","Q+","", 0, 250);
      // g_HV[ii_Run][ii_Eta]    -> Fit("fitfunc","","", 0, 250);
      Eff95HV[ii_Run] = ErrorFunc(fitfunc[ii_Run][ii_Eta]->GetParameter(0), fitfunc[ii_Run][ii_Eta]->GetParameter(1), fitfunc[ii_Run][ii_Eta]->GetParameter(2), fitfunc[ii_Run][ii_Eta]->GetParameter(3));
      sigma[ii_Run]   = fitfunc[ii_Run][ii_Eta]->GetParameter(0);
      std::cout<<fitfunc[ii_Run][ii_Eta]->GetParameter(0)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(1)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(2)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(3)<<"    "<<Eff95HV[ii_Run]<<std::endl;
      
      if(ii_Eta==0) l1 -> AddEntry(g_HV[ii_Run][ii_Eta], TSLegend[ii_Run], "PEL");
    }
    
    l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw(); tex5->Draw();
    texEta[ii_Eta]->Draw();
    c1 -> Print(pdfname1+".pdf","pdf");
    delete c1;
    
    TCanvas * c2 = new TCanvas("c2", "", 800, 600);
    c2 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame2 = (TH1F*)c2->DrawFrame(0, 0, 200, 250);
    frame2 -> SetTitle(";Delivered Integrated Luminosity [fb^{-1}];HV[mV] (Hit Efficiency=95%)");
    frame2 -> GetXaxis() -> SetTitleSize(0.05);
    frame2 -> GetXaxis() -> SetLabelSize(0.05);
    frame2 -> GetXaxis() -> SetTitleOffset(0.9);
    frame2 -> GetYaxis() -> SetTitleSize(0.05);
    frame2 -> GetYaxis() -> SetTitleOffset(0.9);
    frame2 -> GetYaxis() -> SetLabelSize(0.05);
    g_Eff95HV[ii_Eta] = new TGraph(mRun, IntLumi, Eff95HV);
    g_Eff95HV[ii_Eta] -> Draw("L sames");
    tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
    texEta[ii_Eta]->Draw();
    for(int ii_year=0; ii_year<4; ii_year++){ tlineYear[ii_year] -> Draw(); }
    // tline -> Draw();
    c2 -> Print(pdfname1+".pdf","pdf");
    delete c2;
    std::cout<<"##################################################"<<std::endl;
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      std::cout<<IntLumi[ii_Run]<<"     "<<Eff95HV[ii_Run]<<std::endl;
    }
    std::cout<<"##################################################"<<std::endl;
    
    TCanvas * c3 = new TCanvas("c3", "", 800, 600);
    c3 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame3 = (TH1F*)c3->DrawFrame(0, 0, 200, 45);
    frame3 -> SetTitle(";Delivered Integrated Luminosity [fb^{-1}];Sigma of ErrorFunction [mV]");
    frame3 -> GetXaxis() -> SetTitleSize(0.05);
    frame3 -> GetXaxis() -> SetLabelSize(0.05);
    frame3 -> GetXaxis() -> SetTitleOffset(0.9);
    frame3 -> GetYaxis() -> SetTitleSize(0.05);
    frame3 -> GetYaxis() -> SetTitleOffset(0.9);
    frame3 -> GetYaxis() -> SetLabelSize(0.05);
    g_sigma[ii_Eta] = new TGraph(mRun, IntLumi, sigma);
    g_sigma[ii_Eta] -> Draw("L sames");
    tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
    texEta[ii_Eta]->Draw();
    for(int ii_year=0; ii_year<4; ii_year++){ tlineSigma[ii_year] -> Draw(); }
    c3 -> Print(pdfname1+".pdf","pdf");
    delete c3;
    std::cout<<"##################################################"<<std::endl;
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      std::cout<<IntLumi[ii_Run]<<"     "<<sigma[ii_Run]<<std::endl;
    }
    std::cout<<"##################################################"<<std::endl;
    
  }
  
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
