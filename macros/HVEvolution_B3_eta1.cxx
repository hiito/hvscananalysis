/***********************************************************************/
/** g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_B3_eta1.cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs` **/
/***********************************************************************/
#include "include/myinclude.h"
#include "include/mycolor.h"

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"
#include "Math/ProbFunc.h"

#include "atlasstyle-00-04-02/AtlasStyle.h"
#include "atlasstyle-00-04-02/AtlasStyle.C"
#include "atlasstyle-00-04-02/AtlasLabels.C"
#include "atlasstyle-00-04-02/AtlasUtils.C"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

double ErrorFunc(double p0, double p1, double p2, double p3){
  double y = 0;
  for(int ii = 0; ii < 25000; ii++){
    double x = (double) ii / 100;
    // if(ROOT::Math::gaussian_cdf(x, p0, p1) > 0.95){
    if(p2*((ROOT::Math::gaussian_cdf(x, p0, p1))+p3) > 0.95){
      y = x;
      break;
    }
  }
  return y;
}


int main(int argc, char** argv)
{ 
  SetAtlasStyle();
  
  static const unsigned int nRun = 12; int mRun = 12;
  int Runnumber[nRun]    = {284484, 309375, 324502, 340072, 348251, 361689, 427514, 437692, 440199, 450271, 456316, 461655};
  std::string TSBD[nRun] = {    "",     "",     "",     "",   "b0",   "b0", "b0d2",   "b0",   "b0",   "b0",   "b0",   "b0"};
  // int profile[nRun]      = {     1,      1,      1,      1,      1,      3,      1,      1,      1,      1,      2,      1};
  int profile[nRun]      = {     1,      1,      1,      1,      1,      3,      1,      1,      1,      1,      2,      1};
  double IntLumi[nRun]   = {   4.2,   33.2,   42.6,   91.6,   92.9,  144.2,  156.2,  176.2,  193.9,  195.5,  224.3,  224.8};
  double Fitxmax[nRun]   = {   150,    150,    150,    150,    150,    250,    200,    250,    250,    350,    350,    350};
  double Fitxmin[nRun]   = {    10,     10,     10,     10,     10,     40,      5,     10,     10,     10,     10,     10};
  
  static const unsigned int nEta = 6; int mEta = 1;
  TGraphErrors* g_HV[nRun][nEta];
  double Eff95HV[nRun];
  TGraph* g_Eff95HV[nEta];
  double sigma[nRun];
  TGraph* g_sigma[nEta];
  
  TLegend * l1 = new TLegend(0.53,0.18,0.82,0.67); l1 -> SetBorderSize(0); l1 -> SetFillStyle(0); l1 -> SetTextSize(0.038);
  // TString TSLegend[nRun] = {"November 2015", "September 2016", "May 2017", "November 2017", "April 2018", "September 2018","7 July 2022", "21 October 2022", "21 November 2022", "22 April 2023", "11 July 2023", "27/Sep-1/Oct 2023 Pb-Pb #sqrt{s} = 5.36 TeV"};
  // TString TSLegend[nRun] = {"November 2015 4.2 [fb^{-1}]", "September 2016 33.2 [fb^{-1}]", "May 2017 42.6 [fb^{-1}]", "November 2017 91.6 [fb^{-1}]", "April 2018 92.6 [fb^{-1}]", "September 2018 144.2 [fb^{-1}]","7 July 2022 156.2 [fb^{-1}]", "21 October 2022 176.2 [fb^{-1}]", "21 November 2022 193.9 [fb^{-1}]", "22 April 2023 195.5 [fb^{-1}]", "11 July 2023 224.3 [fb^{-1}]", "1 October 2023 Pb-Pb #sqrt{s} = 5.36 TeV 224.8 [fb^{-1}]"};
  // TString TSLegend[nRun] = {"November 2015", "September 2016", "May 2017", "November 2017", "September 2018","7 July 2022", "21 October 2022", "21 November 2022"};
  TString TSLegend[nRun] = {"November 2015 4.2 [fb^{-1}]", "September 2016 33.2 [fb^{-1}]", "May 2017 42.6 [fb^{-1}]", "November 2017 91.6 [fb^{-1}]", "April 2018 92.6 [fb^{-1}]", "September 2018 144.2 [fb^{-1}]","7 July 2022 156.2 [fb^{-1}]", "21 October 2022 176.2 [fb^{-1}]", "21 November 2022 193.9 [fb^{-1}]", "22 April 2023 195.5 [fb^{-1}]", "11 July 2023 224.3 [fb^{-1}]", "1 October 2023 Pb-Pb"};
  TLatex * texPb = new TLatex(0.595,0.143,"#sqrt{s} = 5.36 TeV 224.8 [fb^{-1}]");        texPb->SetNDC();  texPb->SetTextSize(0.038); texPb->SetLineWidth(2);  // texPb->SetTextFont(42);
  TLatex * tex1 = new TLatex(0.15,0.90,"ATLAS");        tex1->SetNDC(); tex1->SetTextFont(72); tex1->SetTextSize(0.05); tex1->SetLineWidth(2);
  // TLatex * tex2 = new TLatex(0.275,0.90,"SCT Internal"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex2 = new TLatex(0.275,0.90,"SCT Preliminary"); tex2->SetNDC(); tex2->SetTextFont(42); tex2->SetTextSize(0.05); tex2->SetLineWidth(2);
  TLatex * tex3 = new TLatex(0.15,0.835,"#sqrt{s} = 13 TeV until 2018");     tex3->SetNDC(); tex3->SetTextFont(42); tex3->SetTextSize(0.05); tex3->SetLineWidth(2);
  TLatex * tex4 = new TLatex(0.50,0.835,"#sqrt{s} = 13.6 TeV from 2022");   tex4->SetNDC(); tex4->SetTextFont(42); tex4->SetTextSize(0.05); tex4->SetLineWidth(2);
  TLatex * texEta[nEta];
  TString TSEta[nEta] = {"Barrel 3, |#eta_{index}| = 1", "Barrel 3, |#eta_{index}| = 2", "Barrel 3, |#eta_{index}| = 3", "Barrel 3, |#eta_{index}| = 4", "Barrel 3, |#eta_{index}| = 5", "Barrel 3, |#eta_{index}| = 6"};
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
    texEta[ii_Eta] = new TLatex(0.15,0.77,TSEta[ii_Eta]); texEta[ii_Eta]->SetNDC(); texEta[ii_Eta]->SetTextFont(42); texEta[ii_Eta]->SetTextSize(0.05); texEta[ii_Eta]->SetLineWidth(2);
  }
  
  static const unsigned int nyear = 5; int myear = 5;
  double YearLumi[nyear] = {4.20134, 42.6356, 92.8927, 156.185, 194.5};
  TLine * tlineYear[nyear];
  for(int ii_year=0; ii_year<myear; ii_year++){ tlineYear[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 180); tlineYear[ii_year]->SetLineStyle(1); tlineYear[ii_year]->SetLineWidth(1); }
  TLine * tlineSigma[nyear];
  for(int ii_year=0; ii_year<myear; ii_year++){ tlineSigma[ii_year] = new TLine(YearLumi[ii_year], 0, YearLumi[ii_year], 40);  tlineSigma[ii_year]->SetLineStyle(1); tlineSigma[ii_year]->SetLineWidth(1); }
  TLatex * texYear[5]; TString TSYear[5] = {"2016", "2017", "2018", "2022", "2023"};
  double StartLumi[6] = { 4.2, 42.6, 92.9, 156.2, 194, 224};
  double AxisYear[5]; // = {15.7, 57.7, 111.9, 167.5, 205};
  for(int ii_year=0; ii_year<5; ii_year++){
    //  texYear[ii_year] = new TLatex( AxisYear[ii_year], 0.17, TSYear[ii_year]); texYear[ii_year]->SetNDC(); texYear[ii_year]->SetTextFont(42); texYear[ii_year]->SetTextSize(0.04); texYear[ii_year]->SetLineWidth(2);
    AxisYear[ii_year] = 0.35*(StartLumi[ii_year+1]-StartLumi[ii_year]) + StartLumi[ii_year];
    texYear[ii_year] = new TLatex( AxisYear[ii_year], 10, TSYear[ii_year]); texYear[ii_year]->SetTextFont(42); texYear[ii_year]->SetTextSize(0.04); texYear[ii_year]->SetLineWidth(2);
  }
  
  TString pdfname1 = "plots/HVScanResult_TimeEvolution_B3_eta1only";
  TCanvas * c_tmp = new TCanvas("c_tmp","", 800, 600);
  gROOT -> SetBatch();
  c_tmp -> Print(pdfname1+".pdf[","pdf");

  std::ofstream ofs("Eff95vsIntLumi.txt");
  ofs<<"Runnumber    Layer    IntegraedLuminosity[fb^-1]    HV[V]@Effciency=95%"<<std::endl;
  
  std::vector<TProfile*> profs;
  std::vector<Module> modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
    {3, 1, -6, 14},
    {3, 1, -6, 21},
    {3, 1, -5, 14},
    {3, 1, -5, 21},
    {3, 1, -4, 20},
    {3, 1, -3, 20},
    {3, 1, -3, 21},
    {3, 1, -2, 20},
    {3, 1, -1, 20},
    {3, 1, 1, 20},
    {3, 1, 2, 22},
    {3, 1, 3, 22},
    {3, 1, 4, 5},
    {3, 1, 4, 20},
    {3, 1, 6, 31}
  };
  
  float value = 0;
  float error1 = 0, error2 = 0, error = 0;
  int counter = 0;
  
  TProfile* p = nullptr;
  TProfile* ppos = nullptr;
  TProfile* pneg = nullptr;
  
  int ipos, ineg;

  TF1 * fitfunc[nRun][nEta];
  for(int ii_Eta = 0; ii_Eta < mEta; ii_Eta++){
  // for(int ii_Eta = 0; ii_Eta < 1; ii_Eta++){
    int eta = ii_Eta + 1;
    
    TCanvas * c1 = new TCanvas("c1", "", 800, 600);
    c1 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    TH1F *frame1 = (TH1F*)c1->DrawFrame(0, 0, 370, 1.4);
    frame1 -> SetTitle(";HV[V];Hit efficiency");
    frame1 -> GetXaxis() -> SetTitleSize(0.05);
    frame1 -> GetXaxis() -> SetLabelSize(0.05);
    frame1 -> GetXaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetTitleSize(0.05);
    frame1 -> GetYaxis() -> SetTitleOffset(0.9);
    frame1 -> GetYaxis() -> SetLabelSize(0.05);
    
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){ std::cout<<"L141   "<<Runnumber[ii_Run]<<std::endl;
      Config conf(Runnumber[ii_Run], true, false, 2, TSBD[ii_Run], profile[ii_Run], true, false, true, 0);
      std::vector<std::string> sp = conf.GetScanPoints();

      double Eff50HV = 0;
      double beforeEff = 0;
      g_HV[ii_Run][ii_Eta] = new TGraphErrors(sp.size());
      for(std::string sp : sp){
	std::cout << "------------------------------------------------------" << std::endl; 
	int SP = stoi(sp);
	profs = getProfileXExclMod(conf, getEffProf2D(conf, sp), modB3_CO100);
	if      (eta == 1){ineg = 5; ipos = 6;}
	else if (eta == 2){ineg = 4; ipos = 7;}
	else if (eta == 3){ineg = 3; ipos = 8;}
	else if (eta == 4){ineg = 2; ipos = 9;}
	else if (eta == 5){ineg = 1; ipos = 10;}
	else if (eta == 6){ineg = 0; ipos = 11;}
	ppos = profs.at(ipos);
	pneg = profs.at(ineg);
	value = ppos->GetBinContent(ppos->FindBin(eta));
	value += pneg->GetBinContent(pneg->FindBin(-1*eta));
	value = value/2;
	error1 = ppos->GetBinError(ppos->FindBin(eta));
	error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
	error = TMath::Sqrt(error1*error1 + error2*error2);
	g_HV[ii_Run][ii_Eta]->SetPoint(counter, SP, value);
	g_HV[ii_Run][ii_Eta]->SetPointError(counter, 0, error);
	std::cout << "Efficiency for run "<<Runnumber[ii_Run]<<" and eta = " << eta<<"  and HV "<<SP << " is " << value << " +/- " << error << std::endl;
	if(beforeEff<0.5 && 0.5<value) Eff50HV = SP;
	beforeEff = value;
	++counter;
      }
      counter = 0; value = 0; error = 0; error1 = 0; error2 = 0; ipos=-1; ineg=-1;
      
      gROOT->GetColor(3)->SetRGB(0., 0.7, 0.);
      g_HV[ii_Run][ii_Eta] -> SetLineColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Eta] -> SetMarkerColor(mycolor(ii_Run));
      g_HV[ii_Run][ii_Eta] -> SetMarkerStyle(mystyle(Runnumber[ii_Run]));
      g_HV[ii_Run][ii_Eta] -> Draw("PE sames");
      
      fitfunc[ii_Run][ii_Eta]  = new TF1("fitfunc", "[2]*((ROOT::Math::gaussian_cdf(x, [0], [1]))+[3])");
      fitfunc[ii_Run][ii_Eta] -> SetParameters(15, Eff50HV);
      fitfunc[ii_Run][ii_Eta] -> SetLineColor(mycolor(ii_Run));
      fitfunc[ii_Run][ii_Eta] -> SetLineWidth(2);
      g_HV[ii_Run][ii_Eta]    -> Fit("fitfunc","Q+","", Fitxmin[ii_Run], Fitxmax[ii_Run]);
      // g_HV[ii_Run][ii_Eta]    -> Fit("fitfunc","","", 0, 250);
      Eff95HV[ii_Run] = ErrorFunc(fitfunc[ii_Run][ii_Eta]->GetParameter(0), fitfunc[ii_Run][ii_Eta]->GetParameter(1), fitfunc[ii_Run][ii_Eta]->GetParameter(2), fitfunc[ii_Run][ii_Eta]->GetParameter(3));
      sigma[ii_Run]   = fitfunc[ii_Run][ii_Eta]->GetParameter(0);
      std::cout<<fitfunc[ii_Run][ii_Eta]->GetParameter(0)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(1)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(2)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParameter(3)<<"    "<<Eff95HV[ii_Run]<<std::endl;
      std::cout<<fitfunc[ii_Run][ii_Eta]->GetParError(0)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParError(1)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParError(2)<<"     "<<fitfunc[ii_Run][ii_Eta]->GetParError(3)<<"    "<<Eff95HV[ii_Run]<<std::endl;
      
      if(ii_Eta==0) l1 -> AddEntry(g_HV[ii_Run][ii_Eta], TSLegend[ii_Run], "PEL");
    }
    std::cout<<"L193   "<<std::endl;
    
    l1->Draw(); tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
    texEta[ii_Eta]->Draw(); texPb->Draw();
    c1 -> Print(pdfname1+".pdf","pdf");
    delete c1;

    std::cout<<"L192   "<<std::endl;
    TCanvas * c2 = new TCanvas("c2", "", 800, 600);
    c2 -> SetMargin(0.13, 0.02, 0.15, 0.03);
    // c2 -> SetMargin(0.1, 0.02, 0.12, 0.3);
    TH1F *frame2 = (TH1F*)c2->DrawFrame(0, 0, 225, 250);
    // TH1F *frame2 = (TH1F*)c2->DrawFrame(0, 0, 200, 200);
    frame2 -> SetTitle(";Delivered Integrated Luminosity from Run2 [fb^{-1}];HV[V] (Hit Efficiency=95%)");
    frame2 -> GetXaxis() -> SetTitleSize(0.05);
    frame2 -> GetXaxis() -> SetLabelSize(0.05);
    frame2 -> GetXaxis() -> SetTitleOffset(1.1);
    frame2 -> GetYaxis() -> SetTitleSize(0.05);
    frame2 -> GetYaxis() -> SetTitleOffset(1.1);
    frame2 -> GetYaxis() -> SetLabelSize(0.05);
    for(int ii_year=0; ii_year<myear; ii_year++){ tlineYear[ii_year] -> Draw(); }
    g_Eff95HV[ii_Eta] = new TGraph(mRun, IntLumi, Eff95HV);
    g_Eff95HV[ii_Eta] ->SetMarkerColor(2);
    g_Eff95HV[ii_Eta] ->SetLineColor(2);
    g_Eff95HV[ii_Eta] -> Draw("P L sames");
    tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
    texEta[ii_Eta]->Draw();
    for(int ii_year=0; ii_year<myear; ii_year++){ texYear[ii_year] -> Draw(); }
    // tline -> Draw();
    c2 -> Print(pdfname1+".pdf","pdf");
    delete c2;
    std::cout<<"##################################################"<<std::endl;
    for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
      std::cout<<IntLumi[ii_Run]<<"     "<<Eff95HV[ii_Run]<<std::endl;
      ofs<<Runnumber[ii_Run]<<"    "<<TSEta[ii_Eta]<<"   "<<IntLumi[ii_Run]<<"     "<<Eff95HV[ii_Run]<<std::endl;
    }
    std::cout<<"##################################################"<<std::endl;
    
    // TCanvas * c3 = new TCanvas("c3", "", 800, 600);
    // c3 -> SetMargin(0.1, 0.02, 0.12, 0.03);
    // TH1F *frame3 = (TH1F*)c3->DrawFrame(0, 0, 225, 60);
    // frame3 -> SetTitle(";Delivered Integrated Luminosity from Run2 [fb^{-1}];Sigma of ErrorFunction [V]");
    // frame3 -> GetXaxis() -> SetTitleSize(0.05);
    // frame3 -> GetXaxis() -> SetLabelSize(0.05);
    // frame3 -> GetXaxis() -> SetTitleOffset(0.9);
    // frame3 -> GetYaxis() -> SetTitleSize(0.05);
    // frame3 -> GetYaxis() -> SetTitleOffset(0.9);
    // frame3 -> GetYaxis() -> SetLabelSize(0.05);
    // for(int ii_year=0; ii_year<myear; ii_year++){ tlineSigma[ii_year] -> Draw(); }
    // g_sigma[ii_Eta] = new TGraph(mRun, IntLumi, sigma);
    // g_sigma[ii_Eta] ->SetMarkerColor(2);
    // g_sigma[ii_Eta] ->SetLineColor(2);
    // g_sigma[ii_Eta] -> Draw("P L sames");
    // tex1->Draw(); tex2->Draw(); tex3->Draw(); tex4->Draw();
    // texEta[ii_Eta]->Draw();
    // for(int ii_year=0; ii_year<myear; ii_year++){ texYear[ii_year] -> Draw(); }
    // c3 -> Print(pdfname1+".pdf","pdf");
    // delete c3;
    // std::cout<<"##################################################"<<std::endl;
    // for(int ii_Run = 0; ii_Run < mRun; ii_Run++){
    //   std::cout<<IntLumi[ii_Run]<<"     "<<sigma[ii_Run]<<std::endl;
    // }
    // std::cout<<"##################################################"<<std::endl;
    
  }
  
  c_tmp -> Print(pdfname1+".pdf]","pdf");  
  return 0;
}
