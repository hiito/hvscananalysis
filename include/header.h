#include "myinclude.h"

TString RunDate(int Run = 0){
  if(Run==284484)        return "November 2015 4.2 [fb^{-1}]";
  else if(Run==309375)   return "September 2016 33.2 [fb^{-1}]";
  else if(Run==324502)   return "May 2017 42.6 [fb^{-1}]";
  else if(Run==340072)   return "November 2017 91.6 [fb^{-1}]";
  else if(Run==348251)   return "April 2018 92.6 [fb^{-1}]";
  else if(Run==361689)   return "September 2018 144.2 [fb^{-1}]";
  else if(Run==427514)   return "7 July 2022 156.2 [fb^{-1}]";
  else if(Run==437692)   return "21 October 2022 176.2 [fb^{-1}]";
  else if(Run==440199)   return "21 November 2022 193.9 [fb^{-1}]";
  else if(Run==450271)   return "22 April 2023 195.5 [fb^{-1}]";
  else if(Run==456316)   return "11 July 2023 224.3 [fb^{-1}]";
  else if(Run==461655)   return "1 October 2023 Pb-Pb";
  else if(Run==472553)   return "5 April 2024 224.8 [fb^{-1}]";
  else if(Run==479374)   return "2 July 2024 261.4 [fb^{-1}]";
  else if(Run==486640)   return "12 October 2024 349.5 [fb^{-1}]";
}

double IntLuminosity(int Run = 0){
  if(Run==284484)        return 4.2;
  else if(Run==309375)   return 33.2;
  else if(Run==324502)   return 42.6;
  else if(Run==340072)   return 91.6;
  else if(Run==348251)   return 92.6;
  else if(Run==361689)   return 144.2;
  else if(Run==427514)   return 156.2;
  else if(Run==437692)   return 176.2;
  else if(Run==440199)   return 193.9;
  else if(Run==450271)   return 195.5;
  else if(Run==456316)   return 224.3;
  else if(Run==461655)   return 224.5;
  else if(Run==472553)   return 224.8;
  else if(Run==479374)   return 261.4;
  else if(Run==486640)   return 349.5;
}
