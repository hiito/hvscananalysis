#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <string>
#include <vector>

class Config
{
	
	private:
		int 				m_run;
		bool 				m_isBarrel;
		bool 				m_isEA;
		int 				m_side;
		std::string m_BD;
		int 				m_stream; 
		bool 				m_plotEtaCurves;
		bool 				m_plotPhiCurves;
		bool				m_BCID;
                int                             m_layer;
	
	public:
                Config(int run, bool isBarrel, bool isEA, int side, std::string BD, int stream, bool plotEtaCurves, bool plotPhiCurves, bool BCID, int layer);
                void SetConfiguration(int run, bool isBarrel, bool isEA, int side, std::string BD, int stream, bool plotEtaCurves, bool plotPhiCurves, bool BCID, int layer);
		void PrintConfiguration();
		int GetRunNumber();
		bool GetIsBarrel();
		bool GetIsEA();
		int GetSide();
		std::string GetBD();
		int GetStream();
		bool PlotEtaCurves();
		bool PlotPhiCurves();
		int GetScannedLayer();
		int GetScannedDisk();
		bool GetBCID();
                int GetLayer();
		std::vector<int> GetPhiLow();
		std::vector<int> GetPhiUp();
		std::vector<int> GetAbsEtaIndices();
		std::vector<std::string> GetScanPoints();
};

#endif
