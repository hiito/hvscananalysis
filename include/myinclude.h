#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <bitset>
#include <sstream>
#include <TPaveStats.h>
#include <TPaletteAxis.h>
/* #include <ctime> */
/* #include <chrono> */
/* #include <boost/chrono.hpp> */
/* #include <sys/time.h> */

#include "TApplication.h"
#include "TArc.h"
#include "TBasket.h"
#include "TBranch.h"
#include "TBrowser.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TClass.h"
#include "TCut.h"
#include "TEfficiency.h"
#include "TF1.h"
#include "TF2.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "THStack.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TLine.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TObject.h"
#include "TPad.h"
#include "TPave.h"
#include "TROOT.h"
#include "TRandom.h"
#include "TRotation.h"
#include "TStopwatch.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TTree.h"
#include "TVector3.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TRatioPlot.h"
using namespace std;
