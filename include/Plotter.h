#ifndef PLOTTER_H
#define PLOTTER_H

#include <vector>
#include <string>

#include "TGraphErrors.h"
#include "TCanvas.h"

#include "include/Config.h"

class Plotter
{
	private:
		std::vector<int> 		m_graphColor;
		std::vector<int> 		m_marker;
		//~ std::vector<string> m_graphName;
		//~ std::vector<string> m_legEntry;
	
	public:
		Plotter();
		void PlotGraphs(Config configuration, std::vector<TGraphErrors*> graphs);
		void AddATLASLabel(TCanvas* c);
		std::string GetLegendTitle(Config configuration);
		std::string GetPlotTitle(Config configuration);
};


#endif
