#ifndef MODULE_H
#define MODULE_H

class Module
{
	
	private:
		int m_layer;
		int m_side;
		int m_eta;
		int m_phi;
	
	public:
		Module(int layer, int side, int eta, int phi);
		int GetLayer();
		int GetSide();
		int GetEta();
		int GetPhi();
};

#endif
