#ifndef HVSCANFUNCTIONS
#define HVSCANFUNCTIONS

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"

std::map<int, std::map<int, float>> getHVCurrentMap(Config configuration);
double getShiftedHV(Config configuration, std::string HVpoint, int eta);

TFile* OpenFile(Config configuration, std::string HVpoint);

TProfile2D* getEffProf2D(Config configuration, std::string sp);
std::vector<TProfile*> getProfileX(Config configuration, TProfile2D* prof2D);
std::vector<TProfile*> getProfileXExclMod(Config configuration, TProfile2D* prof2D, std::vector<Module> modules);

void getEffPlots(Config configuration);
void getEffPlotsExclMod(Config configuration, std::vector<Module> exclModules);


#endif
