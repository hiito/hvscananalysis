#include "myinclude.h"
int mycolor(int Run = 0){
  int color[16] = {1, 2, kCyan-8, 4, kOrange+2, 6, 3, kAzure+4, kSpring+8, kViolet-5, kTeal+6, kPink+2, kOrange-3, kRed-2, kGreen+3, kBlue+3};
  
  if(Run==284484)        return color[0];
  else if(Run==309375)   return color[1];
  else if(Run==324502)   return color[2];
  else if(Run==340072)   return color[3];
  else if(Run==348251)   return color[4];
  else if(Run==361689)   return color[5];
  else if(Run==427514)   return color[6];
  else if(Run==437692)   return color[7];
  else if(Run==440199)   return color[8];
  else if(Run==450271)   return color[9];
  else if(Run==456316)   return color[10];
  else if(Run==461655)   return color[11];
  else if(Run==472553)   return color[12];
  else if(Run==479374)   return color[13];
  else                   return color[15];
}


int mystyle(int Run = 0){
  if(Run==284484)        return 20;
  else if(Run==309375)   return 21;
  else if(Run==324502)   return 22;
  else if(Run==340072)   return 23;
  else if(Run==348251)   return 29;
  else if(Run==361689)   return 33;
  else if(Run==427514)   return 24;
  else if(Run==437692)   return 25;
  else if(Run==440199)   return 26;
  else if(Run==450271)   return 27;
  else if(Run==456316)   return 28;
  else if(Run==461655)   return 30;
  else if(Run==472553)   return 32;
  else if(Run==479374)   return 46;
  else                   return 42;
}
