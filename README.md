# HVScanAnalysis

## How to use
```
setupATLAS
lsetup "root 6.30.02-x86_64-el9-gcc13-opt"
 g++ -std=c++14 -o evolution src/HVscanFunctions.cxx macros/HVEvolution_(layer).cxx src/Config.cxx src/Module.cxx src/Plotter.cxx `root-config --cflags --glibs`
 layer : Barrel3->B3,  Disk2->D2, etc...
./evolution
pdf file is made in plots folder.
```

## How to make input file
The official DQ HIST file have the efficiency of the average of one Run and doesn't have the efficiency of the average of LB.
DQ HIST file for each LB must be make.
After making DQ HIST file, the folders in MakeInput (b0, b3, d2, d5) are copied into the DQ HIST folder.
And fix RunNumber, inHV, startLB, and endLB for mergeHIST.
After fixing, execute below.
g++ -std=c++14 -o analysis mergeHIST.cxx `root-config --cflags --glibs`
./analysis
