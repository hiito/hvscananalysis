#include "include/HVscanFunctions.h"

#include <iostream>
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

using namespace std;

std::map<int, std::map<int, float>> getHVCurrentMap(Config configuration)
{
	
	int iRun = configuration.GetRunNumber();
	bool isBarrel = configuration.GetIsBarrel();
	bool isEA = configuration.GetIsEA();
	
	std::string filename;
	if (iRun == 361689){
		if (isBarrel) filename = "run361689_B3_HVcurrent.csv";
		else {
			if (isEA) filename = "run361689_EA6_HVcurrent.csv";
			else filename = "run361689_EC6_HVcurrent.csv";
		}
	}
	
	std::ifstream file;
	file.open(filename.data());
	
	std::map<int, std::map<int, float>> HVCurrentMap;
	int HVKey;
	int etaKey;
	std::string line;
	std::string field;
	int ifield;
	float ffield;
	
	while(file.good()) {
		std::getline(file, line);
		//~ std::cout << "Current line " << line << std::endl;
		std::istringstream sstream(line);
		int i = 0;
		while (std::getline(sstream, field, ',')){
			ifield = std::stoi(field);
			ffield = std::stof(field);
			//~ std::cout << "string field "<< field  << std::endl; 
			//~ std::cout << " int " << ifield << " float " << ffield << std::endl;
			if (i==0) {HVKey = ifield; ++i; continue; }
			else {
				etaKey = i;
			}
			HVCurrentMap[HVKey][etaKey] = ffield;
			++i;
		}
  }
  return HVCurrentMap;
	
}


double getShiftedHV(Config configuration, std::string HVpoint, int eta)
{
	int iRun = configuration.GetRunNumber();
	
	// std::cout << "Inside getShiftedHV() function for run " << iRun << " and HVpoint " << HVpoint << std::endl;
	
	int iHV = std::stoi(HVpoint);
	double shiftedHV = 0;
	double r = 11.2e3;//resistance
	double current = 0;
	
	std::map<int, std::map<int, float>> HVCurrentMap = getHVCurrentMap(configuration);
	
	current = HVCurrentMap[iHV][eta];
	shiftedHV = iHV - r*current*1e-6;
	
	return shiftedHV;
}


TFile* OpenFile(Config configuration, std::string HVpoint)
{
  // std::cout << "Inside OpenFile function: HVpoint " << HVpoint << std::endl;
	
	int iRun = configuration.GetRunNumber();
	std::string sRun = std::to_string(iRun);
	int stream = configuration.GetStream();
	std::string BD = configuration.GetBD();
	std::string filename, fullpath;
	
	switch (iRun){
	  
	case 486640 :
	  if (stream == 0) {
	    std::cout << "" << std::endl;
            exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run486640_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data24_13p6TeV.00486640.physics_Main.HIST.f1519/")+filename;
	  }
	  break;
	  
	case 479374 :
	  if (stream == 0) {
	    std::cout << "" << std::endl;
            exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run479374_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data24_13p6TeV.00479374.physics_Main.HIST.f1484/")+filename;
	  }
	  break;
	  
	case 472553 :
	  if (stream == 0) {
	    filename = BD+std::string("/run472553_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data24_13p6TeV.00472553.physics_Main.HIST.x812/")+filename;
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run472553_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data24_13p6TeV.00472553.physics_Main.HIST.f1437/")+filename;
	  }
	  break;
	  
	case 461899 :
	  if (stream == 0){
	    std::cout << "" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run461899_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_hi.00461899.physics_UPC.HIST.x787/")+filename;
	  }
	  else if (stream == 2) {
	    filename = BD+std::string("_oneLB/run461899_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_hi.00461899.physics_UPC.HIST.x787/")+filename;
	  }
	  else if (stream == 3) {
	    filename = BD+std::string("_twoLB/run461899_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_hi.00461899.physics_UPC.HIST.x787/")+filename;
	  }
	  break;
	  
	case 461655 :
	  if (stream == 0){
	    std::cout << "" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1 || stream == 2) {
	    filename = BD+std::string("/run461655_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_hi.00461655.physics_UPC.HIST.x780/")+filename;
	  }
	  else if (stream == 3) {
	    filename = BD+std::string("/run461655_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_hi.00461655.physics_HardProbes.HIST.x786/")+filename;
	  }
	  break;
	  
	  
	case 456316 :
	  if (stream == 0){
	    std::cout << "" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run456316_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_13p6TeV.00456316.express_express.HIST.x754/")+filename;
	  }
	  else if (stream == 2) {
	    filename = BD+std::string("/run456316_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_13p6TeV.00456316.physics_Main.HIST.x754/")+filename;
	  }
	  break;
	  
	case 450271 :
	  if (stream == 0){
	    std::cout << "" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run450271_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_13p6TeV.00450271.physics_Main.recon.HIST.x731/")+filename;
	  }
	  break;
	  
	case 440199 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 427514" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run440199_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data22_13p6TeV.00440199.express_express.recon.HIST.x711/")+filename;
	  }
	  break;
	  
	case 437692 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 427514" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = BD+std::string("/run437692_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data22_13p6TeV.00437692.express_express.recon.HIST.x705/")+filename;
	  }
	  else if (stream == 2) {
	    filename = std::string("OUT.HIST.lb0")+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data22_13p6TeV.00437692.express_express.recon.HIST.x705/")+filename;
	  }
	  else if (stream == 3) {
	    filename = BD+std::string("/run437692_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data22_13p6TeV.00437692.express_express.recon.HIST.x705/")+filename;
	  }
	  break;
	  
	case 427514 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 427514" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = std::string("run427514_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data22_13p6TeV.00427514.express_express.recon.HIST.x668/ForInput/")+filename;
	  }
	  break;
	  
	case 361689 :
	  if (stream == 0){
	    filename = std::string("physics_HV")+HVpoint.data()+std::string(".root");
	    //~ fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  }
	  else if (stream == 1) {
	    //Kazuya's files
	    //~ filename = std::string("express_361689_HV")+HVpoint.data()+std::string(".root");
	    //~ fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/express/")+filename;
	    //Shigeki's reprocessed files
	    filename = std::string("run361689_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("_express/")+filename;
	  }
	  else if (stream == 2) {
	    filename = BD+std::string("/run361689_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data18_13TeV.00361689.express_express.merge.HIST.f1042/")+filename;
	  }
	  else if (stream == 3) {
	    filename = BD+std::string("/run361689_")+BD+"_"+HVpoint.data()+std::string(".root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/user.hiito.data18_13TeV.00361689.physics_Main.merge.HIST.f1042_EXT0/")+filename;
	  }
	  break;
	  
	  
	case 360414 :
	  // if (stream == 0){
	  //   filename = std::string("physics_HV")+HVpoint.data()+std::string(".root");
	  //   fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	  // }
	  // else {
	  //   filename = std::string("express_HV")+HVpoint.data()+std::string(".root");
	  //   fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/express/")+filename;
	  // }
	  // break;
	  
	  filename = std::string("run360414_")+HVpoint.data()+std::string(".root");
	  fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data18_13TeV.00360414/")+filename;
	  break;
	  
	case 363664 : 
	  filename = std::string("HV")+HVpoint.data()+std::string(".root");
	  fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	  break;
	case 367134 :
	  filename = std::string("HV")+HVpoint.data()+std::string(".root");
	  fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	  break;
	case 367170 :
	  filename = std::string("HV")+HVpoint.data()+std::string("V.root");
	  fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	  break;
	case 348251 :
	  // if (stream == 0){
	  //   filename = std::string("run")+sRun+std::string("_")+BD+std::string("_")+HVpoint+std::string("V.root");
	  //   //Kazuya's files
	  //   //~ fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+sRun+std::string("/")+filename;
	  //   //Shigeki's reprocessed files 
	  //   fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  // }
	  // else if(stream == 1){
	  //   filename = std::string("run")+sRun+std::string("_")+BD+std::string("_")+HVpoint+std::string("V.root");
	  //   fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("_express/")+filename;
	  // }
	  filename = BD+std::string("/run348251_")+BD+"_"+HVpoint.data()+std::string(".root");
	  fullpath = std::string("/eos/user/h/hiito/SCT/HVScan/data18_13TeV.00348251.physics_Main/")+filename;
	  break;
	case 324502 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 324502" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = std::string("run324502_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  }
	  else if (stream == 2) {
	    filename = std::string("run324502_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data17_13TeV.00324502.express_express")+std::string("/")+filename;
	  }
	  break;
	case 309375 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 309375" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = std::string("run309375_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  }
	  else if (stream == 2) {
	    filename = std::string("run309375_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data16_13TeV.00309375.express_express")+std::string("/")+filename;
	  }
	  break;
	case 340072 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 340072" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = std::string("run340072_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  }
	  else if (stream == 2) {
	    filename = std::string("run340072_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data17_13TeV.00340072.express_express")+std::string("/")+filename;
	  }
	  break;
	case 284484 :
	  if (stream == 0){
	    std::cout << "There is no data from physicsMain for run 284484" << std::endl;
	    exit(0);
	  }
	  else if (stream == 1) {
	    filename = std::string("run284484_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/user/h/hborecka/SCTReprocessedFiles4Paper/")+sRun+std::string("/")+filename;
	  }
	  else if (stream == 2) {
	    filename = std::string("run284484_")+HVpoint.data()+std::string("V.root");
	    fullpath = std::string("/eos/atlas/atlascerngroupdisk/det-sct/run2analysis/HV_scan/data15_13TeV.00284484.express_express")+std::string("/")+filename;
	  }
	  break;
	}
	
	std::cout << "Opening file: " << fullpath << std::endl;
	TFile* f = TFile::Open(fullpath.data());
	return f;
}



TProfile2D* getEffProf2D(Config configuration, std::string sp) //sp=ScanPoint
{
	// std::cout << "Inside getEffProf2D function, run " << configuration.GetRunNumber() << " sp " << sp << std::endl;
	// std::cout << "conf.isBarrel = " << configuration.GetIsBarrel() << " conf.isEA " << configuration.GetIsEA() << std::endl;
	
	int iRun = configuration.GetRunNumber();
	bool BCID = configuration.GetBCID();
	bool isBarrel = configuration.GetIsBarrel();
	bool isEA = configuration.GetIsEA();
	int side = configuration.GetSide();
	int layer = configuration.GetScannedLayer();
	int disk = configuration.GetScannedDisk();
	
	// std::cout << "isBarrel " << isBarrel << " isEA " << isEA << std::endl;
	
	TFile* f = OpenFile(configuration, sp);
	
	string runPrefix = std::string("run_")+std::to_string(iRun);
	string BEC = isBarrel ? std::string("SCTB") : (isEA ? std::string("SCTEA") : std::string("SCTEC"));
	string eff = isBarrel ? std::string("eff") : (isEA ? std::string("p_eff") : std::string("m_eff"));
	string LD = isBarrel ? std::to_string(layer) : std::to_string(disk);
	
	string profPath = runPrefix.data()+std::string("/SCT/")+BEC.data()+std::string("/eff/")+eff.data()+std::string("_")+LD.data();
	//~ if (BCID) profPath = runPrefix.data()+std::string("/SCT/")+BEC.data()+std::string("/eff/")+eff.data()+std::string("_bcid_")+LD.data();
	if (BCID) profPath = runPrefix.data()+std::string("/SCT/")+BEC.data()+std::string("/eff/")+eff.data()+std::string("_")+LD.data();
	// if (iRun == 348251) profPath = std::string("SCT/")+BEC.data()+std::string("/eff/")+eff.data()+std::string("_")+LD.data();
	
	TProfile2D* prof = nullptr;
	
	if (side == 0){
		profPath = profPath.data()+std::string("_0");
		if (BCID) profPath = profPath.data()+std::string("_bcid");
		// std::cout << "profPath = " << profPath << std::endl;
		prof = dynamic_cast<TProfile2D*>(f->Get(profPath.data()));
	}
	else if (side == 1){
		profPath = profPath.data()+std::string("_1");
		if (BCID) profPath = profPath.data()+std::string("_bcid");
		// std::cout << "profPath = " << profPath << std::endl;
		prof = dynamic_cast<TProfile2D*>(f->Get(profPath.data()));
	}
	else if (side == 2){ //Average of Side
		string profPath0 = profPath.data()+std::string("_0");
		string profPath1 = profPath.data()+std::string("_1");
		if (BCID) { //first BCID
			profPath0 = profPath0.data()+std::string("_bcid");
			profPath1 = profPath1.data()+std::string("_bcid");
		}
		TProfile2D* prof0 = dynamic_cast<TProfile2D*>(f->Get(profPath0.data()));
		TProfile2D* prof1 = dynamic_cast<TProfile2D*>(f->Get(profPath1.data()));
		// std::cout << "profPath0 = " << profPath0 << std::endl;
		// std::cout << "profPath1 = " << profPath1 << std::endl;
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	
	return prof;
}



std::vector<TProfile*> getProfileX(Config configuration, TProfile2D* prof2D)
{
  // std::cout << "Inside getProfileX() function:  prof2D " << prof2D << std::endl;
  
  bool isBarrel = configuration.GetIsBarrel();
  bool isEA = configuration.GetIsEA();
  std::vector<int> phiLow = configuration.GetPhiLow();
  std::vector<int> phiUp = configuration.GetPhiUp();
  int iRun = configuration.GetRunNumber();
  std::vector<int> etaIndices = configuration.GetAbsEtaIndices();
  
  std::vector<TProfile*> profs;
  TProfile* p = nullptr;
  
  if (isBarrel){
    p = prof2D->ProfileX();
    profs.push_back(p);
  }
  else if (isBarrel == false){
    // for (unsigned int i=0; i<etaIndices.size(); ++i){
    // for (unsigned int i=0; i<3; ++i){
    for (unsigned int i=0; i<phiLow.size(); ++i){
      string name = std::string("px_m")+std::to_string(i);
      p = prof2D->ProfileX(name.data(), phiLow.at(i), phiUp.at(i));
      profs.push_back(p);
      // std::cout<<i << "   Phi range = (" << phiLow.at(i) << ", "  << phiUp.at(i) << "), adding prof " << name.data() << " for eta " << etaIndices.at(i) << std::endl;
    }
  }
  return profs;
}



std::vector<TProfile*> getProfileXExclMod(Config configuration, TProfile2D* prof2D, std::vector<Module> modules)
{
  //for now the functionality implemented only for barrel modules
  // std::cout << "Inside getProfileXExclMod() function: prof2D " << prof2D << std::endl;
  
  //~ std::cout << "Excluding the following modules: " << std::endl;
  //~ for (auto module : modules){
  //~ std::cout << "Module: layer " << module.GetLayer() << " side " << module.GetSide() << " eta " << module.GetEta() << " phi " << module.GetPhi() << std::endl;
  //~ }
  
  bool isBarrel = configuration.GetIsBarrel();
  bool isEA = configuration.GetIsEA();
  int layer = configuration.GetLayer();
  std::vector<int> phiLow = configuration.GetPhiLow();
  std::vector<int> phiUp = configuration.GetPhiUp();
  int iRun = configuration.GetRunNumber();
  //~ std::vector<int> etaIndices = configuration.GetAbsEtaIndices();
  
  std::vector<int> etaIndices = {-6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6}; 
  std::vector<std::vector<int>> exclPhi(12);
  int maxPhi = 0;
  if (iRun == 361689 && isBarrel == true) maxPhi = 31; //maxPhi for barrel3
  if (isBarrel==true && layer==0) maxPhi = 32;
  if (isBarrel==true && layer==3) maxPhi = 56;
  
  std::vector<TProfile*> profs;
  TProfile* p = nullptr;
  TProfile* p1 = nullptr;
  TProfile* p2 = nullptr;
  TProfile* p3 = nullptr;
  
  if (isBarrel){
    if (modules.size() == 0){
      p = prof2D->ProfileX();
      profs.push_back(p);
    }
    else {
      //check how many excluded modules are there for each eta 
      for (int i=0; i<etaIndices.size(); ++i){
	for (int j=0; j<modules.size(); ++j){
	  if (etaIndices.at(i) == modules.at(j).GetEta()){
	    exclPhi.at(i).push_back(modules.at(j).GetPhi());	    
	  }
	}
      }
      
      // if(isBarrel==true && layer==0) exclPhi.at(0).push_back(12);
      // if(isBarrel==true && layer==0) exclPhi.at(2).push_back(22);
      // if(isBarrel==true && layer==0) exclPhi.at(4).push_back(3);
      // if(isBarrel==true && layer==0) exclPhi.at(10).push_back(14);
      // if(isBarrel==true && layer==0) exclPhi.at(11).push_back(14);
      
      // if(isBarrel==true && layer==3) exclPhi.at(4).push_back(6);
      // if(isBarrel==true && layer==3) exclPhi.at(6).push_back(8);
      // if(isBarrel==true && layer==3) exclPhi.at(6).push_back(38);
      // if(isBarrel==true && layer==3) std::cout<<etaIndices.at(6)<<"   "<<exclPhi.at(6).at(0)<<"   "<<exclPhi.at(6).at(1)<<"   "<<exclPhi.at(6).at(2)<<std::endl;
      
      
      //~ std::cout << "After finding indices for excluded modules" << std::endl;
      for (int i=0; i<etaIndices.size(); ++i){
	if (exclPhi.at(i).size() == 1){
	  //this works only if the modules are passed on in the right order of phi, first smallest phi later highest
	  //~ std::cout << "Found 1 excluded module for this eta (" << etaIndices.at(i) << ") " << exclPhi.at(i).at(0) << " creating 2 TProfiles" << std::endl;
	  p = prof2D->ProfileX("m_0", 0, exclPhi.at(i).at(0)-1);
	  p->SetDirectory(0);
	  p1 = prof2D->ProfileX("m_1", exclPhi.at(i).at(0)+1, maxPhi);
	  p->SetDirectory(0);
	  p->Add(p1);
	}
	else if (exclPhi.at(i).size() == 2){
	  //~ std::cout << "Found 2 excluded modules for this eta (" << etaIndices.at(i) << ") " << exclPhi.at(i).at(0) << " and " <<  exclPhi.at(i).at(1) << " creating 3 TProfiles" << std::endl;
	  p = prof2D->ProfileX("m_2", 0, exclPhi.at(i).at(0)-1);
	  p->SetDirectory(0);
	  p1 = prof2D->ProfileX("m_3", exclPhi.at(i).at(0)+1, exclPhi.at(i).at(1)-1);
	  p1->SetDirectory(0);
	  p2 = prof2D->ProfileX("m_4", exclPhi.at(i).at(1)+1, maxPhi);
	  p2->SetDirectory(0);
	  p->Add(p1);
	  p->Add(p2);
	}
	else if (exclPhi.at(i).size() == 3){
	  //~ std::cout << "Found 2 excluded modules for this eta (" << etaIndices.at(i) << ") " << exclPhi.at(i).at(0) << " and " <<  exclPhi.at(i).at(1) << " creating 3 TProfiles" << std::endl;
	  p = prof2D->ProfileX("m_2", 0, exclPhi.at(i).at(0)-1);
	  p->SetDirectory(0);
	  p1 = prof2D->ProfileX("m_3", exclPhi.at(i).at(0)+1, exclPhi.at(i).at(1)-1);
	  p1->SetDirectory(0);
	  p2 = prof2D->ProfileX("m_4", exclPhi.at(i).at(1)+1, exclPhi.at(i).at(2)-1);
	  p2->SetDirectory(0);
	  p3 = prof2D->ProfileX("m_5", exclPhi.at(i).at(2)+1, maxPhi);
	  p3->SetDirectory(0);
	  p->Add(p1);
	  p->Add(p2);
	  p->Add(p3);
	}
	
	profs.push_back(p);
	p->Clear();
	p1->Clear();
	p2->Clear();
	// p3->Clear();
      }
    }
  }
  else if (isBarrel == false){
    // for (unsigned int i=0; i<etaIndices.size(); ++i){
    // for (unsigned int i=0; i<3; ++i){
    for (unsigned int i=0; i<phiLow.size(); ++i){
      string name = std::string("px_m")+std::to_string(i);
      p = prof2D->ProfileX(name.data(), phiLow.at(i), phiUp.at(i));
      profs.push_back(p);
      // std::cout<<i << "     Phi range = (" << phiLow.at(i) << ", "  << phiUp.at(i) << "), adding prof " << name.data() << " for eta " << etaIndices.at(i) << std::endl;
    }
  }
  // std::cout << "Returning vector of " << profs.size() << " profiles" << std::endl;
  return profs;
}




void getEffPlots(Config configuration)
{
  bool isBarrel = configuration.GetIsBarrel();
  bool isEA = configuration.GetIsEA();
  int side = configuration.GetSide();
  std::vector<int> etaIndices = configuration.GetAbsEtaIndices();
  std::vector<std::string> scanPoints = configuration.GetScanPoints();
  
  std::vector<TProfile*> profs;
  std::vector<TGraphErrors*> graphs;
  TGraphErrors *gEff = nullptr;
  
  for (auto eta : etaIndices){
    int i = 0; //to iterate over scan points
    gEff = new TGraphErrors(scanPoints.size());
    gEff->SetName(TString::Format("graph_%d", eta).Data());
    float value = 0;
    float error1 = 0, error2 = 0, error = 0;
    
    for (auto sp : scanPoints){
      TProfile* p = nullptr;
      if (isBarrel){
	profs = getProfileX(configuration, getEffProf2D(configuration, sp));
	p = profs.at(0);
	value = p->GetBinContent(p->FindBin(eta));
	value += p->GetBinContent(p->FindBin(-1*eta));
	value = value/2;
	error1 = p->GetBinError(p->FindBin(eta));
	error2 = p->GetBinError(p->FindBin(-1*eta));
	error = TMath::Sqrt(error1*error1 + error2*error2);
      }
      else if (isBarrel == false) {
	profs = getProfileX(configuration, getEffProf2D(configuration, sp));
	p = profs.at(eta-1);
	value = p->GetBinContent(p->FindBin(eta-1));//because eta indices for the endcaps starts from 0 and ends at 2
	error = p->GetBinError(p->FindBin(eta-1));
      }
      std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
      if (value > 0.98) std::cout << "Efficiency above 98% for HV = " << std::stoi(sp) << std::endl;
      // std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
      gEff->SetPoint(i, std::stoi(sp), value);
      gEff->SetPointError(i, 0, error);
      ++i;
      value = 0; 
      error = 0;
      error1 = 0;
      error2 = 0;
    }
    graphs.push_back(gEff);
  }
  Plotter plotter;
  std::cout << "Creating plotting object " << &plotter << std::endl;
  plotter.PlotGraphs(configuration, graphs);
}




void getEffPlotsExclMod(Config configuration, std::vector<Module> exclModules)
{
  std::cout << "Inside getEffPlotsExclMod() function " << std::endl;
  bool isBarrel = configuration.GetIsBarrel();
  bool isEA = configuration.GetIsEA();
  int side = configuration.GetSide();
  std::vector<int> etaIndices = configuration.GetAbsEtaIndices();
  std::vector<std::string> scanPoints = configuration.GetScanPoints();
  
  std::vector<TProfile*> profs;
  std::vector<TGraphErrors*> graphs;
  TGraphErrors *gEff = nullptr;
  
  int ipos, ineg;
  
  for (auto eta : etaIndices){
    int i = 0; //to iterate over scan points
    gEff = new TGraphErrors(scanPoints.size());
    gEff->SetName(TString::Format("graph_%d", eta).Data());
    float value = 0;
    float error1 = 0, error2 = 0, error = 0;
    
    for (auto sp : scanPoints){
      TProfile* p = nullptr;
      TProfile* ppos = nullptr;
      TProfile* pneg = nullptr;
      if (isBarrel){
	profs = getProfileXExclMod(configuration, getEffProf2D(configuration, sp), exclModules);
	if (eta == 1){ineg = 5; ipos = 6;}
	else if (eta == 2){ineg = 4; ipos = 7;}
	else if (eta == 3){ineg = 3; ipos = 8;}
	else if (eta == 4){ineg = 2; ipos = 9;}
	else if (eta == 5){ineg = 1; ipos = 10;}
	else if (eta == 6){ineg = 0; ipos = 11;}
	ppos = profs.at(ipos);
	pneg = profs.at(ineg);
	value = ppos->GetBinContent(ppos->FindBin(eta));
	value += pneg->GetBinContent(pneg->FindBin(-1*eta));
	value = value/2;
	error1 = ppos->GetBinError(ppos->FindBin(eta));
	error2 = pneg->GetBinError(pneg->FindBin(-1*eta));
	error = TMath::Sqrt(error1*error1 + error2*error2);
      }
      else if (isBarrel == false) {
	profs = getProfileX(configuration, getEffProf2D(configuration, sp));
	p = profs.at(eta-1);
	value = p->GetBinContent(p->FindBin(eta-1));//because eta indices for the endcaps starts from 0 and ends at 2
	error = p->GetBinError(p->FindBin(eta-1));
      }
      std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
      if (value > 0.98) std::cout << "Efficiency above 98% for HV = " << std::stoi(sp) << std::endl;
      // std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
      gEff->SetPoint(i, std::stoi(sp), value);
      //~ gEff->SetPoint(i, getShiftedHV(configuration, sp, eta), value);
      gEff->SetPointError(i, 0, error);
      ++i;
      value = 0; 
      error = 0;
      error1 = 0;
      error2 = 0;
    }
    graphs.push_back(gEff);
  }
  Plotter plotter;
  // std::cout << "Creating plotting object " << &plotter << std::endl;
  plotter.PlotGraphs(configuration, graphs);
}
