#include "include/Plotter.h"

#include "TStyle.h"
#include "TLatex.h"

#include "TLegend.h"
#include "TCanvas.h"
#include "TAxis.h"

Plotter::Plotter(){
	
	//~ m_graphColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	m_graphColor = {kOrange-3, kSpring+9, kTeal+3, kAzure+1, kBlue+3, kPink+8};
	//~ m_graphColor = {kBlack, kBlue, kGreen, kOrange, kRed, kViolet};
	m_marker = {20,21,22,23,33,29};
	
}

void Plotter::PlotGraphs(Config configuration, std::vector<TGraphErrors*> graphs){
	std::cout << "Inside Plotter:PlotGraphs()" << std::endl;
	
	int iRun = configuration.GetRunNumber();
	bool isBarrel = configuration.GetIsBarrel();
	bool isEA = configuration.GetIsEA();
	bool plotEtaCurves = configuration.PlotEtaCurves();
	bool plotPhiCurves = configuration.PlotPhiCurves();
	int scannedLayer = configuration.GetScannedLayer();
	int scannedDisk = configuration.GetScannedDisk();
	bool BCID = configuration.GetBCID();
	int side = configuration.GetSide();
	
	std::string pdfName; 
	
	if (isBarrel){
		if (plotEtaCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_Barrel_")+std::to_string(scannedLayer)+std::string("_side_")+std::to_string(side)+std::string("_Eta");
		if (plotPhiCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_Barrel_")+std::to_string(scannedLayer)+std::string("_side_")+std::to_string(side)+std::string("_Phi");
	}
	else if (isBarrel == false){
		if (isEA == true){
			if (plotEtaCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_EA_")+std::to_string(scannedDisk)+std::string("_side_")+std::to_string(side)+std::string("_Eta");
			if (plotPhiCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_EA_")+std::to_string(scannedDisk)+std::string("_side_")+std::to_string(side)+std::string("_Phi");
		}
		else if (isEA == false){
			if (plotEtaCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_EC_")+std::to_string(scannedDisk)+std::string("_side_")+std::to_string(side)+std::string("_Eta");
			if (plotPhiCurves == true) pdfName = std::string("Eff_")+std::to_string(iRun)+std::string("_EC_")+std::to_string(scannedDisk)+std::string("_side_")+std::to_string(side)+std::string("_Phi");
		}
	}
	
	gStyle->SetOptStat(0);
	
	TCanvas* c = new TCanvas(); 
	c->SetMargin(0.08, 0.02, 0.1, 0.06);
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	TLegend leg(0.6, 0.25, 0.85, 0.6);
	std::string legHeader = GetLegendTitle(configuration);
	leg.SetHeader(legHeader.data());
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	leg.SetFillStyle(0);
	
	std::string legEntryPrefix;
	if (isBarrel == true) legEntryPrefix = "|#eta_{index}| = ";
	else legEntryPrefix = "#eta_{index} = ";
	
	for (int i=0; i<graphs.size(); ++i){
		graphs.at(i)->SetLineColor(m_graphColor.at(i));
		graphs.at(i)->SetMarkerColor(m_graphColor.at(i));
		graphs.at(i)->SetMarkerStyle(m_marker.at(i));
		graphs.at(i)->SetMarkerSize(0.9);
		if (i==0){
			graphs.at(i)->Draw("APEL");
			graphs.at(i)->GetYaxis()->SetRangeUser(0, 1.2);
			graphs.at(i)->GetYaxis()->SetTitle("Hit efficiency");
			graphs.at(i)->GetXaxis()->SetTitle("HV [V]");
			std::string plotTitle = GetPlotTitle(configuration);
			graphs.at(i)->SetTitle(plotTitle.data());
			graphs.at(i)->GetXaxis()->SetTitleOffset(1.1);
			graphs.at(i)->GetXaxis()->SetTitleSize(0.04);
			graphs.at(i)->GetXaxis()->SetLabelSize(0.04);
			graphs.at(i)->GetYaxis()->SetLabelSize(0.04);
			graphs.at(i)->GetYaxis()->SetTitleSize(0.04);
			graphs.at(i)->GetYaxis()->SetTitleOffset(0.9);
		}
		else {
			graphs.at(i)->Draw("PEL SAME");
		}
		std::string graphName = std::string("g_") + std::to_string(i);
		graphs.at(i)->SetName(graphName.data());
		std::string legEntry;
		if (isBarrel == true) legEntry = legEntryPrefix.data() + std::to_string(i+1);
		else legEntry = legEntryPrefix.data() + std::to_string(i);
		leg.AddEntry(graphName.data(), legEntry.data(), "PEL");
	}
	leg.Draw();
	AddATLASLabel(c);
	
	
	std::string pdf = std::string("plots/")+pdfName.data()+std::string(".pdf");
	if (BCID) pdf = std::string("plots/")+pdfName.data()+std::string("_BCID.pdf");
	c->SaveAs(pdf.data());
}

void Plotter::AddATLASLabel(TCanvas* c){

	float x=0.15, y=0.85, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Internal");
	
}

std::string Plotter::GetLegendTitle(Config configuration)
{
	int iRun = configuration.GetRunNumber();
	bool isBarrel = configuration.GetIsBarrel();
	bool isEA = configuration.GetIsEA();
	std::string BD = configuration.GetBD();
	
	if (isBarrel){
	  
	  if (iRun == 361689 || iRun == 367170) return "Barrel 3";
	  else if (iRun == 348251){
	    if (BD == "b3d9") return "Barrel 3";
	    else if (BD == "b6d2") return "Barrel 6";
	  }
	  else if (iRun == 360414) return "Barrel 6";
	  
	  else if (iRun == 427514){
	    if      (BD == "b0d2") return "Barrel 3";
	    else if (BD == "b3d9") return "Barrel 6";
	  }
	}
	else if (isBarrel == false){
	  if (iRun == 361689){ 
	    if (isEA == true) return "Endcap A Disk 6";
	    else return "Endcap C Disk 6";
	  }
	  else if (iRun == 348251){
	    if (BD == "b3d9") return "Disk 9";
	    else if (BD == "b6d2") return "Disk 2";
	  }
	  else if (iRun == 360414) return "Disk 2";
	  else if (iRun == 427514){
	    if      (BD == "b0d2") return "Disk 2";
	    else if (BD == "b3d9") return "Disk 9";
	  }
	  
	}
	return "";
}
		
		
std::string Plotter::GetPlotTitle(Config configuration)
{
	int iRun = configuration.GetRunNumber();
	
	if (iRun == 361689) return "HV scan on 23/09/2018";
	else if (iRun == 367170) return "HV scan on 29/11/2018";
	else if (iRun == 360414) return "HV scan on 10/09/2018";
	else if (iRun == 348251) return "HV scan on 18/04/2018";
	else if (iRun == 427514) return "HV scan on 07/07/2022";
	else return "";
}
