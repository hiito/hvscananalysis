#include "include/Module.h"

Module::Module(int layer, int side, int eta, int phi)
{
	
	m_layer = layer;
	m_side = side;
	m_eta = eta;
	m_phi = phi; 
	
}

int Module::GetLayer()
{
	return m_layer;
}

int Module::GetSide()
{
	return m_side;
}

int Module::GetEta()
{
	return m_eta;
}

int Module::GetPhi()
{
	return m_phi;
}
