#include "include/Config.h"

#include <iostream>
#include <string>

using namespace std;

Config::Config(int run, bool isBarrel, bool isEA, int side, std::string BD, int stream, bool plotEtaCurves, bool plotPhiCurves, bool BCID, int layer)
{
	
  Config::SetConfiguration(run, isBarrel, isEA, side, BD, stream, plotEtaCurves, plotPhiCurves, BCID, layer);

}

void Config::SetConfiguration(int run, bool isBarrel, bool isEA, int side, std::string BD, int stream, bool plotEtaCurves, bool plotPhiCurves, bool BCID, int layer)
{
	
	m_run = run;
	m_isBarrel = isBarrel;
	m_isEA = isEA;
	m_side = side;
	m_BD = BD;
	m_stream = stream;
	m_plotEtaCurves = plotEtaCurves;
	m_plotPhiCurves = plotPhiCurves;
	m_BCID = BCID;
	m_layer = layer;
	
	if (m_run == 348251 && BD == "") {
		std::cout << "You have to provide BD for run 348251, exiting..." << std::endl;
		exit(0);
	}
	
}

void Config::PrintConfiguration()
{
	
	std::cout << "##############################" << std::endl;
	std::cout << "CONFIGURATION:" << std::endl;
	std::cout << "Run:\t\t" << m_run << std::endl;
	std::cout << "Barrel:\t\t" << m_isBarrel << std::endl;
	
	if (m_isBarrel){
		std::cout << "Endcap A:\t" << false << std::endl;
		std::cout << "Endcap C:\t" << false << std::endl;
	}
	else {
		std::cout << "Endcap A:\t" << m_isEA << std::endl;
		std::cout << "Endcap C:\t" << !m_isEA << std::endl;
	}
	
	std::cout << "Side:\t\t" << m_side << std::endl;
	std::cout << "BD:\t\t" << m_BD << std::endl;
	if (m_stream == 0)
		std::cout << "Stream:\t\t"<<"physicsMain" << std::endl;
	else 
		std::cout << "Stream:\t\t"<<"express" << std::endl;
	std::cout << "Plot eta curves:\t\t" << m_plotEtaCurves << std::endl;
	std::cout << "Plot phi curves:\t\t" << m_plotPhiCurves << std::endl;
	std::cout << "Plot results only for 1st BCID\t" << m_BCID << std::endl;
	std::cout << "##############################" << std::endl;
	
}

int Config::GetRunNumber()
{
	return m_run;
}

int Config::GetSide()
{
	return m_side;
}

std::string Config::GetBD()
{
	return m_BD;
}

bool Config::GetIsBarrel()
{
	return m_isBarrel;
}

bool Config::GetIsEA()
{
	return m_isEA;
}

int Config::GetStream()
{
	return m_stream;
}

bool Config::PlotEtaCurves()
{
	return m_plotEtaCurves;
}

bool Config::PlotPhiCurves()
{
	return m_plotPhiCurves;
}

bool Config::GetBCID()
{
	return m_BCID;
}

int  Config::GetLayer()
{
	return m_layer;
}

int Config::GetScannedLayer()
{
	//~ if (m_isBarrel){
	if (m_run == 361689 || m_run == 367170 || m_run == 324502 || m_run == 309375 || m_run == 340072 || m_run == 284484) return 0;
	else if (m_run == 360414) return 3;
	else if (m_run == 348251 && m_BD == "b3d9") return 0;
	else if (m_run == 348251 && m_BD == "b6d2") return 3;
	else if (m_run == 427514 && m_BD == "b0d2") return 0;
	else if (m_run == 427514 && m_BD == "b3d9") return 3;
	else if (m_run == 437692 && m_BD == "b0LB") return 0;
	else if (m_BD == "b0")                      return 0;
	else if (m_BD == "b3")                      return 3;
	else return -1;
	//~ }
}

int Config::GetScannedDisk()
{
	if (m_run == 361689) return 5;
	else if (m_run == 360414) return 1;
	else if (m_run == 348251 && m_BD == "b3d9") return 8;
	else if (m_run == 348251 && m_BD == "b6d2") return 1;
	else if (m_run == 427514 && m_BD == "b0d2") return 1;
	else if (m_run == 427514 && m_BD == "b3d9") return 8;
	else if (m_BD == "d2")   return 1;
	else if (m_BD == "d6")   return 5;
	else if (m_BD == "d5")   return 4;
	else return -1;
}

std::vector<int> Config::GetPhiLow()
{
	if (m_run == 361689){
		if (m_isEA) return { 1, 0, 1};
		else return {0,0,0};
	}
	else if (m_run == 360414){
		if (m_isEA) return {14,10,11};
		else return {0,0,0};
	}
	else if (m_run == 348251){
	  if (m_isEA) return {27,20,21};
	  else return {26,20,20};
	}
	// else if (m_run == 427514 || m_run == 437692 || m_run == 440199 || m_run == 450271 || m_run == 456316 || m_run==461655 || m_run == 461899){
	//   if (m_isEA) return {27,20,21};
	//   else return {26,20,20};
	//   // if (m_isEA) return {20,21};
	//   // else return {20,20};
	// }
	else if (m_BD == "d2" && (m_run == 427514 || m_run == 437692 || m_run == 440199 || m_run == 450271 || m_run == 456316 || m_run==461655 || m_run == 461899)){
	  if (m_isEA) return {28,21,22};
	  else return {27,21,21};
	}
	else if (m_BD == "d6" && (m_run == 427514 || m_run == 437692 || m_run == 440199 || m_run == 450271 || m_run == 456316 || m_run==461655 || m_run == 461899)){
	  if (m_isEA) return {28,21,22};
	  else return {27,21,21};
	}
	else if(472553 <= m_run){
	  return {1, 1, 1};
	}
	else return {-1, -1, -1};
}

std::vector<int> Config::GetPhiUp()
{
	if (m_run == 361689){
	  if (m_isEA) return {13, 9,10};
		else return {8, 9,9};
	}
	else if (m_run == 360414){
	  if (m_isEA) return {26,19,20};
	  else return {12,9,9};
	}
	else if (m_run == 348251){
	  if (m_isEA) return {39,29,30};
	  else return {38,29,29};
	}
	else if (m_run == 427514 || m_run == 437692 || m_run == 440199 || m_run == 450271 || m_run == 456316 || m_run==461655 || m_run == 461899){
	  if (m_isEA) return {39,30,30};
	  else return {39,30,30};
	  // if (m_isEA) return {29,30};
	  // else return {29,29};
	}
	else if(472553 <= m_run){
	  return {52, 40, 40};
	}
	else return {-1, -1, -1};
}

std::vector<int> Config::GetAbsEtaIndices()
{
  if (m_isBarrel) return {1, 2, 3, 4, 5, 6};
  else return {1, 2, 3};
	// else return {2, 3};
}

std::vector<std::string> Config::GetScanPoints()
{
        std::vector<string> scanPoints;
	if( m_run == 486640){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300", "325", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300"};
	  if(m_BD == "b3")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "250", "250", "275", "300"};
	  if(m_BD == "d5")   scanPoints = {"010", "030", "060", "100", "125", "150", "175", "200", "250"};
	}
	
	if( m_run == 479374){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "300", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	  if(m_BD == "b3")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = {"010", "020", "040", "060", "075", "100", "125", "150", "175", "200", "250"};
	  if(m_BD == "d5")   scanPoints = {"010", "030", "060", "100", "125", "150", "175", "200", "250"};
	}
	
	if( m_run == 472553){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300", "325", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	  if(m_BD == "b3")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  // if(m_BD == "b3")   scanPoints = {"010", "020", "030", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	  // if(m_BD == "d6")   scanPoints = {"010", "020", "030", "075", "100", "125", "150", "175", "200", "225", "250"};
	}
  
	if( m_run == 461899){
	  if(m_BD == "b3")   scanPoints = { "010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = { "010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	}
	
	if( m_run == 461655){
	  if(m_BD == "b0" && m_stream==2)  scanPoints = {"010", "040", "060", "075", "100", "125", "150", "175", "200", "275", "350"};
	  else if (m_BD == "b0" && m_stream!=2)  scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300", "325", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	}
	
	if( m_run == 456316){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300", "325", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	  if(m_BD == "b3")   scanPoints = {"020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = {"020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	}
	
	if( m_run == 450271){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250", "275", "300", "325", "350"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	  if(m_BD == "b3")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "d6")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "225", "250"};
	}
	
	if( m_run == 437692 || m_run == 440199){
	  if(m_BD == "b0")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200", "250"};
	  if(m_BD == "d2")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "b3")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150"};
	  if(m_BD == "d6")   scanPoints = {"010", "020", "030", "040", "060", "075", "100", "125", "150", "175", "200"};
	  if(m_BD == "b0LB") scanPoints = {"189", "190", "191", "192", "193", "194", "195", "196", "197", "198", "199", "200", "201", "202", "203", "204", "205", "206", "207", "208", "209", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220", "221", "222", "223", "224", "225", "226", "227", "228", "229", "230", "231", "232", "233", "234", "235", "236", "237", "238", "239", "240", "241", "242", "243", "244", "245", "246", "247", "248", "249", "250", "251", "252", "253", "254", "255", "256", "257", "258", "259", "260", "261", "262", "263", "264", "265", "266", "267", "268", "269", "272", "273", "274", "276", "277", "278", "279", "280", "281", "282", "283", "284", "285", "286", "287", "288", "289", "295", "296", "297", "298", "299", "300", "301", "302", "303", "304", "305", "306", "307", "308", "309", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320", "321", "322", "323", "324", "325", "326", "327", "328", "329", "330", "331", "332", "333", "334", "336", "337", "338", "339", "340", "341", "342", "343", "344", "345", "346", "347", "349", "350", "351", "352", "353", "354", "355", "356", "357", "358", "359", "361", "362", "363", "365", "366", "367", "368", "369", "370", "371", "372", "374", "375", "376", "377", "378", "379", "380", "381", "382", "383", "384", "385", "386", "387", "388", "389", "395", "396", "397", "398", "399", "400", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "411", "412", "413", "414", "415", "416", "417", "418", "419", "420", "421", "422", "423", "424", "425", "426", "427", "428", "429", "430", "431", "432", "433", "434", "435", "436", "437", "438", "439", "440", "441", "442", "443", "444", "445", "446", "447", "448", "449", "450", "451", "452", "453", "454", "455", "456", "457", "458", "459", "460", "461", "462", "463", "464", "465", "466", "467", "468", "469", "470", "471", "472"};
	}
	
	if(m_run == 427514){
	  if(m_BD == "b0d2") scanPoints = {"005","010","020","040","050","075","100","125","150", "175", "200"};
	  if(m_BD == "b3d9") scanPoints = {"005","010","020","040","050","075","100","125","150"};
	}
	
	if (m_run == 361689)
		scanPoints = {"040","060","080","100","120","140","160","180","200","250"};
	else if (m_run == 360414)
		scanPoints = {"030","040","060","080","100","120","130","140"};
	else if (m_run == 348251){
	  if(m_BD == "b0") scanPoints = {"010", "020", "030", "040", "075", "100", "125", "150"};
	  if(m_BD == "b3") scanPoints = {"010", "020", "030", "040", "075", "100", "125", "150"};
	}
	else if (m_run == 363664) 
		scanPoints = {"010", "020", "030", "040",/* "050",*/ "060", "080", "100", "120", "140", "160", "180", "200", "220", "240", "250"};
	else if (m_run == 367170)
		scanPoints = {"100", "110", "120", "130", "140", "150", "170", "200", "250"};
	else if (m_run == 367134) 
		scanPoints = {"005", "010", "020", "030", "040", "070", "090", "110", "130", "150", "160", "180", "200", "250"};
	else if (m_run == 324502)
		scanPoints = {"010", "020", "030", "040", "050", "075", "100", "125", "150"};
	else if (m_run == 309375)
		scanPoints = {"010", "020", "030", "040", "050", "075", "100", "125", "150"};
	else if (m_run == 340072)
		scanPoints = {"010", "020", "030", "040", "050", "075", "100", "125", "150"};
	else if (m_run == 284484)
		scanPoints = {"010", "020", "030", "040", "050", "075", "100", "150"};
	return scanPoints;
}
