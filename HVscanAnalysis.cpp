/***********************************************************************
 * compilation command:
 * g++ -o HVscan HVscanAnalysis.cpp `root-config --cflags --glibs`
 **********************************************************************/

#include "TProfile.h"
#include "TProfile2D.h"
#include "TDirectory.h"
#include "TStyle.h"
#include "TH1.h"
#include "TFile.h"
#include "TPad.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TMath.h"
#include "TGraphErrors.h"
#include "TLatex.h"

#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

TFile* OpenFile(int run, string HVpoint, string BD){
	std::cout << "In function OpenFile with three arguments (for run 348251): run = " << run << " HVpoint " << HVpoint << " BD " << BD << std::endl;
	string runNo = std::to_string(run);
	string filename = std::string("run")+runNo+std::string("_")+BD+std::string("_")+HVpoint+std::string("V.root");
	string fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+runNo+std::string("/")+filename;
	//~ string filename = std::string("HVscan_")+runNo+std::string("_effHisto_")+BD+std::string("_")+HVpoint+std::string(".root");
	//~ string fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+runNo+std::string("/")+filename;
	std::cout << "Full path is = " << fullpath << std::endl;
	TFile* f = TFile::Open(fullpath.data());
	return f;
}


TFile* OpenFile(int run, string HVpoint){
	std::cout << "Inside OpenFile function: run " << run << " HVpoint " << HVpoint << std::endl;
	string runNo = std::to_string(run);
	//~ std::cout << "Run number as string " << runNo << std::endl;
	string filename;
	string fullpath;
	if (run == 361689 || run == 360414)
		filename = std::string("physics_HV")+HVpoint.data()+std::string(".root");
	else if (run == 363664 || run == 367134)
		filename = std::string("HV")+HVpoint.data()+std::string(".root");
	else if (run == 367170)
		filename = std::string("HV")+HVpoint.data()+std::string("V.root");
	fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+runNo+std::string("/")+filename;
	std::cout << "Full path is = " << fullpath << std::endl;
	TFile* f = TFile::Open(fullpath.data());
	return f;
}

TFile* OpenFileExpress(int run, string HVpoint){
	std::cout << "Inside OpenFileExpress function: run " << run << " HVpoint " << HVpoint << std::endl;
	string runNo = std::to_string(run);
	string filename;
	if (run == 361689)
		filename = std::string("express_361689_HV")+HVpoint.data()+std::string(".root");
	else if (run == 360414)
		filename = std::string("express_HV")+HVpoint.data()+std::string(".root");
	string fullpath = std::string("/eos/user/m/mociduki/sct_2018_scans/")+runNo+std::string("/express/")+filename;
	std::cout << "Full path is = " << fullpath << std::endl;
	TFile* f = TFile::Open(fullpath.data());
	return f;
}

std::vector<string> getScanPoints(int run){
	std::cout << "Inside getScanPoints function: run = " << run << std::endl;
	std::vector<string> scanPoints;
	if (run == 361689)
		scanPoints = {"040","060","080","100","120","140","160","180","200","250"};
	else if (run == 360414)
		scanPoints = {"030","040","060","080","100","120","130","140","150"};
	else if (run == 348251)
		scanPoints = {"010", "020", "030","040",/*"050",*/"075","100","125","150"};
	else if (run == 363664) 
		scanPoints = {"010", "020", "030", "040",/* "050",*/ "060", "080", "100", "120", "140", "160", "180", "200", "220", "240", "250"};
	else if (run == 367170)
		scanPoints = {"100", "110", "120", "130", "140", "150", "170", "200", "250"};
	else if (run == 367134) 
		scanPoints = {"005", "010", "020", "030", "040", "070", "090", "110", "130", "150", "160", "180", "200", "250"};
	return scanPoints;
}

TProfile2D* getEffBarrel(int run, string sp, int side, string BD){
	std::cout << "Inside getEffBarrel function: run = " << run << " sp " << sp << " side " << side << " BD " << BD << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	std::cout << "runPrefix = " << runPrefix << std::endl;
	//~ string profPath;
	TProfile2D* prof = nullptr;
	string layer;
	bool bcid = true;
	if (run == 361689 || run == 367170) layer = "0";
	else if (run == 360414) layer = "3";
	else if (run == 348251 && BD == "b3d9") layer = "0";
	else if (run == 348251 && BD == "b6d2") layer = "3";
	
	TFile* f = nullptr;
	if (run == 361689 || run == 360414 || run == 367170) f = OpenFile(run, sp);
	if (run == 348251) f = OpenFile(run, sp, BD);
	//~ std::cout << "f = " << f << std::endl;
	//~ profPath = "run_361689/SCT/SCTB/eff/eff_0_0";
	if (side == 0){
		string profName = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_0");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 1) {
		string profName = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_1");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 2) {
		//~ TProfile2D* prof0=nullptr;
		//~ TProfile2D* prof1=nullptr;
		string profName0, profName1;
		if (bcid && run == 361689){
			profName0 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_bcid_")+layer.data()+std::string("_0");
			profName1 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_bcid_")+layer.data()+std::string("_1");
		}
		else{
			profName0 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_0");
			profName1 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_1");
		}
		//~ string profName0 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_0");
		TProfile2D* prof0 = dynamic_cast<TProfile2D*>(f->Get(profName0.data()));
		//~ string profName1 = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layer.data()+std::string("_1");
		TProfile2D* prof1 = dynamic_cast<TProfile2D*>(f->Get(profName1.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	return prof;
} 

TProfile2D* getEffBarrelHHVscan(int run, string sp, int layer, int side){
	std::cout << "Inside getEffBarrelHHVscan function: run " << run << " sp " << sp << " layer " << layer << " side " << side << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	TProfile2D* prof = nullptr;
	TProfile2D* prof0 = nullptr;
	TProfile2D* prof1 = nullptr;
	string layerS = std::to_string(layer);
	string sideS = std::to_string(side);
	TFile* f = OpenFile(run, sp);
	//~ string profName = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layerS.data()+std::string("_")+sideS.data();
	string prof0Name = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layerS.data()+std::string("_0");
	string prof1Name = runPrefix.data()+std::string("/SCT/SCTB/eff/eff_")+layerS.data()+std::string("_1");
	//~ std::cout << "Prof path = " << profName.data() << std::endl;
	prof0 =  dynamic_cast<TProfile2D*>(f->Get(prof0Name.data()));
	prof1 =  dynamic_cast<TProfile2D*>(f->Get(prof1Name.data()));
	prof = (TProfile2D*) prof0->Clone("prof");
	prof->Add(prof1);
	return prof;
}

TProfile2D* getEffEndcapHHVscan(int run, bool isEA, string sp, int layer, int side){
	std::cout << "Inside getEffEndcapHHVscan function: run " << run << " isEA " << isEA << " sp " << sp << " layer " << layer << " side " << side << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	TProfile2D* prof = nullptr;
	TProfile2D* prof0 = nullptr;
	TProfile2D* prof1 = nullptr;
	string layerS = std::to_string(layer);
	string sideS = std::to_string(side);
	TFile* f = OpenFile(run, sp);
	string profName; 
	string prof0Name; 
	string prof1Name; 
	if (isEA){
		//~ profName = runPrefix.data()+std::string("/SCT/SCTEA/eff/p_eff_")+layerS.data()+std::string("_")+sideS.data();
		prof0Name = runPrefix.data()+std::string("/SCT/SCTEA/eff/p_eff_")+layerS.data()+std::string("_0");
		prof1Name = runPrefix.data()+std::string("/SCT/SCTEA/eff/p_eff_")+layerS.data()+std::string("_1");
		prof0 =  dynamic_cast<TProfile2D*>(f->Get(prof0Name.data()));
		prof1 =  dynamic_cast<TProfile2D*>(f->Get(prof1Name.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	else {
		//~ profName = runPrefix.data()+std::string("/SCT/SCTEC/eff/m_eff_")+layerS.data()+std::string("_")+sideS.data();
		prof0Name = runPrefix.data()+std::string("/SCT/SCTEC/eff/m_eff_")+layerS.data()+std::string("_0");
		prof1Name = runPrefix.data()+std::string("/SCT/SCTEC/eff/m_eff_")+layerS.data()+std::string("_1");
		prof0 =  dynamic_cast<TProfile2D*>(f->Get(prof0Name.data()));
		prof1 =  dynamic_cast<TProfile2D*>(f->Get(prof1Name.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	//~ std::cout << "Prof path = " << profName.data() << std::endl;
	//~ prof =  dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	return prof;
}

TProfile2D* getNoiseBarrelHHVscan(int run, string sp, int layer, int side){
	std::cout << "Inside getNoiseBarrelHHVscan function: run " << run << " sp " << sp << " layer " << layer << " side " << side << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	TProfile2D* prof = nullptr;
	string layerS = std::to_string(layer);
	string sideS = std::to_string(side);
	TFile* f = OpenFile(run, sp);
	string profName = runPrefix.data()+std::string("/SCT/SCTB/Noise/noiseoccupancymaptrigger_")+layerS.data()+std::string("_")+sideS.data();
	std::cout << "Prof path = " << profName.data() << std::endl;
	prof =  dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	return prof;
}

TProfile2D* getEffEndcap(int run, string sp, int side, bool isEA, string BD){
	std::cout << "Inside getEffEndcap function: run = " << run << " sp " << sp << " side " << side << " isEA " << isEA << " BD " << BD << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	std::cout << "runPrefix = " << runPrefix << std::endl;
	//~ string profPath;
	TProfile2D* prof = nullptr;
	string disk;
	if (run == 361689) disk = "5";
	else if (run == 360414) disk = "1";
	else if (run == 348251){
		if (BD == "b3d9") disk = "8";
		else if (BD == "b6d2") disk = "1";
	}
	string prefix; 
	if (isEA) prefix = std::string("run_")+std::to_string(run)+std::string("/SCT/SCTEA/eff/p_eff_")+disk.data();
	else prefix = std::string("run_")+std::to_string(run)+std::string("/SCT/SCTEC/eff/m_eff_")+disk.data();
	
	TFile* f = nullptr;
	if (run == 348251) f = OpenFile(run, sp, BD);
	else f = OpenFile(run, sp);
	//~ std::cout << "f = " << f << std::endl;
	//~ profPath = "run_361689/SCT/SCTB/eff/eff_0_0";
	if (side == 0){
		string profName = prefix.data()+std::string("_0");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 1) {
		string profName = prefix.data()+std::string("_1");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 2) {
		//~ TProfile2D* prof0=nullptr;
		//~ TProfile2D* prof1=nullptr;
		string profName0 = prefix.data()+std::string("_0");
		TProfile2D* prof0 = dynamic_cast<TProfile2D*>(f->Get(profName0.data()));
		string profName1 = prefix.data()+std::string("_1");
		TProfile2D* prof1 = dynamic_cast<TProfile2D*>(f->Get(profName1.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	return prof;
}

TProfile2D* getNoiseBarrel(int run, string sp, int side){
	std::cout << "Inside getNoiseBarrel function: run = " << run << " sp " << sp << " side " << side << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	std::cout << "runPrefix = " << runPrefix << std::endl;
	//~ string profPath;
	TProfile2D* prof = nullptr;
	string layer;
	if (run == 361689) layer = "0";
	else if (run == 360414) layer = "3";
	else if (run == 367170) layer = "0";
	
	TFile* f = OpenFileExpress(run, sp);
	if (side == 0){
		string profName = runPrefix.data()+std::string("/SCT/SCTB/Noise/noiseoccupancymaptrigger_")+layer.data()+std::string("_0");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 1) {
		string profName = runPrefix.data()+std::string("/SCT/SCTB/Noise/noiseoccupancymaptrigger_")+layer.data()+std::string("_1");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 2) {
		//~ TProfile2D* prof0=nullptr;
		//~ TProfile2D* prof1=nullptr;
		string profName0 = runPrefix.data()+std::string("/SCT/SCTB/Noise/noiseoccupancymaptrigger_")+layer.data()+std::string("_0");
		TProfile2D* prof0 = dynamic_cast<TProfile2D*>(f->Get(profName0.data()));
		string profName1 = runPrefix.data()+std::string("/SCT/SCTB/Noise/noiseoccupancymaptrigger_")+layer.data()+std::string("_1");
		TProfile2D* prof1 = dynamic_cast<TProfile2D*>(f->Get(profName1.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	return prof;
}

TProfile2D* getNoiseEndcap(int run, string sp, int side, bool isEA){
	std::cout << "Inside getNoiseEndcap function: run = " << run << " sp " << sp << " side " << side << " isEA " << isEA << std::endl;
	string runPrefix = std::string("run_")+std::to_string(run);
	std::cout << "runPrefix = " << runPrefix << std::endl;
	//~ string profPath;
	TProfile2D* prof = nullptr;
	string disk;
	if (run == 361689) disk = "5";
	else if (run == 360414) disk = "1";
	string prefix; 
	if (isEA) prefix = std::string("run_")+std::to_string(run)+std::string("/SCT/SCTEA/Noise/noiseoccupancymaptriggerECp_")+disk.data();
	else prefix = std::string("run_")+std::to_string(run)+std::string("/SCT/SCTEC/Noise/noiseoccupancymaptriggerECm_")+disk.data();
	
	TFile* f = OpenFileExpress(run, sp);
	if (side == 0){
		string profName = prefix.data()+std::string("_0");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 1) {
		string profName = prefix.data()+std::string("_1");
		prof = dynamic_cast<TProfile2D*>(f->Get(profName.data()));
	}
	else if (side == 2) {
		//~ TProfile2D* prof0=nullptr;
		//~ TProfile2D* prof1=nullptr;
		string profName0 = prefix.data()+std::string("_0");
		TProfile2D* prof0 = dynamic_cast<TProfile2D*>(f->Get(profName0.data()));
		string profName1 = prefix.data()+std::string("_1");
		TProfile2D* prof1 = dynamic_cast<TProfile2D*>(f->Get(profName1.data()));
		prof = (TProfile2D*) prof0->Clone("prof");
		prof->Add(prof1);
	}
	return prof;
}

std::vector<int> getAbsEtaIndices(bool isBarrel){
	std::cout << "Inside getAbsEtaIndices function: isBarrel = " << isBarrel << std::endl;
	std::vector<int> etaIndices;
	if (isBarrel) 
		etaIndices = {1, 2, 3, 4, 5, 6};
	else 
		etaIndices = {1, 2, 3};
	return etaIndices;
}

std::vector<int> getPhiRange(int run){
	std::cout << "Inside getPhiRange function: run = " << run << std::endl;
	std::vector<int> phiRange;
	if (run == 361689)
		phiRange = {0, 11, 21, 31};
		
	return phiRange;
}

std::vector<std::pair<int, int>> getPhiRanges(){
	std::cout << "Inside getPhiRanges function" << std::endl;
	std::vector<std::pair<int, int>> phiRanges;
	
	phiRanges = {{4, 11}, {12, 19}, {20, 27}, {28, 31}};
	
	return phiRanges;
}


std::vector<int> getPhiLowRange(int run, bool isEA){
	std::cout << "Inside getPhiLowRange function: run = " << run << " isEA " << isEA << std::endl;
	if (run == 361689){
		if (isEA) return { 1, 0, 1};
		else return {0,0,0};
	}
	else if (run == 360414){
		if (isEA) return {14,10,11};
		else return {0,0,0};
	}
	else if (run == 348251){
		if (isEA) return {27,20,21};
		else return {26,20,20};
	}
}

std::vector<int> getPhiUpRange(int run, bool isEA){
	std::cout << "Inside getPhiUpRange function: run = " << run << " isEA " << isEA << std::endl;
	if (run == 361689){
		if (isEA) return {13, 9,10};
		else return {8, 9,9};
	}
	else if (run == 360414){
		if (isEA) return {26,19,20};
		else return {12,9,9};
	}
	else if (run == 348251){
		if (isEA) return {39,29,30};
		else return {38,29,29};
	}
}

std::vector<TProfile*> getProfileX(TProfile2D* prof, int run, bool isBarrel, bool isEA){
	std::cout << "Inside getProfileX function: prof = " << prof << " run " << run << " isBarrel " << isBarrel << " isEA " << isEA << std::endl;
	std::vector<TProfile*> profs;
	TProfile* p = nullptr;
	if (isBarrel){
		p = prof->ProfileX();
		profs.push_back(p);
	}
	else {
		std::vector<int> etaIndices = getAbsEtaIndices(false);
		if (isEA){
			std::vector<int> phiLow = getPhiLowRange(run, true);
			std::vector<int> phiUp = getPhiUpRange(run, true);
			//~ p = prof->ProfileX();
			for (unsigned int i=0; i<etaIndices.size(); ++i){
				string name = std::string("px_m")+std::to_string(i);
				p = prof->ProfileX(name.data(), phiLow.at(i), phiUp.at(i));
				profs.push_back(p);
				std::cout << "Phi range = (" << phiLow.at(i) << ", "  << phiUp.at(i) << "), adding prof " << name.data() << " for eta " << etaIndices.at(i) << std::endl;
			}
		}
		else {
			std::vector<int> phiLow = getPhiLowRange(run, false);
			std::vector<int> phiUp = getPhiUpRange(run, false);
			for (unsigned int i=0; i<etaIndices.size(); ++i){
				std::cout << "Phi range = (" << phiLow.at(i) << ", "  << phiUp.at(i) << ")" << std::endl;
				string name = std::string("px_m")+std::to_string(i);
				p = prof->ProfileX(name.data(), phiLow.at(i), phiUp.at(i));
				profs.push_back(p);
			}	
		}
	}
	return profs;
}

std::vector<TProfile*> getProfileY(TProfile2D* prof, int run/*, std::vector<std::pair<int, int>> phiRanges*/){
	// doesn't make sense for the endcaps because the rings have different eta ranges
	//~ std::cout << "Inside getProfileY function: prof = " << prof << " run = " << run << " phiRanges.size() " << phiRanges.size() <<  std::endl;
	std::vector<TProfile*> profs;
	TProfile* p = nullptr;
	//~ int i = 0;
	//~ for (auto phiRange : phiRanges){
		//~ std::cout << "current phiRange = (" << phiRange.first << ", " << phiRange.second << ")" << std::endl; 
		p = prof->ProfileY();
		profs.push_back(p);
		
		//~ ++i; 
	//~ }
	
	return profs;
}

TProfile* getProfileXL96(TProfile2D* prof, int run, bool onlyL96){
	std::cout << "Inside getProfileXL96 function: prof = " << prof << " run " << run << " onlyL96 " << onlyL96 << std::endl;
	TProfile* p = nullptr;
	TProfile* p1 = nullptr;
	TProfile* p2 = nullptr;
	std::vector<int> etaIndices = getAbsEtaIndices(true);
	if (onlyL96){
		int min = 9, max=12;
		std::cout << "Phi range with loop 96 = (" << min << ", " << max << ")" << std::endl; 
		string name = "px_m";
		p = prof->ProfileX(name.data(), min, max);
	}
	else{
		std::cout << "Without loop 96" << std::endl;
		p = prof->ProfileX("px_m1", 0, 8);
		p2 = prof->ProfileX("px_m2", 13);
		p->Add(p2);
	} 
	return p;
}

// function returning profile but excluding some modules (for example these with crystal orientation <100>)
std::vector<TProfile*> getProfileExclMod(TProfile2D* prof, int run, bool positiveEta, std::vector<std::pair<int, int>> modules){ 
	std::cout << "Inside getProfileExclMod function with prof " << prof << " run " << run << std::endl;  
	for (auto mod : modules){
		std::cout << "{" << mod.first << ", " << mod.second << "}\t"; 
	}
	std::cout << "\n";
	std::vector<TProfile*> profiles;
	TProfile *p, *p1, *p2;
	std::vector<int> etaIndices;
	if (positiveEta) etaIndices = getAbsEtaIndices(true);
	else etaIndices = {-1, -2, -3, -4, -5, -6};
	std::vector<std::vector<int>> exclPhi(6);
	if (modules.size()==0) {
		profiles.push_back(prof->ProfileX());
		return profiles;
	}  
	for (int i=0; i<etaIndices.size(); ++i){
		for (int j=0; j<modules.size(); ++j){
			if (etaIndices.at(i) == modules.at(j).first) {
				std::cout << "found excluded module with eta " << modules.at(j).first << std::endl;
				exclPhi.at(i).push_back(modules.at(j).second);
			}
		}
	}
	
	//~ for (auto phi : exclPhi.at(3)){
		//~ std::cout << "Excluded phi = " << phi << std::endl;
	//~ }
	int i = 4;
	for (int i=0; i<etaIndices.size(); ++i){
		if (exclPhi.at(i).size() == 0){
			std::cout << "Found 0 excluded modules" << std::endl;
			p = prof->ProfileX();
			p->SetDirectory(0);
		}
		if (exclPhi.at(i).size() == 1){
			std::cout << "Found 1 excluded module for this eta, creating 2 TProfiles" << std::endl;
			p = prof->ProfileX("m_0", 0, exclPhi.at(i).at(0)-1);
			p->SetDirectory(0);
			p1 = prof->ProfileX("m_1", exclPhi.at(i).at(0)+1, 31);
			p->SetDirectory(0);
			p->Add(p1);
		}
		if (exclPhi.at(i).size() == 2){
			std::cout << "Found 2 excluded module for this eta, creating 3 TProfiles" << std::endl;
			p = prof->ProfileX("m_2", 0, exclPhi.at(i).at(0)-1);
			p->SetDirectory(0);
			p1 = prof->ProfileX("m_3", exclPhi.at(i).at(0)+1, exclPhi.at(i).at(1)-1);
			p1->SetDirectory(0);
			p2 = prof->ProfileX("m_4", exclPhi.at(i).at(1)+1, 31);
			p2->SetDirectory(0);
			p->Add(p1);
			p->Add(p2);
		}
		profiles.push_back(p);
		std::cout << "Vector of profiles size = " << profiles.size() << std::endl;
		p->Clear();
		p1->Clear();
		p2->Clear();
	}
	
	//~ TCanvas c;
	//~ TProfile* p1 = prof->ProfileX("m_1", 0, 13);
	//~ TProfile* p2 = prof->ProfileX("m_2", 15, 31);
	//~ p1->Add(p2);
	
	//~ int bin = prof->FindBin(modules.at(0).first, modules.at(0).second);
	//~ std::cout << "Found bin (for eta " << modules.at(0).first << " phi " << modules.at(0).second << ") = " << bin << std::endl; 
	//~ float binContent = prof->GetBinContent(bin);
	//~ std::cout << "Value in this bin = " << binContent << std::endl;
	//~ prof->SetBinContent(bin, 0);
	//~ prof->SetBinError(bin, 0);
	//~ std::cout << "Reseted bin content for this bin = " << prof->GetBinContent(bin) << " and error = " << prof->GetBinError(bin) << std::endl; 
	//~ TProfile* p = prof->ProfileX();
	//~ p1->Divide(p);
	//~ p1->Draw();
	//~ c.SaveAs("profilesComparison.pdf");
	return profiles;
}

void makeGraphs(std::vector<TGraphErrors*> graphs, int run, bool isBarrel, bool isEA, string BD){
	std::cout << "Inside makeGraphs function: graphs.size() " << graphs.size() << " run " << run << " isBarrel " << isBarrel << " BD " << BD << std::endl;
	gStyle->SetOptStat(0);
	
	std::vector<int> graphColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	
	std::vector<string> graphName = {"g0", "g1", "g2", "g3", "g4", "g5"};
	std::vector<string> legEntry = {"|#eta_{index}| = 1", "|#eta_{index}| = 2", "|#eta_{index}| = 3", "|#eta_{index}| = 4", "|#eta_{index}| = 5", "|#eta_{index}| = 6"};
	
	if (graphs.size() < 6) legEntry = {"#eta_{index} = 1", "#eta_{index} = 2", "#eta_{index} = 3"};
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		graphs.at(i)->SetMarkerColor(graphColor.at(i));
		graphs.at(i)->SetLineColor(graphColor.at(i));
		graphs.at(i)->SetMarkerStyle(marker.at(i));
		graphs.at(i)->SetMarkerSize(0.9);
	}
	
	string title; 
	if (run == 361689){
		if (isBarrel) title = "HV scan on 23/09/2018";
		else title = "HV scan on 23/09/2018";
	}
	else if (run == 360414){
		if (isBarrel) title = "HV scan on 10/09/2018, Barrel 6";
		else title = "HV scan on 10/09/2018, Disk 2";
	}
	else if (run == 348251){
		if (BD == "b3d9"){
			if (isBarrel) title = "HV scan on 18/04/2018, Barrel 3";
			else title = "HV scan on 18/04/2018, Disk 9";
		}
		else if (BD == "b6d2"){
			if (isBarrel) title = "HV scan on 18/04/2018, Barrel 6";
			else title = "HV scan on 18/04/2018, Disk 2";
		}
	}
	else if (run == 367170){
		if (isBarrel) title = "HV scan on 29/11/2018, Barrel 3";
	}
	graphs.at(0)->SetTitle(title.data());
	graphs.at(0)->GetXaxis()->SetTitle("HV [V]");
	graphs.at(0)->GetXaxis()->SetTitleOffset(1.1);
	graphs.at(0)->GetXaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetXaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleOffset(0.9);
	graphs.at(0)->GetYaxis()->SetTitle("Hit efficiency");
	
	TCanvas c; 
	c.SetMargin(0.08, 0.02, 0.1, 0.06);
	TLegend leg(0.6, 0.25, 0.85, 0.55);
	if (run == 361689 && isBarrel)
		leg.SetHeader("Barrel 3");
	else if (run == 361689 && (!isBarrel) && isEA)
		leg.SetHeader("Endcap A, Disk 6");
	else if (run == 361689 && (!isBarrel) && (!isEA))
		leg.SetHeader("Endcap C, Disk 6");
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	leg.SetFillStyle(0);
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		if (i == 0) {
			graphs.at(0)->Draw("apel");
			graphs.at(0)->GetYaxis()->SetRangeUser(0, 1.2);
			graphs.at(0)->SetName(graphName.at(0).data());
			leg.AddEntry(graphName.at(0).data(), legEntry.at(0).data(), "pel");
		}
		else {
			graphs.at(i)->Draw("pel same");
			graphs.at(i)->SetName(graphName.at(i).data());
			leg.AddEntry(graphName.at(i).data(), legEntry.at(i).data(), "pel");
		}
	}
	leg.Draw();
	float x=0.15, y=0.85, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Preliminary");
	c.SaveAs("efficiencyGraph.pdf");
}

void makeGraphsPhi(std::vector<TGraphErrors*> graphs, int run, string BD){
	std::cout << "Inside makeGraphsPhi function: graphs.size() = " << graphs.size() << " run = " << run << " BD " << BD << std::endl;
	
	gStyle->SetOptStat(0);
	
	std::vector<int> graphColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	
	std::vector<string> graphName = {"g0", "g11", "g21", "g31"};
	std::vector<string> legEntry = {"|#phi_{index}| #in (4, 11)", "|#phi_{index}| #in (12, 19)", "|#phi_{index}| #in (20, 27)", "|#phi_{index}| #in (28, 31)"};
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		graphs.at(i)->SetMarkerColor(graphColor.at(i));
		graphs.at(i)->SetLineColor(graphColor.at(i));
		graphs.at(i)->SetMarkerStyle(marker.at(i));
		graphs.at(i)->SetMarkerSize(0.9);
	}
	
	string title; 
	if (run == 361689){
		title = "HV scan on 23/09/2018";
	}
	else if (run == 367170){
		title = "HV scan on 29/11/2018";
	}
	
	graphs.at(0)->SetTitle(title.data());
	graphs.at(0)->GetXaxis()->SetTitle("HV [V]");
	graphs.at(0)->GetXaxis()->SetTitleOffset(1.1);
	graphs.at(0)->GetXaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetXaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleOffset(0.9);
	graphs.at(0)->GetYaxis()->SetTitle("Hit efficiency");
	
	TCanvas c; 
	c.SetMargin(0.08, 0.02, 0.1, 0.06);
	TLegend leg(0.6, 0.25, 0.85, 0.55);
	leg.SetHeader("Barrel 3");
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	leg.SetFillStyle(0);
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		if (i == 0) {
			graphs.at(0)->Draw("apel");
			graphs.at(0)->GetYaxis()->SetRangeUser(0, 1.2);
			graphs.at(0)->SetName(graphName.at(0).data());
			leg.AddEntry(graphName.at(0).data(), legEntry.at(0).data(), "pel");
		}
		else {
			graphs.at(i)->Draw("pel same");
			graphs.at(i)->SetName(graphName.at(i).data());
			leg.AddEntry(graphName.at(i).data(), legEntry.at(i).data(), "pel");
		}
	}
	leg.Draw();
	float x=0.15, y=0.85, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Internal");
	c.SaveAs("efficiencyPhiGraph.pdf");
}

void makeHist(std::vector<TH1D> hists, int run, bool isBarrel, string BD){
	std::cout << "Inside makeHist function: hists.size() = " << hists.size() << " run " << run << " isBarrel " << isBarrel << " BD " << BD << std::endl;
	//~ std::cout << "hists.size() = " << hists.size() << std::endl;
	gStyle->SetOptStat(0);
	//~ std::vector<int> histColor = {kGray, kRed, kOrange, kCyan, kGreen+3, kBlue};
	std::vector<int> histColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	std::vector<string> histName = {"h0", "h1", "h2", "h3", "h4", "h5"};
	std::vector<string> legEntry = {"|#eta-index| = 1", "|#eta-index| = 2", "|#eta-index| = 3", "|#eta-index| = 4", "|#eta-index| = 5", "|#eta-index| = 6"};
	for (unsigned int i=0; i<hists.size(); ++i){
		hists.at(i).SetMarkerColor(histColor.at(i));
		hists.at(i).SetLineColor(histColor.at(i));
		hists.at(i).SetLineWidth(2);
		hists.at(i).SetMarkerStyle(marker.at(i));
		hists.at(i).SetMarkerSize(0.8);
	}
	string title; 
	if (run == 361689){
		if (isBarrel) title = "Run 361689, Barrel 3";
		else title = "Run 361689, Disk 6";
	}
	else if (run == 360414){
		if (isBarrel) title = "Run 360414, Barrel 6";
		else title = "Run 360414, Disk 2";
	}
	else if (run == 348251){
		if (BD == "b3d9"){
			if (isBarrel) title = "Run 348251, Barrel 3";
			else title = "Run 348251, Disk 9";
		}
		else if (BD == "b6d2"){
			if (isBarrel) title = "Run 348251, Barrel 6";
			else title = "Run 348251, Disk 2";
		}
	}
	else if (run == 367170){
		if (isBarrel) title = "Run 367170, Barrel 3";
	}
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Hit efficiency");
	//~ hists.at(0).GetXaxis()->SetRangeUser(90, 260);
	//~ hists.at(0).GetYaxis()->SetRangeUser(0.8, 1);
	TCanvas c; 
	TLegend leg(0.6, 0.25, 0.85, 0.50);
	gPad->SetTickx();
	gPad->SetTicky();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	//~ hists.at(0).SetName("h0");
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
		//~ if (i == 2){
			//~ hists.at(i).Draw("p");
			//~ hists.at(i).SetName(histName.at(i).data());
			//~ leg.AddEntry(histName.at(i).data(), legEntry.at(i).data());
		//~ }
	}
	//~ leg.AddEntry("h0", "|#eta-index| = 1");
	leg.Draw();
	c.SaveAs("efficiency.pdf");
	
	c.Clear();
	leg.Clear();
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Hit efficiency");
	hists.at(0).GetXaxis()->SetRangeUser(90, 255);
	hists.at(0).GetYaxis()->SetRangeUser(0.75, 1.05);
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
	}
	leg.Draw();
	c.SaveAs("efficiency_zoom.pdf");
}

void makeHistNonAbs(std::vector<TH1D> hists, int run, bool isBarrel, string BD, bool positive){
	std::cout << "Inside makeHistNonAbs function: hists.size() = " << hists.size() << " run " << run << " isBarrel " << isBarrel << " BD " << BD << " positive " << positive << std::endl;
	std::cout << "hists.size() = " << hists.size() << std::endl;
	gStyle->SetOptStat(0);
	//~ std::vector<int> histColor = {kGray, kRed, kOrange, kCyan, kGreen+3, kBlue};
	std::vector<int> histColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	std::vector<string> histName = {"h0", "h1", "h2", "h3", "h4", "h5"};
	std::vector<string> legEntry;
	if (positive)
		legEntry = {"#eta-index = 1", "#eta-index = 2", "#eta-index = 3", "#eta-index = 4", "#eta-index = 5", "#eta-index = 6"};
	else 
		legEntry = {"#eta-index = -1", "#eta-index = -2", "#eta-index = -3", "#eta-index = -4", "#eta-index = -5", "#eta-index = -6"};
		
	for (unsigned int i=0; i<hists.size(); ++i){
		hists.at(i).SetMarkerColor(histColor.at(i));
		hists.at(i).SetLineColor(histColor.at(i));
		hists.at(i).SetLineWidth(2);
		hists.at(i).SetMarkerStyle(marker.at(i));
		hists.at(i).SetMarkerSize(0.8);
	}
	string title; 
	if (run == 361689){
		if (isBarrel) title = "Run 361689, Barrel 3";
		else title = "Run 361689, Disk 6";
	}
	else if (run == 360414){
		if (isBarrel) title = "Run 360414, Barrel 6";
		else title = "Run 360414, Disk 2";
	}
	else if (run == 348251){
		if (BD == "b3d9"){
			if (isBarrel) title = "Run 348251, Barrel 3";
			else title = "Run 348251, Disk 9";
		}
		else if (BD == "b6d2"){
			if (isBarrel) title = "Run 348251, Barrel 6";
			else title = "Run 348251, Disk 2";
		}
	}
	else if (run == 367170){
		if (isBarrel) title = "Run 367170, Barrel 3";
	}
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Hit efficiency");
	//~ hists.at(0).GetXaxis()->SetRangeUser(90, 260);
	//~ hists.at(0).GetYaxis()->SetRangeUser(0.8, 1);
	TCanvas c; 
	TLegend leg(0.6, 0.25, 0.85, 0.50);
	gPad->SetTickx();
	gPad->SetTicky();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	//~ hists.at(0).SetName("h0");
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
		//~ if (i == 2){
			//~ hists.at(i).Draw("p");
			//~ hists.at(i).SetName(histName.at(i).data());
			//~ leg.AddEntry(histName.at(i).data(), legEntry.at(i).data());
		//~ }
	}
	//~ leg.AddEntry("h0", "|#eta-index| = 1");
	leg.Draw();
	c.SaveAs("efficiency_nonAbs.pdf");
	
	c.Clear();
	leg.Clear();
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Hit efficiency");
	hists.at(0).GetXaxis()->SetRangeUser(90, 255);
	hists.at(0).GetYaxis()->SetRangeUser(0.75, 1.05);
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
	}
	leg.Draw();
	c.SaveAs("efficiency_zoom_nonAbs.pdf");
	std::cout << "Exiting MakeHistNonAbs() function" << std::endl;
}

void makeGraphsNonAbs(std::vector<TGraphErrors*> graphs, int run, bool isBarrel, string BD, bool positive){
	std::cout << "Inside makeGraphsNonAbs function: graphs.size() " << graphs.size() << " run " << run << " isBarrel " << isBarrel << " BD " << BD << " positive " << positive << std::endl;
	gStyle->SetOptStat(0);
	
	std::vector<int> graphColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	
	std::vector<string> graphName = {"g0", "g1", "g2", "g3", "g4", "g5"};
	
	std::vector<string> legEntry = {};
	if (positive)
		legEntry = {"#eta_{index} = 1", "#eta_{index} = 2", "#eta_{index} = 3", "#eta_{index} = 4", "#eta_{index} = 5", "#eta_{index} = 6"};
	else 
		legEntry = {"#eta_{index} = -1", "#eta_{index} = -2", "#eta_{index} = -3", "#eta_{index} = -4", "#eta_{index} = -5", "#eta_{index} = -6"};
	
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		graphs.at(i)->SetMarkerColor(graphColor.at(i));
		graphs.at(i)->SetLineColor(graphColor.at(i));
		graphs.at(i)->SetMarkerStyle(marker.at(i));
		graphs.at(i)->SetMarkerSize(0.9);
	}
	
	string title; 
	if (run == 361689){
		if (isBarrel) title = "HV scan on 23/09/2018";
		else title = "HV scan on 23/09/2018, Disk 6";
	}
	else if (run == 360414){
		if (isBarrel) title = "HV scan on 10/09/2018, Barrel 6";
		else title = "HV scan on 10/09/2018, Disk 2";
	}
	else if (run == 348251){
		if (BD == "b3d9"){
			if (isBarrel) title = "HV scan on 18/04/2018, Barrel 3";
			else title = "HV scan on 18/04/2018, Disk 9";
		}
		else if (BD == "b6d2"){
			if (isBarrel) title = "HV scan on 18/04/2018, Barrel 6";
			else title = "HV scan on 18/04/2018, Disk 2";
		}
	}
	else if (run == 367170){
		if (isBarrel) title = "HV scan on 29/11/2018, Barrel 3";
	}
	graphs.at(0)->SetTitle(title.data());
	graphs.at(0)->GetXaxis()->SetTitle("HV [V]");
	graphs.at(0)->GetXaxis()->SetTitleOffset(1.1);
	graphs.at(0)->GetXaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetXaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetLabelSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleSize(0.04);
	graphs.at(0)->GetYaxis()->SetTitleOffset(0.9);
	graphs.at(0)->GetYaxis()->SetTitle("Hit efficiency");
	
	TCanvas c; 
	c.SetMargin(0.08, 0.02, 0.1, 0.06);
	TLegend leg(0.6, 0.25, 0.85, 0.55);
	leg.SetHeader("Barrel 3");
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	leg.SetFillStyle(0);
	
	for (unsigned int i=0; i<graphs.size(); ++i){
		if (i == 0) {
			graphs.at(0)->Draw("apel");
			graphs.at(0)->GetYaxis()->SetRangeUser(0, 1.2);
			graphs.at(0)->SetName(graphName.at(0).data());
			leg.AddEntry(graphName.at(0).data(), legEntry.at(0).data(), "pel");
		}
		else {
			graphs.at(i)->Draw("pel same");
			graphs.at(i)->SetName(graphName.at(i).data());
			leg.AddEntry(graphName.at(i).data(), legEntry.at(i).data(), "pel");
		}
	}
	leg.Draw();
	float x=0.15, y=0.85, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Preliminary");
	c.SaveAs("efficiencyGraphNonAbs.pdf");
	
	
}

void getEffPlots(int run, bool isBarrel, bool isEA, int side, string BD){
	std::cout << "Inside getEffPlots function: run = " << run << " isBarrel " << isBarrel << " isEA " << isEA << " side " << side << " BD " << BD << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	std::vector<int> etaIndices = getAbsEtaIndices(isBarrel);
	std::vector<TProfile*> profs;
	std::vector<TH1D> hists;
	std::vector<TGraphErrors*> graphs;
	TGraphErrors *gEff = nullptr;
	for (auto eta : etaIndices) {
		//~ TH1D hist("hist", "",26,-5,255);
		TH1D hist("hist", "",26,-5,255);
		int i = 0; //to iterate over scan points
		gEff = new TGraphErrors(scanPoints.size());
		gEff->SetName(TString::Format("graph_%d", eta).Data());
		float value = 0;
		float error1 = 0, error2 = 0, error = 0;
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			if (isBarrel == true){
				profs = getProfileX(getEffBarrel(run, sp, side, BD),run, isBarrel, isEA);
				p = profs.at(0);
				value = p->GetBinContent(p->FindBin(eta));
				value += p->GetBinContent(p->FindBin(-1*eta));
				//~ value = p->GetBinContent(p->FindBin(-1*eta));
				value = value/2;
				error1 = p->GetBinError(p->FindBin(eta));
				error2 = p->GetBinError(p->FindBin(-1*eta));
				error = TMath::Sqrt(error1*error1 + error2*error2);
				//~ error = p->GetBinError(p->FindBin(-eta));
			}
			else if (isBarrel == false){
				profs = getProfileX(getEffEndcap(run, sp, side, isEA, BD), run, false, isEA);
				//~ std::cout << "eta = " << eta << std::endl;
				p = profs.at(eta-1);
				value = p->GetBinContent(p->FindBin(eta-1));//because eta indices for the endcaps starts from 0 and ends at 2
				error = p->GetBinError(p->FindBin(eta-1));
			}
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
			if (value > 0.995) std::cout << "Efficiency above 99.5% for HV = " << std::stoi(sp) << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			//~ int sPt = std::stoi(sp);
			//~ std::cout << "Scan point as integer = " << sPt << std::endl;
			hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			hist.SetBinError(hist.FindBin(std::stoi(sp)), error);
			gEff->SetPoint(i, std::stoi(sp), value);
			gEff->SetPointError(i, 0, error);
			++i;
		}
		hists.push_back(hist);
		graphs.push_back(gEff);
	}
	//~ makeHist(hists, run, isBarrel, BD);
	makeGraphs(graphs, run, isBarrel, isEA, BD);
	//~ makeGraphsNonAbs(graphs, run, isBarrel, BD, false);
	//~ makeHistNonAbs(hists, run, true, BD, true);
	std::cout << "Exiting getEffPlots() function" << std::endl;
}

void getEffPlotsPhi(int run, int side, string BD){
	
	std::cout << "Inside getEffPlotsPhi function: run = " << run << " side " << side << " BD " << BD << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	//~ int phiLow = getPhiRange(run).at(0);
	//~ int phiHigh = getPhiRange(run).at(1);
	std::vector<TProfile*> profs;
	std::vector<int> phiRange = getPhiRange(run);
	std::vector<TGraphErrors*> graphs;
	TGraphErrors *gEff = nullptr;
	std::vector<std::pair<int, int>> phiRanges = getPhiRanges();
	
	for (int i=0; i < phiRanges.size(); ++i){
		gEff = new TGraphErrors(scanPoints.size());
		gEff->SetName(TString::Format("graph_%d", i).Data());
		
		std::pair<int, int> phiRange =  phiRanges.at(i);
		
		double value = 0;
		double error = 0;
		double valueFinal = 0;
		double errorFinal = 0;
		
		std::vector<double> values;
		std::vector<double> errors;
		int j = 0;
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			profs = getProfileY(getEffBarrel(run, sp, side, BD), run);
			p = profs.at(0);
			
			for (int phi = phiRange.first; phi <= phiRange.second; ++phi){
				value = p->GetBinContent(p->FindBin(phi));
				error = p->GetBinError(p->FindBin(phi));
				values.push_back(value);
				errors.push_back(error);
				std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for phi = " << phi << std::endl;
			}
			
			for (int k=0; k<values.size(); ++k){
				valueFinal = valueFinal + values.at(k);
				errorFinal = errorFinal + errors.at(k)*errors.at(k);
			}
			std::cout << "Final value in this scan point = " << valueFinal/values.size() << " +/- " << TMath::Sqrt(errorFinal) << " for phi range = (" << phiRanges.at(i).first << ", " << phiRanges.at(i).second << ") values.size() " <<
			values.size() << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			gEff->SetPoint(j, std::stoi(sp), valueFinal/values.size());
			gEff->SetPointError(j, 0, TMath::Sqrt(errorFinal));
			errorFinal = 0;
			valueFinal = 0;
			values.clear();
			errors.clear();
			++j;
		}
		graphs.push_back(gEff);
	}
	makeGraphsPhi(graphs, run, BD);
	
	
	/*for (auto phi : phiRange){
		int i = 0; //to iterate over scan points
		gEff = new TGraphErrors(scanPoints.size());
		gEff->SetName(TString::Format("graph_%d", phi).Data());
		
		float value = 0;
		float error1 = 0, error2 = 0, error = 0;
		
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			profs = getProfileY(getEffBarrel(run, sp, side, BD), run);
			p = profs.at(0);
			
			
			
			value = p->GetBinContent(p->FindBin(phi));
			error = p->GetBinError(p->FindBin(phi));
			
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for phi = " << phi << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			gEff->SetPoint(i, std::stoi(sp), value);
			gEff->SetPointError(i, 0, error);
			++i;
		}
		graphs.push_back(gEff);
	}
	makeGraphsPhi(graphs, run, BD);*/
}

void getEffPlotsL96(int run, int side, bool onlyL96){
	std::cout << "Inside getEffPlotsL96 function: run = " << run << " side " << side << " onlyL96 " << onlyL96 << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	std::vector<int> etaIndices = getAbsEtaIndices(true);
	//~ std::vector<TProfile*> profs;
	std::vector<TH1D> hists;
	string BD = "";
	if (run==348251) BD = "b3d9";
	std::cout << "BD = " << BD << std::endl;
	for (auto eta : etaIndices) {
		TH1D hist("hist", "",16,-5,155);
		//~ TH1D hist("hist", "",26,-5,255);
		float value = 0;
		float error1 = 0, error2 = 0, error = 0;
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			p = getProfileXL96(getEffBarrel(run, sp, side, BD), run, onlyL96);
			//~ p = profs.at(0);
			//~ value = p->GetBinContent(p->FindBin(-eta));
			value = p->GetBinContent(p->FindBin(eta));
			value += p->GetBinContent(p->FindBin(-1*eta));
			value = value/2;
			//~ error = p->GetBinError(p->FindBin(-eta));
			error1 = p->GetBinError(p->FindBin(eta));
			error2 = p->GetBinError(p->FindBin(-1*eta));
			error = TMath::Sqrt(error1*error1 + error2*error2);
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			//~ int sPt = std::stoi(sp);
			//~ std::cout << "Scan point as integer = " << sPt << std::endl;
			hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			hist.SetBinError(hist.FindBin(std::stoi(sp)), error);
		}
		hists.push_back(hist);
	}
	makeHist(hists, run, true, BD);
	//~ makeHistNonAbs(hists, run, true, BD, false);
}

void makeNoiseHist(std::vector<TH1D> hists, int run, bool isBarrel){
	std::cout << "Inside makeNoiseHist function: hists.size() = " << hists.size() << " run " << run << " isBarrel " << isBarrel << std::endl;
	std::cout << "hists.size() = " << hists.size() << std::endl;
	  gStyle->SetOptStat(0);
	// std::vector<int> histColor = {kGray, kRed, kOrange, kCyan, kGreen+3, kBlue};
	std::vector<int> histColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	std::vector<string> histName = {"h0", "h1", "h2", "h3", "h4", "h5"};
	std::vector<string> legEntry = {"|#eta-index| = 1", "|#eta-index| = 2", "|#eta-index| = 3", "|#eta-index| = 4", "|#eta-index| = 5", "|#eta-index| = 6"};
	for (unsigned int i=0; i<hists.size(); ++i){
		hists.at(i).SetMarkerColor(histColor.at(i));
		hists.at(i).SetLineColor(histColor.at(i));
		hists.at(i).SetLineWidth(2);
		hists.at(i).SetMarkerStyle(marker.at(i));
		hists.at(i).SetMarkerSize(0.8);
	}
	string title; 
	if (run == 361689){
		if (isBarrel) title = "Run 361689, Barrel 3";
		else title = "Run 361689, Disk 6";
	}
	else if (run == 360414){
		if (isBarrel) title = "Run 360414, Barrel 6";
		else title = "Run 360414, Disk 2";
	}
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Noise occupancy");
	hists.at(0).GetXaxis()->SetRangeUser(0, 260);
	hists.at(0).GetYaxis()->SetRangeUser(0, 140);
	TCanvas c; 
	TLegend leg(0.65, 0.55, 0.85, 0.80);
	gPad->SetTickx();
	gPad->SetTicky();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	// hists.at(0).SetName("h0");
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
		// if (i == 2){
			// hists.at(i).Draw("p");
			// hists.at(i).SetName(histName.at(i).data());
			// leg.AddEntry(histName.at(i).data(), legEntry.at(i).data());
		// }
	}
	// leg.AddEntry("h0", "|#eta-index| = 1");
	leg.Draw();
	c.SaveAs("noise.pdf");
	
	c.Clear();
	leg.Clear();
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Noise occupancy");
	hists.at(0).GetXaxis()->SetRangeUser(90, 260);
	hists.at(0).GetYaxis()->SetRangeUser(0, 100);
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
	}
	leg.Draw();
	c.SaveAs("noise_zoom.pdf");
}

void makeNoiseHistNonAbs(std::vector<TH1D> hists, int run, bool isBarrel, bool positive){
	std::cout << "Inside makeNoiseHistNonAbs function: hists.size() = " << hists.size() << " run " << run << " isBarrel " << isBarrel << " positive " << positive << std::endl;
	std::cout << "hists.size() = " << hists.size() << std::endl;
	  gStyle->SetOptStat(0);
	// std::vector<int> histColor = {kGray, kRed, kOrange, kCyan, kGreen+3, kBlue};
	std::vector<int> histColor = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> marker = {20,21,22,23,33,29};
	std::vector<string> histName = {"h0", "h1", "h2", "h3", "h4", "h5"};
	std::vector<string> legEntry;
	if (positive)
		legEntry = {"#eta-index = 1", "#eta-index = 2", "#eta-index = 3", "#eta-index = 4", "#eta-index = 5", "#eta-index = 6"};
	else 
		legEntry = {"#eta-index = -1", "#eta-index = -2", "#eta-index = -3", "#eta-index = -4", "#eta-index = -5", "#eta-index = -6"};
	for (unsigned int i=0; i<hists.size(); ++i){
		hists.at(i).SetMarkerColor(histColor.at(i));
		hists.at(i).SetLineColor(histColor.at(i));
		hists.at(i).SetLineWidth(2);
		hists.at(i).SetMarkerStyle(marker.at(i));
		hists.at(i).SetMarkerSize(0.8);
	}
	string title; 
	if (run == 361689){
		if (isBarrel) title = "Run 361689, Barrel 3";
		else title = "Run 361689, Disk 6";
	}
	else if (run == 360414){
		if (isBarrel) title = "Run 360414, Barrel 6";
		else title = "Run 360414, Disk 2";
	}
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Noise occupancy");
	hists.at(0).GetXaxis()->SetRangeUser(0, 260);
	hists.at(0).GetYaxis()->SetRangeUser(0, 140);
	TCanvas c; 
	TLegend leg(0.65, 0.55, 0.85, 0.80);
	gPad->SetTickx();
	gPad->SetTicky();
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	// hists.at(0).SetName("h0");
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
		// if (i == 2){
			// hists.at(i).Draw("p");
			// hists.at(i).SetName(histName.at(i).data());
			// leg.AddEntry(histName.at(i).data(), legEntry.at(i).data());
		// }
	}
	// leg.AddEntry("h0", "|#eta-index| = 1");
	leg.Draw();
	c.SaveAs("noise_nonAbs.pdf");
	
	c.Clear();
	leg.Clear();
	hists.at(0).SetTitle(title.data());
	hists.at(0).GetXaxis()->SetTitle("HV [V]");
	hists.at(0).GetXaxis()->SetTitleOffset(1.1);
	hists.at(0).GetYaxis()->SetTitle("Noise occupancy");
	hists.at(0).GetXaxis()->SetRangeUser(90, 260);
	hists.at(0).GetYaxis()->SetRangeUser(0, 100);
	leg.SetBorderSize(0);
	leg.SetTextSize(0.04);
	for (unsigned int i=0; i<hists.size(); ++i){
		if (i == 0) {
			hists.at(0).Draw("pe");
			hists.at(0).SetName(histName.at(0).data());
			leg.AddEntry(histName.at(0).data(), legEntry.at(0).data(), "pl");
		}
		else {
			hists.at(i).Draw("pe same");
			hists.at(i).SetName(histName.at(i).data());
			leg.AddEntry(histName.at(i).data(), legEntry.at(i).data(), "pl");
		}
	}
	leg.Draw();
	c.SaveAs("noise_zoom_nonAbs.pdf");
}

void getNoisePlots(int run, bool isBarrel, bool isEA, int side){
	std::cout << "Inside getNoisePlots function: run = " << run << " isBarrel " << isBarrel << " isEA " << isEA << " side " << side << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	std::vector<int> etaIndices = getAbsEtaIndices(isBarrel);
	
	std::vector<TH1D> hists;
	for (auto eta : etaIndices) {
		// TH1D hist("hist", "",26,-5,255);
		TH1D hist("hist", "",26,-5,255);
		std::vector<TProfile*> profs;
		float value = 0;
		float error1 = 0, error2 = 0, error = 0;
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			if (isBarrel == true){
				profs = getProfileX(getNoiseBarrel(run, sp, side),run, isBarrel, isEA);
				p = profs.at(0);
				value = p->GetBinContent(p->FindBin(-eta));
				//~ value = p->GetBinContent(p->FindBin(eta));
				//~ value += p->GetBinContent(p->FindBin(-1*eta));
				//~ value = value/2;
				error = p->GetBinError(p->FindBin(-eta));
				//~ error1 = p->GetBinError(p->FindBin(eta));
				//~ error2 = p->GetBinError(p->FindBin(-1*eta));
				//~ error = TMath::Sqrt(error1*error1 + error2*error2);
			}
			else if (isBarrel == false){
				profs = getProfileX(getNoiseEndcap(run, sp, side, isEA), run, false, isEA);
				std::cout << "isEA " << isEA << " eta = " << eta << " prof name " << profs.at(eta-1)->GetName() << std::endl;
				p = profs.at(eta-1);
				value = p->GetBinContent(p->FindBin(eta-1));
				error = p->GetBinError(p->FindBin(eta-1));
			}
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			// int sPt = std::stoi(sp);
			// std::cout << "Scan point as integer = " << sPt << std::endl;
			hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			hist.SetBinError(hist.FindBin(std::stoi(sp)), error);
		}
		hists.push_back(hist);
	}
	//~ makeNoiseHist(hists, run, isBarrel);
	makeNoiseHistNonAbs(hists, run, isBarrel, false);
}


void getNoisePlotsL96(int run, bool isBarrel, int side, bool onlyL96){
	std::cout << "Inside getNoisePlotsL96 function: run = " << run << " isBarrel " << isBarrel << " side " << side << " only96 " << onlyL96 << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	std::vector<int> etaIndices = getAbsEtaIndices(isBarrel);
	
	std::vector<TH1D> hists;
	for (auto eta : etaIndices) {
		// TH1D hist("hist", "",26,-5,255);
		TH1D hist("hist", "",26,-5,255);
		//~ std::vector<TProfile*> profs;
		float value = 0;
		float error1 = 0, error2 = 0, error = 0;
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			if (isBarrel == true){
				p = getProfileXL96(getNoiseBarrel(run, sp, side), run, onlyL96);
				//~ p = profs.at(0);
				//~ value = p->GetBinContent(p->FindBin(-eta));
				value = p->GetBinContent(p->FindBin(eta));
				value += p->GetBinContent(p->FindBin(-1*eta));
				value = value/2;
				//~ error = p->GetBinError(p->FindBin(-eta));
				error1 = p->GetBinError(p->FindBin(eta));
				error2 = p->GetBinError(p->FindBin(-1*eta));
				error = TMath::Sqrt(error1*error1 + error2*error2);
			}
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			// int sPt = std::stoi(sp);
			// std::cout << "Scan point as integer = " << sPt << std::endl;
			hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			hist.SetBinError(hist.FindBin(std::stoi(sp)), error);
		}
		hists.push_back(hist);
	}
	makeNoiseHist(hists, run, isBarrel);
	//~ makeNoiseHistNonAbs(hists, run, isBarrel, false);
}

std::vector<float> getModuleEff(int run, bool isBarrel, bool isEA, string sp, int layer, int side, int eta, int phi){
	std::cout << "Inside getModuleEff function: run " << run << " isBarrel " << isBarrel << " isEA " << isEA <<  " sp " << sp << " layer " << layer << " side " << side <<  " eta " << eta << " phi " << phi << std::endl;
	TProfile2D* prof = nullptr;
	if (isBarrel)
		prof = getEffBarrelHHVscan(run, sp, layer, side);
	else if (isBarrel == false && isEA == true)
		prof = getEffEndcapHHVscan(run, true, sp, layer, side);
	else if (isBarrel == false && isEA == false)
		prof = getEffEndcapHHVscan(run, false, sp, layer, side);
		std::cout << "prof " << prof << std::endl;
		std::vector<float> eff;
		int SP = std::stoi(sp);
		//~ if ((phi != 21) && (SP > 200)) return eff;
		int bin = prof->FindFixBin(eta, phi);
		std::cout << "Bin found = " << bin << std::endl; 
		float val = prof->GetBinContent(prof->FindFixBin(eta, phi));
		float err = prof->GetBinError(prof->FindFixBin(eta, phi));
		eff.push_back(val);
		eff.push_back(err);
		return eff;
}

std::vector<float> getModuleNoise(int run, string sp, int layer, int side, int eta, int phi){
	std::cout << "Inside getModuleNoise function: run " << run << " sp " << sp << " layer " << layer << " side " << side <<  " eta " << eta << " phi " << phi << std::endl;
	TProfile2D* prof = nullptr;
	prof = getNoiseBarrelHHVscan(run, sp, layer, side);
	std::cout << "prof " << prof << std::endl;
	std::vector<float> noise;
	int SP = std::stoi(sp);
	if ((phi != 21) && (SP > 200)) return noise;
	int bin = prof->FindFixBin(eta, phi);
	std::cout << "Bin found = " << bin << std::endl; 
	float val = prof->GetBinContent(prof->FindFixBin(eta, phi));
	float err = prof->GetBinError(prof->FindFixBin(eta, phi));
	std::cout << "Value in this bin = " << val << " error = " << err << std::endl;
	noise.push_back(val);
	noise.push_back(err);
	return noise;
}

std::vector<float> getModuleCurrent(int run, bool isBarrel, bool isEA, int layer, int eta, int phi){
	std::vector<float> current;
	if (isBarrel){
		if (layer == 0){
			if (eta == 1 && phi == 20) current = {235.4, 351.3, 510.9, 624.4, 724.0, 958.6, 1064.9, 1129.3, 1163.9, 1189.2, 1192.7, 1211.1, 1227.2, 1258.4};
			else if (eta == -6 && phi == 21) current = {256.6, 378.2, 550.6, 669.8, 774.8, 1017.3, 1131.6, 1205.9, 1246.6, 1275.1, 1281.4, 1300.1, 1318.3, 1353.7};
		}
		else if (layer == 2){
			if (eta == 1 && phi == 37) current = {210.4, 298.5, 412.5, 495.2, 555.8, 639.5, 654.1, 664.6, 671.4, 677.1, 681.3, 686.7, 692.0};
			else if (eta == -5 & phi == 33) current = {227.5, 317.0, 435.6, 521.5, 589.0, 694.9, 714.3, 725.7, 732.6, 739.1};
		}
		else if (layer == 3){
			if (eta == 1 & phi == 26) current = {316.0, 475.1, 683.3, 821.0, 912.3, 1008.2, 1028.6, 1042.2, 1050.1, 1061.4};
			else if (eta == -6 & phi == 24) current = {312.7, 475.9, 687.2, 835.9, 950.7, 1096.7, 1124.0, 1137.7, 1153.9, 1164.1};
		}
	}
	else if (isBarrel == false && isEA == true){
		if (eta == 0 && phi == 41) current = {132.5, 187.0, 259.7, 312.6, 351.4, 404.5, 410.4, 419.2, 423.8, 427.0, 432.0, 432.2, 437.6};
		else if (eta == 1 && phi == 31) current = {155.6, 221.2, 315.3, 387.1, 448.4, 560.4, 580.1, 597.1, 606.6, 613.2, 621.5, 622.1, 630.7};
		else if (eta == 2 && phi == 23) current = {92.6, 130.9, 191.5, 241.3, 281.3, 335.8, 343.1, 352.2, 357.1, 361.1, 366.6, 368.0, 373.9};
	}
	else if (isBarrel == false && isEA == false){
		if (eta == 0 && phi == 41) current = {102.3, 140.5, 194.8, 233.1, 261.9, 303.8, 310.8, 315.5, 319.9, 322.1, 324.7, 326.9, 329.7};
		else if (eta == 1 && phi == 31) current = {101.2, 142.5, 202.0, 248.7, 284.1, 353.7, 369.6, 379.0, 385.6, 389.1, 392.7, 396.7, 399.8};
		else if (eta == 2 && phi == 23) current = {62.9, 87.1, 126.4, 159.4, 184.1, 219.8, 225.5, 229.9, 233.7, 235.8, 238.5, 240.6, 244.1};
	}
	return current;
}

void getEffPlotsExclMod(int run, bool isBarrel, bool isEA, int side, string BD, bool positiveEta){
	std::cout << "Inside getEffPlotsExclMod function: run = " << run << " isBarrel " << isBarrel << " isEA " << isEA << " side " << side << " BD " << BD << " positiveEta " << positiveEta << std::endl;
	std::vector<string> scanPoints = getScanPoints(run);
	std::vector<int> etaIndices;
	if (isBarrel) {
		if (positiveEta) etaIndices = getAbsEtaIndices(true);
		else etaIndices = {-1, -2, -3, -4, -5, -6};
	}
	else etaIndices = getAbsEtaIndices(false); // for the endcaps - eta there only between 1 and 3 
	std::vector<TProfile*> profs;
	std::vector<TH1D> hists;
	for (auto eta : etaIndices) {
		//~ TH1D hist("hist", "",26,-5,255);
		TH1D hist("hist", "",26,-5,255);
		float value = 0;
		float error = 0;
		std::vector<std::pair<int, int>> exclModules = {
		{-6, 14}, {-6, 21}, {-5, 14}, {-5, 21}, {-4, 20}, {-3, 20}, {-3, 21}, {-2, 20}, {-1, 20}, {1, 20}, {2, 22}, {3, 22}, {4, 5}, {4, 20}, {6, 31}
		};
		//~ std::vector<std::pair<int, int>> exclModules = {};
		for (auto sp : scanPoints){
			TProfile* p = nullptr;
			if (isBarrel == true){
				profs = getProfileExclMod(getEffBarrel(run, sp, side, BD), run, positiveEta, exclModules);
				p = profs.at(TMath::Abs(eta)-1);
				value = p->GetBinContent(p->FindBin(eta));
				error = p->GetBinError(p->FindBin(eta));
				//~ std::cout << "Using positive eta = " << eta << "in bin " << p->FindBin(eta) << " the eff = " << value << " +/- " << error << std::endl;
			}
			std::cout << "Value in this scanPoint = " << value << " +/- " << error << " for eta = " << eta << std::endl;
			std::cout << "-----------------------------------------------------------------------------------------" << std::endl;
			//~ int sPt = std::stoi(sp);
			//~ std::cout << "Scan point as integer = " << sPt << std::endl;
			hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			hist.SetBinError(hist.FindBin(std::stoi(sp)), error);
		}
		hists.push_back(hist);
	}
	if (positiveEta) makeHistNonAbs(hists, run, true, BD, true);
	else makeHistNonAbs(hists, run, true, BD, false);
	std::cout << "Exiting getEffPlotsExclMod() function" << std::endl;
}

int main(){
	
	//~ getEffPlots(348251, true, false, 2, "b3d9");
	
	//~ getEffPlotsPhi(361689, 2, "");
	
	
	//~ std::vector<float> moduleEff = getModuleEff(363664, "030", 0, 0, 1, 21);
	//~ std::cout << "Value = " << moduleEff.at(0) << " error = " << moduleEff.at(1) << std::endl;
	//~ getEffPlots(348251, true, false, 2, "b3d9");
	
	/*std::vector<std::string> scanPoints = getScanPoints(367170);
	getEffPlots(367170, true, false, 2, "");*/
	//~ getNoisePlots(367170, true, false, 2);
	//~ getEffPlotsL96(367170, 2, false);
	//~ for (auto sp : scanPoints){
		//~ TFile* f = OpenFile(367170, sp); 
	//~ }
	
	/*********************************************************************/
	/******************* MODULE EFF AND NO *******************************/
	/*********************************************************************/
	int run = 361689;
	std::vector<string> scanPoints = getScanPoints(run);
	TCanvas c;
	c.SetMargin(0.08, 0.02, 0.1, 0.06);
	
	struct Module {
		int layer;
		int side;
		int eta;
		int phi;
	};
	
	std::vector<Module> modules = {
		{5, 0, 0, 1},//disk 5, ring 1
		{5, 0, 0, 6},
		{5, 0, 0, 9},
	};
	
	std::vector<int> gColours = {kOrange-3, kSpring+9, kTeal+3, kAzure+1, kBlue+3, kPink+8};
	//~ std::vector<int> gColours = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	std::vector<int> gMarkers = {20,21,22,23,33,29};
	std::vector<std::string> legEntry; //= {"Entry 1", "Entry 2", "Entry 3"};
	int i = 0, j = 0; //i to loop over scan points, j to loop over eff graphs
	TGraphErrors* gEff = nullptr;
	TLegend leg(0.5, 0.2, 0.85, 0.5);
	
	bool isEA = false;
	
	std::string legTitle = "";
	
	if (isEA)
		legTitle = "Endcap A, ";
	else 
	  legTitle = "Endcap C, ";
	
	int eta = 0;
	std::vector<std::pair<int, int>> phiRangesEta;
	if (eta == 0){
		if (isEA)
			phiRangesEta = {{1, 3}, {6, 8}, {10, 13}};
		else 
			phiRangesEta = {{0, 1}, {4, 5}, {7, 8}};
		std::string entry;
		for(int i=0; i<3; ++i){
			entry = std::string("#phi_{index} #in [") + std::to_string(phiRangesEta.at(i).first) + std::string(", ") + std::to_string(phiRangesEta.at(i).second) + std::string("]");
			legEntry.push_back(entry);
		}
		legTitle = legTitle + std::string("Outer Ring");
	}
	else if (eta == 1){
		if (isEA)
			phiRangesEta = {{0, 1}, {4, 5}, {8, 9}};
		else 
			phiRangesEta = {{0, 1}, {4, 5}, {8, 9}};
		std::string entry;
		for(int i=0; i<3; ++i){
			entry = std::string("#phi_{index} #in [") + std::to_string(phiRangesEta.at(i).first) + std::string(", ") + std::to_string(phiRangesEta.at(i).second) + std::string("]");
			legEntry.push_back(entry);
		}
		legTitle = legTitle + std::string("Middle Ring");
	}
	else if (eta == 2){
		if (isEA)
			phiRangesEta = {{1, 2}, {5, 6}, {9, 10}};
		else 
		 	phiRangesEta = {{0, 1}, {4, 5}, {8, 9}};
		std::string entry;
		for(int i=0; i<3; ++i){
			entry = std::string("#phi_{index} #in [") + std::to_string(phiRangesEta.at(i).first) + std::string(", ") + std::to_string(phiRangesEta.at(i).second) + std::string("]");
			legEntry.push_back(entry);
		}
		legTitle = legTitle + std::string("Inner Ring");
	}
	leg.SetHeader(legTitle.data());
	std::vector<float> modEfficiencies;
	std::vector<float> modEfficienciesErr;
	float modEff = 0;
	float modEffErr = 0;
	
	//loop over phi ranges
	for (int index=0; index<phiRangesEta.size(); ++index){
		Module mod;
		mod.layer = 5;
		mod.side = 0;
		mod.eta = eta;
		
		gEff = new TGraphErrors(scanPoints.size());
		gEff->SetName(TString::Format("graph_%d", index).Data());
		std::vector<float> moduleEff;
		gEff->SetMarkerSize(0.9);
		
		string title; 
		if (run == 361689){
			title = "HV scan on 23/09/2018";
		}
		else if (run == 367170){
			title = "HV scan on 29/11/2018";
		}
	
		gEff->SetTitle(title.data());
		gEff->GetXaxis()->SetTitle("HV [V]");
		gEff->GetXaxis()->SetTitleOffset(1.1);
		gEff->GetXaxis()->SetTitleSize(0.04);
		gEff->GetXaxis()->SetLabelSize(0.04);
		gEff->GetYaxis()->SetLabelSize(0.04);
		gEff->GetYaxis()->SetTitleSize(0.04);
		gEff->GetYaxis()->SetTitleOffset(0.9);
		gEff->GetYaxis()->SetTitle("Hit efficiency");
		
		//loop over scan points
		for (auto sp: scanPoints){
			std::cout << "Inside loop over scan points, sp: " << sp << " i " << i << " for graph no " << j << std::endl;
			int SP = std::stoi(sp);
			//loop over phi in the phi range
			int phiInRange = 0;
			for (int phi=phiRangesEta.at(index).first; phi<phiRangesEta.at(index).second; ++phi){
				mod.phi = phi;
				modEfficiencies.push_back(getModuleEff(361689, false, isEA, sp, mod.layer, mod.side, mod.eta, mod.phi).at(0));
				modEfficienciesErr.push_back(getModuleEff(361689, false, isEA, sp, mod.layer, mod.side, mod.eta, mod.phi).at(1));
				++phiInRange;
			}
			
			for (int nPhi=0; nPhi<phiInRange; ++nPhi){
				modEff += modEfficiencies.at(nPhi);
				modEffErr = modEffErr + modEfficienciesErr.at(nPhi)*modEfficienciesErr.at(nPhi);
			}
			
			modEff = modEff/phiInRange;
			modEffErr = TMath::Sqrt(modEffErr);
			
			gEff->SetPoint(i, SP, modEff);
			gEff->SetPointError(i, 0, modEffErr);
			
			modEff = 0;
			modEffErr = 0;
			phiInRange = 0;
			modEfficiencies.clear();
			modEfficienciesErr.clear();
			++i;
		}
		i = 0;
		
		gEff->SetMarkerColor(gColours.at(j));
		gEff->SetMarkerStyle(gMarkers.at(j));
		gEff->SetLineColor(gColours.at(j));
		
		if (j == 0) {
			gEff->GetXaxis()->SetRangeUser(0, 300);
			gEff->GetYaxis()->SetRangeUser(0, 1.2);
			gEff->SetTitle("");
			gEff->GetXaxis()->SetTitle("HV [V]");
			gEff->GetYaxis()->SetTitle("Hit Efficiency");
			gEff->Draw("APEL");
			leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		}
		else  {	
			gEff->Draw("PEL same");
			leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		}
		
		++j;
		std::cout << "End of loop over graphs (phi ranges), incremented j to " << j << std::endl;
		std::cout << "******************************************************************" << std::endl;
	}
	
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	leg.SetBorderSize(0);
  leg.SetFillStyle(0);
	leg.Draw();
	float x1=0.15, y1=0.85, labelOffset1=0.12;
	TLatex txt1;
	txt1.SetTextFont(72);
	txt1.SetTextSize(0.05);
	txt1.DrawLatexNDC(x1,y1,"ATLAS");
	txt1.SetTextFont(42);
	txt1.DrawLatexNDC(x1+labelOffset1,y1,"SCT Internal");
	c.SaveAs("moduleEfficiency.pdf");
	
	std::cout << "###############################################################################" << std::endl;
	std::cout << "###############################################################################" << std::endl;
	std::cout << "###############################################################################" << std::endl;
	
	TCanvas cProfiles;
	gStyle->SetOptStat(0);
	cProfiles.SetMargin(0.08, 0.02, 0.1, 0.06);
	//~ std::vector<TProfile*> p1 = getProfileX(getEffBarrel(361689, "040", 2, ""), 361689, true, false);
	//~ p1.at(0)->Draw("pel");
	gPad->SetTickx();
	gPad->SetTicky();
	//~ gPad->SetGrid();
	std::vector<TProfile*> p;
	int counter = 0;
	
	TLegend profLeg(0.6, 0.68, 0.95, 0.92);
	profLeg.SetBorderSize(0);
	profLeg.SetFillStyle(0);
	profLeg.SetNColumns(2);
	profLeg.SetTextSize(0.04);
	
	std::vector<int> profColour = {kOrange, kRed, kPink, kMagenta, kViolet, kBlue, kAzure, kCyan, kTeal, kSpring};
	std::vector<int> profMarker = {20, 21, 22, 23, 29, 33, 34, 43, 47, 41};
	std::vector<string> profLegend = {"HV = 40V", "HV = 60V", "HV = 80V", "HV = 100V", "HV = 120V", "HV = 140V", "HV = 160V", "HV = 180V", "HV = 200V", "HV = 250V"};
	
	string profName;
	for (auto sp : scanPoints){
		
		p = getProfileX(getEffBarrel(361689, sp, 2, ""), 361689, true, false);
		p.at(0)->SetMarkerColor(profColour.at(counter));
		p.at(0)->SetLineColor(profColour.at(counter));
		p.at(0)->SetMarkerStyle(profMarker.at(counter));
		p.at(0)->GetXaxis()->SetTitle("#eta_{index}");
		p.at(0)->GetYaxis()->SetTitle("Hit Efficiency");
		p.at(0)->GetXaxis()->SetTitleOffset(1.1);
		p.at(0)->GetXaxis()->SetTitleSize(0.04);
		p.at(0)->GetXaxis()->SetLabelSize(0.04);
		p.at(0)->GetYaxis()->SetLabelSize(0.04);
		p.at(0)->GetYaxis()->SetTitleSize(0.04);
		p.at(0)->GetYaxis()->SetTitleOffset(0.9);
		if (counter == 0){
			p.at(0)->Draw("pel");
			p.at(0)->GetYaxis()->SetRangeUser(0, 1.5);
			p.at(0)->SetTitle("");
			//~ p.at(0)->SetTitleSize(0);
			profName = std::string("prof")+std::to_string(counter);
			p.at(0)->SetName(profName.data());
			std::cout << "profName = " << profName << std::endl;
			profLeg.AddEntry(p.at(0)->GetName(), profLegend.at(counter).data(), "PEL");
		}
		else {
			p.at(0)->Draw("pel same");
			profName = std::string("prof")+std::to_string(counter);
			p.at(0)->SetName(profName.data());
			
			std::cout << "profName = " << profName << std::endl;
			profLeg.AddEntry(p.at(0)->GetName(), profLegend.at(counter).data(), "PEL");
		}
		++counter;
	}
	profLeg.Draw();
	float x=0.15, y=0.85, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Internal");
	cProfiles.SaveAs("Barrel3Profiles.pdf");
	
	//~ for (const Module& module : modules) {
		//~ std::cout << "Inside loop over modules: layer " << module.layer << " side " << module.side << " eta " << module.eta << " phi " << module.phi << std::endl;
		//~ 
		//~ gEff = new TGraphErrors(modules.size());
		//~ gEff->SetName(TString::Format("graph_%d", j).Data());
		//~ std::vector<float> moduleEff;
		//~ gEff->SetMarkerSize(0.8);
		//~ for (auto sp: scanPoints){
			//~ std::cout << "Inside loop over scan points, sp: " << sp << " i " << i << " module.layer: " << module.layer << std::endl;
			//~ int SP = std::stoi(sp);
			//~ moduleEff = getModuleEff(361689, false, true, sp, module.layer, module.side, module.eta, module.phi);
			//~ gEff->SetPoint(i, SP, moduleEff.at(0));
			//~ gEff->SetPointError(i, 0, moduleEff.at(1));
			//~ ++i;
		//~ }
		//~ i = 0;
		//~ 
		//~ gEff->SetMarkerColor(gColours.at(j));
		//~ gEff->SetMarkerStyle(gMarkers.at(j));
		//~ gEff->SetLineColor(gColours.at(j));
		//~ 
		//~ if (j == 0) {
			//~ gEff->GetXaxis()->SetRangeUser(0, 300);
			//~ gEff->GetYaxis()->SetRangeUser(0, 1.2);
			//~ gEff->SetTitle("");
			//~ gEff->GetXaxis()->SetTitle("HV [V]");
			//~ gEff->GetYaxis()->SetTitle("Hit Efficiency");
			//~ gEff->Draw("APEL");
			//~ leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		//~ }
		//~ else  {	
			//~ gEff->Draw("PEL same");
			//~ leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		//~ }
		//~ ++j;
		//~ std::cout << "End of loop over modules, incremented j to " << j << std::endl;
		//~ std::cout << "******************************************************************" << std::endl;
	//~ }

	
	//~ double r = 11.2e3; //resistance in Ohms
	//~ 
	//~ std::vector<Module> modules = {
		//~ // {0, 0, 1, 20}, //6142 barrel 3
		//~ // {0, 0, -6, 21}, //8400 
		//~ // {2, 0, 1, 37}, //0106 barrel 5
		//~ // {2, 0, -5, 33}, //8601
		//~ // {3, 0, 1, 26}, //7518 barrel 6
		//~ // {3, 0, -6, 24}, //7412
		//~ {5, 0, 0, 41}, //1233 endcap c
		//~ {5, 0, 1, 31}, //1227
		//~ {5, 0, 2, 23}, //6720
		//~ // {5, 0, 0, 41}, //1116 endcap a
		//~ // {5, 0, 1, 31}, //1121
		//~ // {5, 0, 2, 23}, //7318
		//~ /*{0, 0, 1, 21},
		//~ {0, 1, 1, 21},
		//~ {1, 0, 1, 31},
		//~ {1, 1, 1, 31},
		//~ {2, 0, 1, 41},
		//~ {2, 1, 1, 41},
		//~ {3, 0, 1, 51},
		//~ {3, 1, 1, 51},*/
	//~ };
	//~ std::vector<int> gColours = {kBlue+3, kCyan+2, kCyan-6, kOrange+1, kOrange+10, kRed+3};
	//~ // std::vector<int> gColours = {kRed, kGreen, kBlue, kBlack, kMagenta, kGray+2};
	//~ std::vector<int> gMarkers = {20,21,22,23,33,29};
	//~ // std::vector<std::string> legEntry = {"Barrel 3 #eta_{index}= 1 #Phi_{index}= 20", "Barrel 3 #eta_{index}= -6 #Phi_{index}= 21", "Barrel 5 #eta_{index}= 1 #Phi_{index}= 37", "Barrel 5 #eta_{index}= -5 #Phi_{index}= 33", "Barrel 6 #eta_{index}= 1 #Phi_{index}= 26", "Barrel 6 #eta_{index}= -6 #Phi_{index}= 24"};
	//~ std::vector<std::string> legEntry = {"Endcap C6 #eta_{index}= 0 #Phi_{index}= 41", "Endcap C6 #eta_{index}= 1 #Phi_{index}= 31", "Endcap C6 #eta_{index}= 2 #Phi_{index}= 23"};
	//~ // std::vector<std::string> legEntry = {"Endcap A6 #eta_{index}= 0 #Phi_{index}= 41", "Endcap A6 #eta_{index}= 1 #Phi_{index}= 31", "Endcap A6 #eta_{index}= 2 #Phi_{index}= 23"};
	//~ int i = 0, j = 0; //i to loop over scan points, j to loop over modules
	//~ TGraphErrors* gEff = nullptr;
	//~ TLegend leg(0.5, 0.2, 0.85, 0.5);
	//~ // leg.SetNColumns(2);
	//~ std::vector<float> current;
	//~ for (const Module& module : modules) {
		//~ std::cout << "Inside loop over modules: layer " << module.layer << " side " << module.side << " eta " << module.eta << " phi " << module.phi << std::endl;
		//~ current = getModuleCurrent(367134, false, false, module.layer, module.eta, module.phi);
		//~ gEff = new TGraphErrors(current.size());
		//~ gEff->SetName(TString::Format("graph_%d", j).Data());
		//~ std::cout << "Creating gEff graph " << &gEff << " name " << gEff->GetName() << std::endl;
		//~ std::vector<float> moduleEff;
		//~ gEff->SetMarkerSize(0.8);
		//~ for (auto sp: scanPoints){
			//~ std::cout << "Inside loop over scan points, sp: " << sp << " i " << i << " module.layer: " << module.layer << std::endl;
			//~ int SP = std::stoi(sp);
			//~ // if (j >=3 && SP > 150) break;//barrel
			//~ // if (j == 2 && SP > 200) break;//barrel
			//~ if (SP > 200) break;
			//~ moduleEff = getModuleEff(367134, false, false, sp, module.layer, module.side, module.eta, module.phi);
			//~ gEff->SetPoint(i, SP-current.at(i)*r*1e-6, moduleEff.at(0));
			//~ gEff->SetPointError(i, 0, moduleEff.at(1));
			//~ ++i;
		//~ }
		//~ i = 0;
		//~ std::cout << "Marker attributes: colour " <<  gColours.at(j) << " type " << gMarkers.at(j) << std::endl;
		//~ gEff->SetMarkerColor(gColours.at(j));
		//~ gEff->SetMarkerStyle(gMarkers.at(j));
		//~ gEff->SetLineColor(gColours.at(j));
		//~ std::cout << "Graph before drawing on canvas " << &gEff << std::endl;
		//~ if (j == 0) {
			//~ gEff->GetXaxis()->SetRangeUser(0, 300);
			//~ gEff->GetYaxis()->SetRangeUser(0, 1.2);
			//~ gEff->SetTitle("");
			//~ gEff->GetXaxis()->SetTitle("HV [V]");
			//~ gEff->GetYaxis()->SetTitle("Hit Efficiency");
			//~ gEff->Draw("APEL");
			//~ leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		//~ }
		//~ else  {	
			//~ gEff->Draw("PEL same");
			//~ leg.AddEntry(gEff->GetName(), legEntry.at(j).data(), "PEL");
		//~ }
		//~ ++j;
		//~ std::cout << "End of loop over modules, incremented j to " << j << std::endl;
		//~ std::cout << "******************************************************************" << std::endl;
	//~ }
	//~ gPad->SetTickx();
	//~ gPad->SetTicky();
	//~ gPad->SetGrid();
	//~ leg.SetBorderSize(0);
  //~ leg.SetFillStyle(0);
	//~ leg.Draw();
	//~ c.SaveAs("moduleEfficiency.pdf");
	//~ // std::cout << "******************************************************************" << std::endl;
	//~ // Plot excluding modules with crystal orientation <100>
	//~ std::pair<int, int> mod1(-6, 14);
	//~ std::pair<int, int> mod2(-6, 21);
	//~ std::vector<std::pair<int, int>> exclModules = {
		//~ {-6, 14}, {-6, 21}, {-5, 14}, {-5, 21}, {-4, 20}, {-3, 20}, {-3, 21}, {-2, 20}, {-1, 20}, {1, 20}, {2, 22}, {3, 22}, {4, 5}, {4, 20}, {6, 31}
		//~ };
	//~ //exclModules.push_back(mod1);
	//~ // exclModules.push_back(mod2);
	//~ scanPoints = getScanPoints(367170);
	//~ getEffPlotsExclMod(367170, true, false, 2, "", false);
	//~ getEffPlots(367170, true, false, 2, "");
	//~ 
	//~ 
	/*********************************************************************/
	//~ std::vector<TProfile*> profs = getProfileExclMod(getEffBarrel(367170, scanPoints.at(0), 2, ""), 367170, true, exclModules);
	
	
	/*
	//~ std::vector<float> noise = getModuleNoise(363664, "100", 0, 0, 1, 21);
	//~ std::cout << "Noise in module with eta 1 and phi 21 = " << noise.at(0) << " error = " << noise.at(1) << std::endl;
	i = 0; 
	j = 0;
	TGraphErrors* gNoise = nullptr;
	leg.Clear();
	leg.SetHeader("Side 0   Side 1   Module");
	leg.SetY1(0.4);
	leg.SetY2(0.9);
	//~ leg.SetX1(0.45);
	//~ leg.SetX2(0.85);
	c.Clear();
	//~ TLegend leg(0.4, 0.2, 0.8, 0.5, "Side 0   Side 1   Module");
	leg.SetNColumns(2);
	for (const Module& module : modules) {
		std::cout << "Inside loop over modules: layer " << module.layer << " side " << module.side << " eta " << module.eta << " phi " << module.phi << std::endl;
		if (module.phi == 21) gNoise = new TGraphErrors(scanPoints.size());
		else gNoise = new TGraphErrors(scanPoints.size()-3);
		gNoise->SetName(TString::Format("noisegraph_%d", j).Data());
		std::cout << "Creating gNoise graph " << &gNoise << "name " << gNoise->GetName() << std::endl;
		std::vector<float> moduleNoise;
		gNoise->SetMarkerSize(0.8);
		for (auto sp: scanPoints){
			int SP = std::stoi(sp);
			//~ if (module.phi != 21 && SP > 200) {break;}
			moduleNoise = getModuleNoise(367134, sp, module.layer, module.side, module.eta, module.phi);
			gNoise->SetPoint(i, SP, moduleNoise.at(0)*10e-5);
			gNoise->SetPointError(i, 0, moduleNoise.at(1)*10e-5);
			++i;
		}
		i = 0;
		//~ std::cout << "Marker attributes: colour " <<  gColours.at(j) << " type " << gMarkers.at(j) << std::endl;
		gNoise->SetMarkerColor(gColours.at(j));
		gNoise->SetMarkerStyle(gMarkers.at(j));
		gNoise->SetLineColor(gColours.at(j));
		//~ std::cout << "Graph before drawing on canvas " << &gNoise << std::endl;
		if (j == 0) {
			gNoise->GetXaxis()->SetRangeUser(0, 300);
			gNoise->GetYaxis()->SetRangeUser(0, 0.1);
			gNoise->SetTitle("");
			gNoise->GetXaxis()->SetTitle("HV [V]");
			gNoise->GetYaxis()->SetTitle("Noise Occupancy");
			gNoise->Draw("APEL");
			//~ leg.AddEntry(gNoise->GetName(), legEntry.at(j).data(), "PEL");
		}
		else  {	
			gNoise->Draw("PEL same");
			//~ leg.AddEntry(gNoise->GetName(), legEntry.at(j).data(), "PEL");
		}
		++j;
		std::cout << "End of loop over modules, incremented j to " << j << std::endl;
		std::cout << "******************************************************************" << std::endl;
	}
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	leg.SetBorderSize(0);
  leg.SetFillStyle(0);
	leg.Draw();
	c.SaveAs("moduleNO.pdf");
	std::cout << "******************************************************************" << std::endl;
	*/
	/*************************************/
	//plots for HEP Xmas meeting + PUBLIC PLOT	
	/*
	TProfile* p_361689 = nullptr;
	TProfile* p_348251 = nullptr;
	std::vector<TProfile*> profs;
	std::vector<string> sp_361689 = getScanPoints(361689);
	std::vector<string> sp_348251 = getScanPoints(348251);
	
	std::vector<double> i_348251 = {292.091, 402.465, 403.123, 511.297, 551.245, 559.806, 568.563, 574.891};//uA
	std::vector<double> voltShift_348251;
	double r = 11.2e3;
	for (auto i : i_348251){
		voltShift_348251.push_back(r*i*1e-6);
		std::cout << "voltShift in run 348251 for i = " << i << " is " << r*i*1e-6 << std::endl;
	}
	
	std::vector<double> i_361689 = {679.017, 832.457, 930.392, 964.784, 983.084, 1002.51, 1015.86, 1028.02, 1035.45, 1065.06};//uA
	std::vector<double> voltShift_361689;
	
	for (auto i : i_361689){
		voltShift_361689.push_back(r*i*1e-6);
		std::cout << "voltShift in run 361689 for i = " << i << " is " << r*i*1e-6 << std::endl;
	}
	
	float value = 0;
	float error1 = 0, error2 = 0, error = 0;
	int eta = 1;
	int counter = 0;
	
	TGraphErrors* g_361689 = new TGraphErrors(sp_361689.size());
	TGraphErrors* g_348251 = new TGraphErrors(sp_348251.size());
	
	for(string sp : sp_361689){
		int SP = stoi(sp);
		profs = getProfileX(getEffBarrel(361689, sp, 2, ""), 361689, true, false);
		p_361689 = profs.at(0);
		value = p_361689->GetBinContent(p_361689->FindBin(eta));
		value += p_361689->GetBinContent(p_361689->FindBin(-1*eta));
		value = value/2;
		error1 = p_361689->GetBinError(p_361689->FindBin(eta));
		error2 = p_361689->GetBinError(p_361689->FindBin(-1*eta));
		error = TMath::Sqrt(error1*error1 + error2*error2);
		g_361689->SetPoint(counter, SP-voltShift_361689.at(counter), value);
		g_361689->SetPointError(counter, 0, error);
		++counter;
	}
	counter = 0;
	for(string sp : sp_348251){
		int SP = stoi(sp);
		profs = getProfileX(getEffBarrel(348251, sp, 2, "b3d9"), 348251, true, false);
		p_348251 = profs.at(0);
		value = p_348251->GetBinContent(p_348251->FindBin(eta));
		value += p_348251->GetBinContent(p_348251->FindBin(-1*eta));
		value = value/2;
		error1 = p_348251->GetBinError(p_348251->FindBin(eta));
		error2 = p_348251->GetBinError(p_348251->FindBin(-1*eta));
		error = TMath::Sqrt(error1*error1 + error2*error2);
		g_348251->SetPoint(counter, SP-voltShift_348251.at(counter), value);
		g_348251->SetPointError(counter, 0, error);
		++counter;
	}
	
	
	TCanvas ccomp;
	ccomp.SetMargin(0.08, 0.02, 0.1, 0.03);
	g_361689->GetYaxis()->SetRangeUser(0, 1.2);
	g_361689->GetYaxis()->SetTitle("Hit Efficiency");
	g_361689->GetYaxis()->SetTitleSize(0.04);
	g_361689->GetYaxis()->SetTitleOffset(0.9);
	g_361689->GetXaxis()->SetTitle("HV [V]");
	g_361689->GetXaxis()->SetTitleSize(0.04);
	g_361689->GetXaxis()->SetLabelSize(0.038);
	g_361689->GetYaxis()->SetLabelSize(0.038);
	g_361689->SetTitle("");
	g_361689->SetMarkerStyle(20);
	g_361689->SetMarkerColor(kSpring+5);
	g_361689->SetLineColor(kBlack);
	g_361689->Draw("APEL");
	g_348251->SetMarkerStyle(21);
	g_348251->SetMarkerColor(kAzure+5);
	g_348251->SetLineColor(kBlack);
	g_348251->Draw("PEL same");
	TLegend lcomp(0.4, 0.3, 0.7, 0.5);
	gPad->SetTickx();
	gPad->SetTicky();
	gPad->SetGrid();
	lcomp.SetBorderSize(0);
	lcomp.SetFillStyle(0);
	lcomp.SetTextSize(0.045);
	lcomp.AddEntry(g_361689, "2018-09-23, Barrel 3, |#eta_{index}| = 1", "PEL");
	lcomp.AddEntry(g_348251, "2018-04-18, Barrel 3, |#eta_{index}| = 1", "PEL");
	lcomp.Draw();
	float x=0.15, y=0.87, labelOffset=0.12;
	TLatex txt;
	txt.SetTextFont(72);
	txt.SetTextSize(0.05);
	txt.DrawLatexNDC(x,y,"ATLAS");
	txt.SetTextFont(42);
	txt.DrawLatexNDC(x+labelOffset,y,"SCT Preliminary");
	ccomp.SaveAs("HitEfficiency.pdf");
	ccomp.SaveAs("HitEfficiency.png");
	*/
	/************************************/
	//~ getEffPlotsL96(348251, 2, false);
	//~ getNoisePlots(361689, true, false, 2);
	//~ getNoisePlots(361689, true, false, 2);
	//~ getNoisePlotsL96(361689, true, 2, false);
/***************************************/
	//~ effVsHV();
	//~ TProfile2D* p2D = getEffBarrel(361689, "060", 2, "");
	//~ TProfile2D* p2D = getNoiseBarrel(361689, "040", 2);
	
	//~ TProfile *pwith = getProfileXL96(p2D, 361689, true);
	//~ pwith->GetYaxis()->SetRangeUser(0, 120);
	//~ TProfile *pwithout = getProfileXL96(p2D, 361689, false);
	//~ pwithout->GetYaxis()->SetRangeUser(0, 120);
	//~ std::vector<TProfile*> vec = getProfileX(p2D, 331689, true, false);
	//~ TProfile* pwhole = vec.at(0);
	//~ TCanvas cnv;
	//~ cnv.Divide(3,1);
	//~ cnv.cd(1);
	//~ pwith->Draw();
	//~ cnv.cd(2);
	//~ pwithout->Draw();
	//~ cnv.cd(3);
	//~ pwhole->Draw();
	//~ cnv.SaveAs("compareL96_40.pdf");
	//~ getEffPlotsL96(361689, 2, true);
	//~ getEffPlotsL96(361689, 2, false);
	//~ bool isBarrel = false;
	//~ bool isEA = false;
	//~ int side = 0;
	//~ int run = 360414;
	//~ getEffPlots(361689, false, true, 2, "");
	//~ getEffPlots(348251, true, false, 2, "b6d2");
	//~ getNoisePlots(361689, false, false, 2);
	
	//~ getEffPlots(run, isBarrel, isEA, side);
	//~ std::vector<string> scanPoints = getScanPoints(run);
	//~ TProfile2D* p2D = nullptr;
	//~ for (auto point : scanPoints){
		//~ if (isBarrel) p2D = getEffBarrel(run, point, 2);
		//~ else p2D = getEffEndcap(run, point, 2, isEA);
		//~ TProfile* p = getProfileX(p2D , run, isBarrel, isEA);
		//~ TCanvas c("c","cnv",0,0,600,600);
		//~ c.Divide(1,2);
		//~ c.cd(1);
		//~ p2D->SetTitle("Hit efficiency");
		//~ p2D->Draw("colz");
		//~ c.cd(2);
		//~ p->Draw();
		//~ string filename = point.data()+std::string("_EA.pdf");
		//~ c.SaveAs(filename.data());
	//~ }
	//~ std::vector<TProfile*> profsEA;
	//~ std::vector<TProfile*> profsEC;
	//~ std::vector<TProfile*> profsB;
	//~ TProfile* p = nullptr;
	//~ TCanvas cnvB;
	//~ TLegend leg(0.15, 0.15, 0.3, 0.3);
	//~ leg.SetBorderSize(0);
	//~ leg.SetTextSize(0.04);
	//~ int i = 0, j = 0;
	//~ 
	//~ if (isBarrel){
		//~ string title, title_prefix;
		//~ if (run == 361689) title_prefix = "Run 361689, Layer 3";
		//~ else if (run == 360414) title_prefix = "Run 360414, Layer 6";
		//~ for (auto point : scanPoints) {
			//~ p = getProfileX(getEffBarrel(run, point, side), run, isBarrel, isEA);
			//~ profsB.push_back(p);
			//~ title = title_prefix.data() + std::string(", HV = ") + std::to_string(std::stoi(point)) + std::string(" V");
			//~ profsB.at(j)->SetTitle(title.data());
			//~ profsB.at(j)->GetXaxis()->SetTitle("#eta-index");
			//~ profsB.at(j)->GetYaxis()->SetTitle("Efficiency");
			//~ profsB.at(j)->GetYaxis()->SetTitleOffset(1.2);
			//~ profsB.at(j)->SetLineColor(kBlack);
			//~ profsB.at(j)->SetMarkerColor(kBlack);
			//~ profsB.at(j)->SetLineWidth(2);
			//~ profsB.at(j)->Draw("pe");
			//~ profsB.at(j)->GetYaxis()->SetRangeUser(0, 1.1);
			//~ string filename = point.data()+std::string("_B.pdf");
			//~ cnvB.SaveAs(filename.data());
			//~ ++j;
		//~ }
	//~ }
	//~ if (isEA == false) return 0;
	//~ TCanvas cnv;
	//~ if (isBarrel == false){
		//~ for (auto point : scanPoints){
			//~ p = getProfileX(getEffEndcap(run, point, side, isEA), run, isBarrel, isEA);
			//~ profsEA.push_back(p);
			//~ isEA = false;
			//~ p = getProfileX(getEffEndcap(run, point, side, isEA), run, isBarrel, isEA);
			//~ profsEC.push_back(p);
			//~ string title_prefix;
			//~ if (run == 361689) title_prefix = "Run 361689, Disk 6, HV = ";
			//~ else if (run == 360414) title_prefix = "Run 360414, Disk 2, HV = ";
			//~ string title = title_prefix.data() + std::to_string(std::stoi(point)) + std::string(" V");
			//~ profsEA.at(i)->SetTitle(title.data());
			//~ profsEA.at(i)->GetXaxis()->SetTitle("#eta-index");
			//~ profsEA.at(i)->GetYaxis()->SetTitle("Efficiency");
			//~ profsEA.at(i)->GetYaxis()->SetTitleOffset(1.2);
			//~ profsEA.at(i)->SetLineColor(kSpring+9);
			//~ profsEA.at(i)->SetMarkerColor(kSpring+9);
			//~ profsEA.at(i)->SetLineWidth(2);
			//~ profsEA.at(i)->Draw("pe");
			//~ profsEA.at(i)->GetYaxis()->SetRangeUser(0, 1.1);
			//~ profsEA.at(i)->SetName("EA");
			//~ leg.AddEntry("EA", "Endcap A", "l");
			//~ profsEC.at(i)->SetLineColor(kAzure+7);
			//~ profsEC.at(i)->SetMarkerColor(kAzure+7);
			//~ profsEC.at(i)->SetLineWidth(2);
			//~ profsEC.at(i)->Draw("pe same");
			//~ profsEC.at(i)->SetName("EC");
			//~ leg.AddEntry("EC", "Endcap C", "l");
			//~ leg.Draw();
			//~ string filename = point.data()+std::string("_EAC.pdf");
			//~ cnv.SaveAs(filename.data());
			//~ ++i;
			//~ leg.Clear();
			//~ isEA = true;
		//~ }
	//~ }
	
	/*********************************************************/
	//~ int RunNo = 361689;
	//~ std::vector<std::string> SP = getScanPoints(RunNo);
	//~ TFile *tmpf = nullptr;
	//~ for (auto sp : SP){
		//~ TProfile2D* prf = getEffBarrel(361689, sp, 2, "");
		//~ TCanvas c1;
		//~ prf->Draw("colz");
		//~ c1.SaveAs("test2d.pdf");
		//~ getProfileXL96(getEffBarrel(361689, sp, 2, ""), 361689, true);
		//~ TProfile* pr1d = getProfileX(getEffBarrel(361689, sp, 2, ""), 361689, true, false);
		//~ c1.Clear();
		//~ pr1d->Draw();
		//~ c1.SaveAs("test1d_wholerange.pdf");
		//~ tmpf = OpenFileExpress(RunNo, sp);
		//~ std::cout << tmpf << std::endl;
	//~ }
	//~ std::cout << "profsEA size = " << profsEA.size() << std::endl;
	//~ std::cout << "profsEC size = " << profsEC.size() << std::endl;
	//~ std::vector<string> scanPoints = getScanPoints(361689);
	//~ std::vector<int> etaIndices = getAbsEtaIndices(true);
	//~ for (auto sp : scanPoints) {
		//~ std::cout << "Scan Point = " << sp << std::endl;
		//~ TFile* f = OpenFile(361689, sp);
		//~ string histPath = "";
		//~ TProfile2D* prof = getEffBarrel(361689, sp, 2);
		//~ TProfile* p = getProfileX(prof);
		//~ TCanvas c;
		//~ prof->Draw("colz");
		//~ p->Draw();
		//~ string cnvName = sp.data()+std::string(".pdf");
		//~ c.SaveAs(cnvName.data());
	//~ }
	//~ std::vector<TH1D> hists;
	//~ for (auto eta : etaIndices) {
		//~ TH1D hist("hist", "",26,-5,255);
		//~ for (auto sp : scanPoints){
			//~ TProfile* p = getProfileX(getEffBarrel(361689, sp, 2));
			//~ float value = p->GetBinContent(p->FindBin(eta));
			//~ value += p->GetBinContent(p->FindBin(-1*eta));
			//~ value = value/2;
			//~ std::cout << "Value in this scanPoint =" << value << " for eta = " << eta << std::endl;
			//~ int sPt = std::stoi(sp);
			//~ std::cout << "Scan point as integer = " << sPt << std::endl;
			//~ hist.SetBinContent(hist.FindBin(std::stoi(sp)), value);
			//~ hist.SetBinError(hist.FindBin(std::stoi(sp)), 0);
		//~ }
		//~ hists.push_back(hist);
	//~ }
	//~ makeHist(hists);
	//~ TCanvas c1;
	//~ hists.at(0).Draw("p");
	//~ hists.at(1).Draw("p same");
	//~ hists.at(2).Draw("p same");
	//~ hists.at(3).Draw("p same");
	//~ c1.SaveAs("test.pdf");
	//~ TFile* file1 = OpenFile(360414, "040");
	//~ TFile* file2 = OpenFile(348251, "HV0040", "B3D9");
	//~ setConfiguration(361689, true, true);
	return 0;
}
