/***********************************************************************/
/** g++ -std=c++14 -o analysis mergeHIST.cxx `root-config --cflags --glibs` **/
/***********************************************************************/

#include "/eos/user/h/hiito/myinclude.h"
#include "/eos/user/h/hiito/mycolor.h"
int main()
{
  std::string RunNumber = "486640";
  static const unsigned int nHV = 9; int mHV = 9;
  TString inHV[nHV]   = {  "200", "175", "150", "125", "100", "075", "060", "040", "020", "010"};
  int startLB[nHV]    = {    707,   712,   718,   722,   727,   732,   738,   743,   748,   752};
  int endLB[nHV]      = {    709,   715,   720,   725,   730,   736,   740,   745,   750,   754};
  int m_skip = 0;
  int skipnumber[m_skip]={ };
  
  int tru_LB=0;
  TString TS01;
  for(int ii_HV = 0; ii_HV < mHV; ii_HV++){
    TString outfileName = "run"+RunNumber+"_b3_"+inHV[ii_HV]+".root";
    
    TFile *fNew = new TFile(outfileName, "RECREATE");
    fNew->cd();
    TString dirName = "run_"+RunNumber;
    TDirectory *dir = fNew->mkdir(dirName);
    TDirectory *dir_1 = dir->mkdir("SCT");
    
    TDirectory *dir_2 = dir_1->mkdir("SCTB");
    TDirectory *dir_3 = dir_2->mkdir("eff");
    
    TDirectory *dir_2_1 = dir_1->mkdir("SCTEA");
    TDirectory *dir_3_1 = dir_2_1->mkdir("eff");
    
    TDirectory *dir_2_2 = dir_1->mkdir("SCTEC");
    TDirectory *dir_3_2 = dir_2_2->mkdir("eff");
    
    TString files[endLB[ii_HV] - startLB[ii_HV] + 1];
    int flag=0;
    for(int ii_LB = 0; ii_LB < endLB[ii_HV] - startLB[ii_HV] + 1; ii_LB++){
      flag = 0;
      for(int ii_skip = 0; ii_skip < m_skip; ii_skip++){
	if(startLB[ii_HV] + ii_LB == skipnumber[ii_skip]){
	  flag=1;
	  break;
	}
      }
      std::cout<<flag<<"    "<<startLB[ii_HV] + ii_LB<<std::endl;
      if(flag==1) continue;
      
      tru_LB = startLB[ii_HV] + ii_LB;  std::ostringstream oss01;  oss01<<tru_LB;
      TS01=oss01.str();
      
      // files[ii_LB] = "/eos/home-h/hiito/SCT/HVScan/data18_13TeV.00456316.express_express.merge.HIST.f1042/OUT.HIST.lb3"+TS01+".root";
      // files[ii_LB] = "/eos/atlas/atlascerngroupdisk/det-sct/run3analysis/HV_scan/data23_13p6TeV.00456316.physics_Main.recon.HIST.x731/OUT.HIST.lb0"+TS01+".root";
      files[ii_LB] = "../OUT.HIST.lb0"+TS01+".root";
    }
    // std::cout<<files.size()<<std::endl;  std::cout<<"L47"<<std::endl;

    std::string pathName = "run_"+RunNumber+"/SCT/SCTB/eff";
    std::vector<std::pair<std::string, std::string>> profs = {
      {pathName, "eff_3_0"},
      {pathName, "eff_3_0_bcid"},
      {pathName, "eff_3_1"},
      {pathName, "eff_3_1_bcid"},
    };
    
        
    std::string path = ".";
    TProfile2D* prof = nullptr;
    int countinput = 0;
    
    for (int j=0; j<profs.size(); ++j){
      
      std::string currentProf = profs.at(j).first + std::string("/") + profs.at(j).second;
      std::cout << "currentProf name = " << currentProf.data() << std::endl;
      for (int i=0; i<endLB[ii_HV] - startLB[ii_HV] + 1; ++i){
	flag = 0;
	for(int ii_skip = 0; ii_skip < m_skip; ii_skip++){
	  if(startLB[ii_HV] + i == skipnumber[ii_skip]){
	    flag=1;
	    break;
	  }
	}
	if(flag==1) continue;
	TFile *f = TFile::Open(files[i]);
	
	if(i==0){
	  prof = dynamic_cast<TProfile2D*>(f->Get(currentProf.data())->Clone("eff_clone"));
	  prof->SetDirectory(0);
	  std::cout << "Prof = " << prof << std::endl;
	}
	else {
	  TProfile2D *prof1 = nullptr;
	  prof1 = dynamic_cast<TProfile2D*>(f->Get(currentProf.data()));
	  std::cout << "Prof = " << prof << std::endl;
	  std::cout << "Prof1 = " << prof1 << std::endl;
	  prof->Add(prof1);
	}
	f->Close();
	countinput++;
      }
      fNew->cd();
      
      if (profs.at(j).first.find("SCTB") != std::string::npos)
	dir_3->cd();
      else if (profs.at(j).first.find("SCTEA") != std::string::npos)
	dir_3_1->cd();
      else if (profs.at(j).first.find("SCTEC") != std::string::npos)
	dir_3_2->cd();
      
      prof->Write(profs.at(j).second.data());
      
    }
    fNew->Close();
  }
  
  return 0;
}  
