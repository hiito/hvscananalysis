/************************************************************************************************************/
/** g++ -std=c++14 -o analysis apps/main.cxx src/Config.cxx src/Plotter.cxx src/Module.cxx src/HVscanFunctions.cxx `root-config --cflags --glibs` **/
/************************************************************************************************************/

#include <iostream>

#include "include/Module.h"
#include "include/Config.h"
#include "include/Plotter.h"
#include "include/HVscanFunctions.h"

#include "TProfile2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TMath.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TLatex.h"

using namespace std;


int main()
{
	cout << "in main function" << endl;
	
	Config configuration(427514, true, false, 2, "b0d2", 1, true, false, true);
	
	//~ Config configuration(348251, true, false, 2, "b3d9", 0, true, false, false);
	//configuration(run, isBarrel, isEA, side, BD, stream, etaCurves, phiCurves, BCID)
	// Config configuration(361689, true, false, 2, "", 0, true, false, true);
	//~ configuration.PrintConfiguration();
	
	//~ std::vector<string> sp = getScanPoints(configuration);
	//~ TFile *f = OpenFile(configuration, "060");
	//~ TProfile2D* p2D = getEffProf2D(configuration, "060");
	
	//~ std::vector<TProfile*> ps = getProfileX(configuration, p2D);
	//~ TCanvas c;
	//~ ps.at(0)->Draw();
	//~ c.SaveAs("plots/prof.pdf");
	
	//~ getEffPlots(configuration);
	
	std::vector<Module> modB3_CO100;
	
	if (configuration.GetIsBarrel()){
		modB3_CO100 = {//modules in barrel3 with <100> orientation that need to be excluded
			{3, 1, -6, 14},
			{3, 1, -6, 21},
			{3, 1, -5, 14},
			{3, 1, -5, 21},
			{3, 1, -4, 20},
			{3, 1, -3, 20},
			{3, 1, -3, 21},
			{3, 1, -2, 20},
			{3, 1, -1, 20},
			{3, 1, 1, 20},
			{3, 1, 2, 22},
			{3, 1, 3, 22},
			{3, 1, 4, 5},
			{3, 1, 4, 20},
			{3, 1, 6, 31}
		}; 
	}
	else {
		modB3_CO100 = {};
	}
	
	std::cout << "Vector with ignored modules size = " << modB3_CO100.size() << std::endl;
	//~ std::vector<TProfile*> profs = getProfileXExclMod(configuration, getEffProf2D(configuration, "060"), exclModules);
	getEffPlotsExclMod(configuration, modB3_CO100);
	
	
	std::cout << "###################################################################" << std::endl;
	std::cout << "###################################################################" << std::endl;
	std::cout << "###################################################################" << std::endl;
	
	// efficiency rainbow plot with excluded modules with <100> crystal orientation
	
	// TCanvas cProfiles;
	// gStyle->SetOptStat(0);
	// cProfiles.SetMargin(0.08, 0.02, 0.1, 0.06);
	// gPad->SetTickx();
	// gPad->SetTicky();
	
	// TLegend profLeg(0.6, 0.68, 0.95, 0.92);
	// profLeg.SetBorderSize(0);
	// profLeg.SetFillStyle(0);
	// profLeg.SetNColumns(2);
	// profLeg.SetTextSize(0.04);
	
	// //~ std::vector<int> profColour = {kOrange, kRed, kPink, /*kMagenta,*/ kViolet, /*kBlue,*/ kAzure, /*kCyan,*/ kTeal, /*kSpring*/};
	// std::vector<int> profColour = {kOrange-3, kSpring+9, kTeal+3, kAzure+1, kBlue+3, kPink+8};
	// std::vector<int> profMarker = {20, 21, 22, 23, 29, 33, 34, 43, 47, 41};
	// std::vector<string> profLegend = {"HV = 40V", "HV = 60V", "HV = 80V", "HV = 100V", "HV = 120V", "HV = 140V", "HV = 160V", "HV = 180V", "HV = 200V", "HV = 250V"};
	
	
	// std::vector<int> etaIndices = {-6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6};
	// double val = 0.4;
	// double err = 0.05;
	
	// int i_eta = 0; //to choose the prof for right eta
	// int i_sp = 0; //to choose the right scan point
	
	// //~ std::string sp = "040";
	// TProfile* p = nullptr;
	
	// std::string profName = "";
	
	// std::vector<std::string> v_scanPoints = {"040","060","080","100","120","140"};
	
	// //~ for (auto sp : configuration.GetScanPoints()){
	// for (auto sp : v_scanPoints){
	// 	TH1D *h = new TH1D("h", "; #eta_{index}; Hit efficiency", 13, -6.5, 6.5); 
	// 	std::vector<TProfile*> profs = getProfileXExclMod(configuration, getEffProf2D(configuration, sp), modB3_CO100);
		
	// 	for (auto eta : etaIndices){
			
	// 		p = profs.at(i_eta);
	// 		val = p->GetBinContent(p->FindBin(eta));
	// 		err = p->GetBinError(p->FindBin(eta));
	// 		h->SetBinContent(h->FindBin(eta), val);
	// 		h->SetBinError(h->FindBin(eta), err);
	// 		++i_eta;
			
	// 	}
	// 	h->SetLineColor(profColour.at(i_sp));
	// 	h->SetMarkerColor(profColour.at(i_sp));
	// 	h->SetMarkerStyle(profMarker.at(i_sp));
		
	// 	h->GetYaxis()->SetRangeUser(0, 1.5);
	// 	h->GetXaxis()->SetTitle("#eta_{index}");
	// 	h->GetYaxis()->SetTitle("Hit Efficiency");
	// 	h->GetXaxis()->SetTitleOffset(1.1);
	// 	h->GetXaxis()->SetTitleSize(0.04);
	// 	h->GetXaxis()->SetLabelSize(0.04);
	// 	h->GetYaxis()->SetLabelSize(0.04);
	// 	h->GetYaxis()->SetTitleSize(0.04);
	// 	h->GetYaxis()->SetTitleOffset(0.9);
	// 	if (i_sp == 0) {
	// 		h->Draw("pe");
	// 		profName = std::string("prof")+std::to_string(i_sp);
	// 		h->SetName(profName.data());
	// 		profLeg.AddEntry(h->GetName(), profLegend.at(i_sp).data(), "PEL");
	// 	}
	// 	else {
	// 		h->Draw("pe same");
	// 		profName = std::string("prof")+std::to_string(i_sp);
	// 		h->SetName(profName.data());
	// 		profLeg.AddEntry(h->GetName(), profLegend.at(i_sp).data(), "PEL");
	// 	}
	// 	i_eta = 0;
	// 	++i_sp;
	// 	//~ h->Clear();
	// }
	// profLeg.Draw();
	// float x=0.15, y=0.85, labelOffset=0.12;
	// TLatex txt;
	// txt.SetTextFont(72);
	// txt.SetTextSize(0.05);
	// txt.DrawLatexNDC(x,y,"ATLAS");
	// txt.SetTextFont(42);
	// txt.DrawLatexNDC(x+labelOffset,y,"SCT Internal");
	// cProfiles.SaveAs("Barrel3Profiles.pdf");
	
	return 0;
}
